package com.soldig.kawanmart.service

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.app.PendingIntent
import com.soldig.kawanmart.R
import com.soldig.kawanmart.presentation.HomeActivity

class FCMService : FirebaseMessagingService() {
    private var numMessages = 0

    @SuppressLint("UnspecifiedImmutableFlag")
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        println("""
            Diterima data seperti ini
            title : ${p0.notification?.title}
            body:   ${p0.notification?.body}
            ================ Data ================
        """.trimIndent())
        p0.data.forEach { data ->
            println("Key : ${data.key} dan Value adalah ${data.value}")
        }
        val intent = Intent(applicationContext, HomeActivity::class.java)
//you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//Add Any key-value to pass extras to intent
        //Add Any key-value to pass extras to intent
        intent.putExtra("pushnotification", "yes")
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(p0.notification?.title ?: "Notification Title Error")
                .setContentText(p0.notification?.body ?: "Oops, error on body notification")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.logo))
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.logo)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channel = NotificationChannel(
                getString(R.string.notification_channel_id),
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            channel.description = CHANNEL_DESC
            channel.setShowBadge(true)
            channel.canShowBadge()
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)
            notificationManager?.createNotificationChannel(channel)

        }
        notificationManager?.notify(0, notificationBuilder.build())
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

    }

    companion object {
        private const val CHANNEL_NAME = "com.ahaapp.ahaandroid.util.FCM"
        private const val CHANNEL_DESC = "Firebase Cloud Messaging"
    }

}