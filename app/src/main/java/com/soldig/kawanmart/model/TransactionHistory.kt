package com.soldig.kawanmart.model

class TransactionHistory(
    val id: String,
    val orderNumber: String,
    val orderDate:  String,
    val totalItem : Int,
    val totalPrice: Double,
    val status : TransactionStatus
)