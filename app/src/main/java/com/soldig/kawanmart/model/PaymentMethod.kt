package com.soldig.kawanmart.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PaymentMethod(
    val id: String,
    val name: String,
    val categoryId: String
) : Parcelable