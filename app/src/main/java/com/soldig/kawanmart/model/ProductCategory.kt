package com.soldig.kawanmart.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductCategory(
    val id: String,
    val name: String
) : Parcelable