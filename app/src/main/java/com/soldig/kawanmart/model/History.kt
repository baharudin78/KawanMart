package com.soldig.kawanmart.model

data class History(
    val id: String,
    val date: String,
    val total_item: String,
    val total_price: String
)