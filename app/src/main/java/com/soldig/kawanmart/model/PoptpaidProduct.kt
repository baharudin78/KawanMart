package com.soldig.kawanmart.model


enum class PostPaidStatus{
    Success,
    Pending,
    Fail
}


data class PostPaidProductResult(
    val refId: String,
    val postPaidStatus: PostPaidStatus,
    val totalPrice: Double = 0.0,
    val adminFee: Double,
    val warungFee: Double,
    val subTotal: Double,
    val message: String,
)
