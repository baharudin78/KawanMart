package com.soldig.kawanmart.model

import com.soldig.kawanmart.presentation.features.home.HomeViewState


data class Order(
    val product: HomeViewState.ProductSection.ProductListItem.POSProductListItem,
    var quantity: Int,
     //Price when the order is being ordered ( cause the product price can be change any time )
    var currentTotalPrice: Double
)

