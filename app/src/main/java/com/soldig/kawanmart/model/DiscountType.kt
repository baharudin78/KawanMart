package com.soldig.kawanmart.model

enum class DiscountType {
    Percentage,
    Nominal,
    Nothing
}