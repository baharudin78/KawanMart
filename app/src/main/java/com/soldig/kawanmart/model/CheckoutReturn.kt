package com.soldig.kawanmart.model

data class CheckoutReturn(
    val transactionResult: TransactionDetail,
    val isCheckoutDoneInServer : Boolean
)