package com.soldig.kawanmart.model

import com.soldig.kawanmart.model.Cart
import com.soldig.kawanmart.model.PaymentMethod

data class Transaction(
    val paymentMethod: PaymentMethod,
    val cart: Cart,
    val paidMoney: Double,
    val change: Double = 0.0
)