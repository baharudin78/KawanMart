package com.soldig.kawanmart.model

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cart(
    var totalItems: Int = 0,
    var totalDiscount: Double = 0.0,
    var totalBeforeDiscount: Double = 0.0,
    var totalAfterDiscount: Double = 0.0
) : Parcelable{

    @IgnoredOnParcel
    var orderList: LinkedHashMap<String, Order> = linkedMapOf()



}