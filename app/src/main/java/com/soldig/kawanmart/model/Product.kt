package com.soldig.kawanmart.model

import android.os.Parcelable
import com.google.gson.Gson
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.datasource.DataSourceConst
import com.soldig.kawanmart.utils.Constant
import kotlinx.parcelize.Parcelize


sealed class Product(
    var posId: String,
    val productId: String,
    val categoryId: String,
    val img: String,
    val title: String,
    var qty: Int,
    var sellPrice: Double,
    var sellAfterDiscountPrice: Double,
    var purchasePrice: Double,
    var discountType: DiscountType,
    var discountValue: Double,
    var isActive: Boolean,
    var isPilihan: Boolean,
    val isOwned: Boolean,
    var ordering: Int? = null,
    val packSize: String = ""
) {

    companion object {
        fun buildFromParcelableProduct(parcelableProduct: ParcelableProduct) : NotPOSProduct {
            with(parcelableProduct){
                return NotPOSProduct(
                    posId,
                    productId,
                    categoryId,
                    img,
                    title,
                    qty,
                    sellPrice,
                    sellAfterDiscountPrice,
                    purchasePrice,
                    discountType,
                    discountValue,
                    isActive,
                    isPilihan,
                    isOwned,
                    ordering,
                    packSize
                )
            }
        }

        fun buildFromParcelableProduct(parcelableProduct: ParcelableProduct, id : String) : POSProduct {
            with(parcelableProduct){
                return POSProduct(
                    id = id,
                    posId = posId,
                    productId = productId,
                    categoryId = categoryId,
                    img = img,
                    title = title,
                    qty = qty,
                    sellPrice = sellPrice,
                    sellAfterDiscountPrice = sellAfterDiscountPrice,
                    purchasePrice = purchasePrice,
                    discountType = discountType,
                    discountValue = discountValue,
                    isActive = isActive,
                    isPilihan = isPilihan,
                    isOwned = isOwned,
                    ordering = ordering,
                    packSize = packSize
                )
            }
        }
    }

    class POSProduct(
        val id: String,
        posId: String,
        productId: String,
        categoryId: String,
        img: String,
        title: String,
        qty: Int,
        sellPrice: Double,
        sellAfterDiscountPrice: Double,
        purchasePrice: Double,
        discountType: DiscountType,
        discountValue: Double,
        isActive: Boolean,
        isPilihan: Boolean,
        isOwned: Boolean,
        ordering: Int? = null,
        packSize: String = ""
    ) : Product(
        posId,
        productId,
        categoryId,
        img,
        title,
        qty,
        sellPrice,
        sellAfterDiscountPrice,
        purchasePrice,
        discountType,
        discountValue,
        isActive,
        isPilihan,
        isOwned,
        ordering,
        packSize
    ) {
        fun copy(): POSProduct { //Get another instance of YourClass with the values like this!
            val json = Gson().toJson(this)
            return Gson().fromJson(json, POSProduct::class.java)
        }
    }
    class NotPOSProduct(
        posId: String,
        productId: String,
        categoryId: String,
        img: String,
        title: String,
        qty: Int,
        sellPrice: Double,
        sellAfterDiscountPrice: Double,
        purchasePrice: Double,
        discountType: DiscountType,
        discountValue: Double,
        isActive: Boolean,
        isPilihan: Boolean,
        isOwned: Boolean,
        ordering: Int? = null,
        packSize: String = ""
    ) : Product(
        posId,
        productId,
        categoryId,
        img,
        title,
        qty,
        sellPrice,
        sellAfterDiscountPrice,
        purchasePrice,
        discountType,
        discountValue,
        isActive,
        isPilihan,
        isOwned,
        ordering,
        packSize
    )


    fun getImageURL(): String = DataSourceConst.BASE_IMAGE_URL + img

    fun getDisountString(): String {
        return when (discountType) {
            DiscountType.Nothing -> "Tidak Ada diskon"
            DiscountType.Nominal -> "Potongan ${discountValue.toMoneyFormat(Constant.PREFIX_RP)}"
            DiscountType.Percentage -> "$discountValue%"
        }
    }

    /*
    @Return DiscountValue in money form
        Discount has 3 form of discount , the percentage, nominal, and Nothing
        IF Discount
            is Nominal -> Return DiscountValue itself
            is Percentage -> Return SellAfterDiscountPrice - SellPrice
            is Nothing -> 0.0 / Zero
     */
    fun getDiscountInMoneyForm(): Double {
        return when (discountType) {
            DiscountType.Nominal -> discountValue
            DiscountType.Percentage -> sellPrice - sellAfterDiscountPrice
            DiscountType.Nothing -> 0.0
        }
    }
}

@Parcelize
data class ParcelableProduct(
    var id: String? = null,
    var posId: String,
    val productId: String,
    val categoryId: String,
    val img: String,
    val title: String,
    var qty: Int,
    var sellPrice: Double,
    var sellAfterDiscountPrice: Double,
    var purchasePrice: Double,
    var discountType: DiscountType,
    var discountValue: Double,
    var isActive: Boolean,
    var isPilihan: Boolean,
    val isOwned: Boolean,
    var ordering: Int? = null,
    val packSize: String = ""
) : Parcelable {

    companion object {
        fun buildFromProduct(product: Product): ParcelableProduct {
            with(product) {
                return ParcelableProduct(
                    if(product is Product.POSProduct) product.id else null,
                    posId,
                    productId,
                    categoryId,
                    img,
                    title,
                    qty,
                    sellPrice,
                    sellAfterDiscountPrice,
                    purchasePrice,
                    discountType,
                    discountValue,
                    isActive,
                    isPilihan,
                    isOwned,
                    ordering,
                    packSize
                )
            }

        }
    }

}


