package com.soldig.kawanmart.model

import com.soldig.kawanmart.model.Schedule

data class SettingModel(
    var isDeliveryAvailable: Boolean,
    var scheduleList : List<Schedule>
)