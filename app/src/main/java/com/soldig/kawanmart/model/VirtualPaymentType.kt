package com.soldig.kawanmart.model

enum class VirtualPaymentType {
    PraBayar,
    PascaBayar
}