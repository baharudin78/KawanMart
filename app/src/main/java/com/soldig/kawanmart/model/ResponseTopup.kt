package com.soldig.kawanmart.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ResponseTopup(

	@field:SerializedName("Response")
	val response: String? = null,

	@field:SerializedName("Message")
	val message: String? = null,

	@field:SerializedName("DFResult")
	val dFResult: List<Any?>? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		TODO("dFResult")
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(response)
		parcel.writeString(message)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<ResponseTopup> {
		override fun createFromParcel(parcel: Parcel): ResponseTopup {
			return ResponseTopup(parcel)
		}

		override fun newArray(size: Int): Array<ResponseTopup?> {
			return arrayOfNulls(size)
		}
	}
}
