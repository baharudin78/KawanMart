package com.soldig.kawanmart.model

import android.os.Parcelable
import com.soldig.kawanmart.common.toMoneyFormat
import kotlinx.parcelize.Parcelize

@Parcelize
class TransactionDetail(
    val id:String,
    val orderNumber: String,
    val status: String,
    val paymentWith: String,
    val date: String,
    val productList: List<TransactionProductDetail>,
    val totalItem: Int,
    val totalPaid: Double,
    val totalChange: Double,
    val totalDiscount: Double,
    val totalBeforeDiscount: Double,
    var totalAfterDiscount: Double,
    var buyerName: String? = null,
    var buyerPhoneNumber: String? = null,
    var buyerAddress: String? = null,
    var statusType: TransactionStatus,

    ) : Parcelable

@Parcelize
data class TransactionProductDetail(
    val productId: String,
    val name: String,
    val jml: Int,
    val priceAfterDiscount: Double = 0.0,
    val priceBeforeDiscount: Double = 0.0,
    val discountValue: Double = 0.0
) : Parcelable {

    fun getTotalDiscountString() : String{
        if(priceBeforeDiscount + discountValue == priceAfterDiscount){
            return  "${discountValue.toMoneyFormat()}"
        }
        return "${discountValue}%"
    }
}