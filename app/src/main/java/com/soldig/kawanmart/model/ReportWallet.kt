package com.soldig.kawanmart.model

data class ReportWallet(
    val total: Double,
    val name: String
)