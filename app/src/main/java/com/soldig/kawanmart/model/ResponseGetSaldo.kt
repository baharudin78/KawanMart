package com.soldig.kawanmart.model

import com.google.gson.annotations.SerializedName

data class ResponseGetSaldo(

	@field:SerializedName("Response")
	val response: String? = null,

	@field:SerializedName("Message")
	val message: String? = null,

	@field:SerializedName("Data")
	val data: List<DataItemSaldo>? = null
)

data class DataItemSaldo(

	@field:SerializedName("SaldoWarung")
	val saldoWarung: String? = null,

	@field:SerializedName("NamaWarung")
	val namaWarung: String? = null,

	@field:SerializedName("IdWarung")
	val idWarung: String? = null,
)
