package com.soldig.kawanmart.model

enum class VirtualType {
    Listrik,
    Pulsa,
    Air
}