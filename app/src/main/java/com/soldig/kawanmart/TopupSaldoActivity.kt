package com.soldig.kawanmart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.soldig.kawanmart.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TopupSaldoActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topup_saldo)
    }
}