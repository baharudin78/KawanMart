package com.soldig.ayogrosir.utils

import android.app.Dialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.util.Log
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.util.*


class TimePickerFragment : DialogFragment(), OnTimeSetListener {
    private lateinit var listener: OnTimePicker
    private var type = 1
    private var date: String = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = activity as OnTimePicker
        } catch (e: ClassCastException) {
            Log.e("TAG", "onAttach: " + e.message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments?.getInt("type", 1) as Int
        date = arguments?.getString("date") as String
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current time as the default values for the picker
        val c: Calendar = Calendar.getInstance()
        val hour: Int = c.get(Calendar.HOUR_OF_DAY)
        val minute: Int = c.get(Calendar.MINUTE)

        // Create a new instance of TimePickerDialog and return it
        return TimePickerDialog(
            activity,
            this, hour, minute,
            is24HourFormat(activity)
        )
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        val time = if (minute < 10) {
            "$hourOfDay:0$minute"
        } else {
            "$hourOfDay:$minute"
        }
        listener.onTimeSelect(date, time, type)
    }

    companion object {
        const val OPEN = 1
        const val CLOSE = 2

        fun newInstance(date: String = "", type: Int): TimePickerFragment {
            val f = TimePickerFragment()
            val args = Bundle()
            args.putInt("type", type)
            args.putString("date", date)
            f.arguments = args
            return f
        }
    }

    interface OnTimePicker {
        fun onTimeSelect(date: String, time: String, type: Int)
    }
}