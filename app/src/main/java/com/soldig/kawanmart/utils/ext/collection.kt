package com.soldig.ayogrosir.utils.ext

inline fun <T, R,> Iterable<T>.filterThenMap(predicate: (T) -> Boolean, transform: (T) -> R): List<R> {
    return filterThenMap(ArrayList<R>(), predicate, transform)
}
inline fun <T, R, C: MutableCollection<in R>>
        Iterable<T>.filterThenMap(collection: C, predicate: (T) -> Boolean,
                                  transform: (T) -> R): C {
    for(element in this) {
        if (predicate(element)) {
            collection.add(transform(element))
        }
    }
    return collection
}


inline fun <T, R,> Iterable<T>.filterThenMapIndexed(predicate: ( Int,T) -> Boolean, transform: ( Int, Int, T) -> R): List<R> {
    return filterThenMapIndexed(ArrayList<R>(), predicate, transform)
}
inline fun <T, R, C: MutableCollection<in R>>
        Iterable<T>.filterThenMapIndexed(collection: C, predicate: (Int,T) -> Boolean,
                                  transform: (Int, Int ,T) -> R): C {

    var filtered = 0
    this.forEachIndexed{ index, item ->
        if(predicate(index, item)){
            collection.add(transform(index, filtered, item))
            filtered++
        }
    }
    return collection
}

