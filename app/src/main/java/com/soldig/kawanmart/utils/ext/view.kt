package com.soldig.kawanmart.utils.ext

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun View.goVisible() {
    visibility = View.VISIBLE
}

fun View.goGone() {
    visibility = View.GONE
}

fun View.goInvisible() {
    visibility = View.INVISIBLE
}

fun View.toggleVisibility(isVisible: Boolean) = if(isVisible) goVisible() else goGone()

fun View.showLongSnackbar(message:String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun View.showShortSnackbar(message:String){
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}
