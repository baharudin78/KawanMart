package com.soldig.kawanmart.utils.validator

import android.content.res.Resources
import com.soldig.kawanmart.R

object PinValidator {

    private fun blankValidator(input : String,resources: Resources) : InputValidation<String> {
        if(input.isNotBlank()){
            return InputValidation.Success("")
        }
        return InputValidation.Error(resources.getString(R.string.input_pin_blank))
    }



    fun validate(input: String, resources: Resources) : InputValidation<String> {
        return blankValidator(input,resources)
    }


}

object PhoneNumberValidator {

    private fun containSpace(input: String, resources: Resources) : InputValidation<String>{
        if(input.contains(" ")){
            return InputValidation.Error(resources.getString(R.string.input_phone_contains_space))
        }
        return InputValidation.Success("")
    }

    private fun blankValidator(input : String,resources : Resources) : InputValidation<String> {
        if(input.isNotBlank()){
            return InputValidation.Success("")
        }
        return InputValidation.Error(resources.getString(R.string.input_phone_blank))
    }


    fun validate(input: String, resources: Resources) : InputValidation<String> {
        return blankValidator(input,resources) then containSpace(input, resources)
    }


}


object CommonStringValidator {

    fun blankValidator(input : String,onErrorString : String) : InputValidation<String> {
        if(input.isNotBlank()){
            return InputValidation.Success("")
        }
        return InputValidation.Error(onErrorString)
    }

}

object DoubleValidator {

    fun maximumThenValidator(input: Double, maximum: Double, errorMessage: String) : InputValidation<String>{
        return if(input > maximum) InputValidation.Error(errorMessage) else InputValidation.Success("")
    }

    fun minimumThenValidator(input: Double, minimum: Double, errorMessage: String) : InputValidation<String>{
        println("Input adalah ${input} minimum adalah $minimum hasil adalah ${input < minimum}")
        return if(input < minimum) InputValidation.Error(errorMessage) else InputValidation.Success("")
    }
}
