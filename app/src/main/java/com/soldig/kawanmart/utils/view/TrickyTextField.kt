package com.soldig.kawanmart.utils.view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.soldig.kawanmart.utils.validator.InputValidation
import com.soldig.kawanmart.utils.validator.ValidationCallback

class TrickyTextField(
    context: Context,
    attributeSet: AttributeSet? = null
) : TextInputEditText(context, attributeSet){

    private var validationCallback : ValidationCallback<String, String>? = null

    private var afterTextChangedListener : ((input : String) -> Unit)? = null

    private var textInputLayout : TextInputLayout? = null

    private var textWatcher : TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            afterTextChangedListener?.let{
                it(s.toString())
            }
            val validation = validationCallback ?: return
            when(val result = validation.validator(s.toString())){
                is InputValidation.Success -> {
                    textInputLayout?.error = null
                    validation.onValidationDone(s.toString() , true)
                }
                is InputValidation.Error -> {
                    textInputLayout?.error = result.t
                    validation.onValidationDone(s.toString() , false)
                }
            }
        }

    }

    init{
        addTextChangedListener(textWatcher)
    }

    fun setValidator(
        validationCallback: ValidationCallback<String, String>,
        textInputLayout: TextInputLayout? = null
    ){
        this.textInputLayout = textInputLayout
        this.validationCallback = validationCallback
    }

    fun setAfterTextChangeListener( listener: (input : String) -> Unit  ){
        afterTextChangedListener = listener
    }

    fun isInputValid(): Boolean {
        val validation = validationCallback ?: return true
        val input = text.toString()
        return when(validation.validator(input)){
            is InputValidation.Success -> {
                true
            }
            is InputValidation.Error -> {
                false
            }
        }
    }
}