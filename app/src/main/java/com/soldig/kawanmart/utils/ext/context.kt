package com.soldig.ayogrosir.utils.ext

import android.content.Context
import android.view.LayoutInflater
import androidx.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.soldig.kawanmart.R
import com.soldig.kawanmart.databinding.DialogInformationBinding
import com.soldig.kawanmart.utils.ext.goGone

fun Context.launchOneChoiceDialog(
    title:String,
    choiceList: List<String>,
    initialSection : Int = 0,
    @StringRes positiveButtonText: Int = R.string.dialog_choice_default,
    @StringRes negativeButtonText: Int = R.string.dialog_choice_cancel,
    onPositiveButton : (materialDialog: MaterialDialog, selectedIndex : Int) -> Unit
) : MaterialDialog {
    return MaterialDialog(this)
        .show {

            title(text = title)
            listItemsSingleChoice(items = choiceList, initialSelection = initialSection){  dialog, index, text ->
                onPositiveButton(dialog,index)
            }
            positiveButton(positiveButtonText)
            negativeButton(negativeButtonText){
                it.dismiss()
            }
        }
}

fun Context.launchInformationDialog(
    message: String,
    @StringRes positiveButtonText: Int = R.string.yes,
    onPositiveButton : (materialDialog: MaterialDialog) -> Unit = {
        it.dismiss()
    }) : MaterialDialog {
    return MaterialDialog(this)
        .show {
            val dialogBinding =  DialogInformationBinding.inflate(LayoutInflater.from(this.view.context),this.view,false)
            customView(view = dialogBinding.root)
            with(dialogBinding){
                tvTitle.text = message
                tvMessage.goGone()
                btnYes.setOnClickListener { onPositiveButton(this@show) }
            }

        }

}