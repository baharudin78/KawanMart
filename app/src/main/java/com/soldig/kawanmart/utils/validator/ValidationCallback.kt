package com.soldig.kawanmart.utils.validator

interface ValidationCallback<Input,Result>{
    fun validator(input : Input) : InputValidation<Result>
    fun onValidationDone(input: Input, isValid : Boolean)
}