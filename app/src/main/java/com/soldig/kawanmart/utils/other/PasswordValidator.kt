package com.soldig.kawanmart.utils.other

import android.content.SharedPreferences
import com.scottyab.aescrypt.AESCrypt
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class PasswordValidator @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val prefEditor: SharedPreferences.Editor
) {

    companion object{
        const val PASSWORD_KEY = "com.soldig.amanahmart.password_key"
        const val ENCRYPT_PASS = "%@!g8u4QYUhQm-^X"
        const val PASSOWRD_KEY_ENCRPY_PASS = "r!KaNJNe2Dr*89kt"
    }

    suspend fun addNewPassword(newPassword:String){
        val encryptedPassword = AESCrypt.encrypt(ENCRYPT_PASS, newPassword)
        prefEditor.putString(getPasswordKey(), encryptedPassword)
        prefEditor.apply()
    }

    suspend fun deleteCurrentPassword(){
        prefEditor.remove(getPasswordKey())
        prefEditor.apply()
    }

    private suspend fun getPasswordKey() : String = AESCrypt.encrypt(PASSOWRD_KEY_ENCRPY_PASS, PASSWORD_KEY)


    suspend fun invalidatePassword(password:String) : Boolean{
        val oldPassword = sharedPreferences.getString(getPasswordKey(), null) ?: return false
        val decryptedOldPassword = AESCrypt.decrypt(ENCRYPT_PASS, oldPassword)
        return decryptedOldPassword == password
    }
}