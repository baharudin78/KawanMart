package com.soldig.ayogrosir.utils.ext

import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody


fun Map<String, String?>?.bodyBuilder(): MultipartBody {
    return MultipartBody.Builder().apply {
        setType("application/json".toMediaType())

        if (this@bodyBuilder.isNullOrEmpty()) {
            addFormDataPart("", "") //minimal one form part
        }

        this@bodyBuilder?.forEach { map ->
            map.value?.let { addFormDataPart(map.key, it) }
        }
    }.build()
}

