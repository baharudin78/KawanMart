package com.soldig.kawanmart.utils.other

import android.content.Context
import android.widget.Toast
import com.soldig.kawanmart.interactor.MessageType

sealed class UIInteraction(val message : MessageType) {

    object DoNothing : UIInteraction(MessageType.StringMessage(""))

    data class GenericMessage(val genericMessage : MessageType) : UIInteraction(genericMessage)

    data class ShowToast(val toastMessage: MessageType,var toastTime: Int = Toast.LENGTH_SHORT) : UIInteraction(toastMessage) {
        init {
            if (toastTime != 0 && toastTime != 1) {
                this.toastTime = 0
            }
        }
    }

    data class Dialog(val dialogType: DialogType,val _message: MessageType) : UIInteraction(_message)

    fun getMessage(context: Context) : String{
        return when(message){
            is MessageType.StringMessage -> {
                message.message
            }
            is MessageType.ResourceMessage -> {
                context.getString(message.messageStringRes)
            }
        }
    }
}
sealed class DialogType{
    data class ActionButton(val onPostive : () -> Unit, val onNegative : () -> Unit)
}
