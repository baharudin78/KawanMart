package com.soldig.ayogrosir.utils.ext

import android.content.res.Resources
import android.util.TypedValue

fun Float.toDips(resources: Resources) : Int =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, resources.displayMetrics).toInt()