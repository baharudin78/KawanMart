package com.soldig.ayogrosir.utils.ext

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun translateDate(defaultLocale: Locale, translateLocale: Locale, date : String) : String{
    val local =  Locale.ENGLISH
    val currentLocal = Locale("id", "ID")
    val simpleDateFormat = SimpleDateFormat("EEEE", defaultLocale)
    val parsedDate = simpleDateFormat.parse(date)
    return SimpleDateFormat("EEEE",translateLocale).format(parsedDate)
}

fun getDayOfweek(date : String) : Int{
    val currentLocal = Locale("id", "ID")
    val simpleDateFormat = SimpleDateFormat("EE", currentLocal)
    val parsedDate = simpleDateFormat.parse(date)
    return Calendar.getInstance().apply {
        time = parsedDate
    }.get(Calendar.DAY_OF_WEEK)
}

fun String.capitilizeFirstWord() : String{
    val split = this.split(" ")
    var total = ""
    split.forEachIndexed { index, s ->
        val firstWord = s[0].uppercase()
        total += s.replaceRange(0, 1, firstWord)
        if(index != split.lastIndex){
            total += " "
        }
    }
    return total
}

fun String.toBooleanOrNull() : Boolean? {
    val input = this.lowercase()
    return input.toBooleanStrictOrNull()
}


