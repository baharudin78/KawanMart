package com.soldig.kawanmart.interactor.category

import com.soldig.kawanmart.datasource.network.abstraction.CategoryNetworkDataSource
import com.soldig.kawanmart.model.ProductCategory
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetAllCategory
@Inject constructor(
    private val categoryNetworkDataSource: CategoryNetworkDataSource
){
    fun fetch(
        token: String
    ) : Flow<Resource<List<ProductCategory>>> = networkOnlyCall {
        categoryNetworkDataSource.getAllCategory(token)
    }
}