package com.soldig.kawanmart.interactor.transaction

import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.model.TransactionDetail
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkFirstCall
import com.soldig.kawanmart.model.POS
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetTransactionDetail
@Inject constructor(
    private val cacheDataSource: TransactionCacheDataSource,
    private val networkDataSource: TransactionNetworkDataSource
){

    fun fetch(
        pos: POS,
        id: String
    ) : Flow<Resource<TransactionDetail>> = networkFirstCall(
        fetch = {
            networkDataSource.getTransactionDetail(pos.token, id)
        },
        loadFromDB = {
            cacheDataSource.getTransactionDetail(id)
        }
    )

}