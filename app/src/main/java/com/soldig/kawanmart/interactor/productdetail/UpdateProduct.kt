package com.soldig.kawanmart.interactor.productdetail

import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.interactor.*
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ViewModelScoped
class UpdateProduct
@Inject constructor(
    private val networkDataSource: ProductNetworkDataSource,
    private val cacheDataSource: ProductCacheDataSource
){

    fun fetch(
        pos: POS,
        product: Product
    ) : Flow<Resource<String>> = flow {
        try {
            emit(Resource.Loading())
            val lastTimeUpdateFromCache = safeCacheCall {
                cacheDataSource.getLastProductUpdate(pos.phoneNumber)
            }
            var lastTimeUpdateFromServer =  safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            if(lastTimeUpdateFromCache != lastTimeUpdateFromServer){
                val newestProductList = safeApiCall {
                    networkDataSource.getAllProductList(pos)
                }
                safeCacheCall {
                    cacheDataSource.insertAll(newestProductList)
                }
            }
            val result = safeApiCall {
                networkDataSource.updateProduct(pos.token, product)
            }
            lastTimeUpdateFromServer = safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            safeCacheCall {
                cacheDataSource.upsertOne(result)
                cacheDataSource.updateLastProductUpdate(pos.phoneNumber, lastTimeUpdateFromServer)
            }
            emit(Resource.Success(result.id, DataSource.Network))
        } catch (exception: ResourceError) {
            emit(
                Resource.Error<String>(
                    uiInteraction = UIInteraction.GenericMessage(
                        exception.errorType
                    )
                )
            )
        }
    }.flowOn(Dispatchers.IO)


//    fun fetch(
//        token: String,
//        product: Product
//    ) : Flow<Resource<String>> = networkOnlyCall {
//        productNetworkDataSource.updateProduct(token, product)
//    }

}