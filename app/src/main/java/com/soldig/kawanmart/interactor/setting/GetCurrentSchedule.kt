package com.soldig.kawanmart.interactor.setting

import com.soldig.kawanmart.datasource.network.abstraction.ScheduleNetworkDataSource
import com.soldig.kawanmart.model.SettingModel
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetCurrentSchedule
@Inject constructor(
    private val scheduleNetworkDataSource: ScheduleNetworkDataSource
){


    fun fetch(
        token: String
    ) : Flow<Resource<SettingModel>> = networkOnlyCall{
        scheduleNetworkDataSource.getScheduleAndDeliveryStatus(token)
    }


}