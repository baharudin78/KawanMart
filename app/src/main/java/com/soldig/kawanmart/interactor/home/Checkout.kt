package com.soldig.kawanmart.interactor.home

import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkFirstCall
import com.soldig.kawanmart.interactor.safeApiCall
import com.soldig.kawanmart.model.CheckoutReturn
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.model.Transaction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class Checkout
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource,
    private val productNetworkDataSource: ProductNetworkDataSource,
    private val productCacheDataSource: ProductCacheDataSource
){
    fun fetch(
        pos: POS,
        transaction: Transaction
    ) : Flow<Resource<CheckoutReturn>> = networkFirstCall(
        loadFromDB = {
            val transactionResult = transactionCacheDataSource.checkout(pos.phoneNumber,transaction)
            CheckoutReturn(
                transactionResult = transactionResult,
                isCheckoutDoneInServer = false
            )
        },
        fetch = {
            getPOSProductFromTransaction(transaction)
            val transactionResult = transactionNetworkDataSource.checkout(pos.token,transaction)
            val lastTimeUpdateFromServerAfterUpdate = safeApiCall {
                productNetworkDataSource.getProductLastUpdate(pos.token)
            }
            productCacheDataSource.updateLastProductUpdate(pos.phoneNumber, lastTimeUpdateFromServerAfterUpdate)
            productCacheDataSource.insertAll(getPOSProductFromTransaction(transaction))
            CheckoutReturn(
                transactionResult = transactionResult,
                isCheckoutDoneInServer = true
            )
        }
    )

    private fun getPOSProductFromTransaction(transaction: Transaction) : List<Product.POSProduct> {
        return transaction.cart.orderList.map {
            val newProduct = it.value.product.posProduct
            newProduct
        }
    }
}