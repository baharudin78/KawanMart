package com.soldig.kawanmart.interactor.topup_virtual

import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import com.soldig.kawanmart.model.ResponseTopup
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@ViewModelScoped
class TopupVirtualUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource,
) {
    suspend fun topUp(token : String, param: HashMap<String, @JvmSuppressWildcards RequestBody>, partFile: MultipartBody.Part?)
    : Flow<Resource<ResponseTopup>> = networkOnlyCall{
        userNetworkDataSource.topupVirtual(token, param, partFile)
    }
}