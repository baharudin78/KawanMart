package com.soldig.kawanmart.interactor.transaction

import com.soldig.kawanmart.interactor.transaction.CancelTransaction
import com.soldig.kawanmart.interactor.transaction.GetTransactionDetail
import com.soldig.kawanmart.interactor.transaction.GetTransactionHistory
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TransactionInteractor
@Inject constructor(
    val getTransactionHistory: GetTransactionHistory,
    val getTransactionDetail : GetTransactionDetail,
    val cancelTransaction: CancelTransaction
)