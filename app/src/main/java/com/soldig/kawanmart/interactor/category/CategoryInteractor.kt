package com.soldig.kawanmart.interactor.category

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class CategoryInteractor
@Inject constructor(
    val getAllCategory: GetAllCategory,
    val getProductByCategory: GetProductByCategory
)