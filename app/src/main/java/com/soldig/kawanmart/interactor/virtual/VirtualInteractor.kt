package com.soldig.kawanmart.interactor.virtual

import com.soldig.kawanmart.interactor.virtual.DoPayment
import com.soldig.kawanmart.interactor.virtual.GetHistory
import com.soldig.kawanmart.interactor.virtual.GetVirtualProductList
import com.soldig.kawanmart.interactor.virtual.PostPaidCheck
import javax.inject.Inject

class VirtualInteractor @Inject constructor(
    val getVirtualProductList: GetVirtualProductList,
    val postPaidCheck: PostPaidCheck,
    val doPayment: DoPayment,
    val getHistory: GetHistory
) {
}