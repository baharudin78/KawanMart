package com.soldig.kawanmart.interactor.topup_virtual

import com.soldig.kawanmart.interactor.topup_virtual.GetSaldoUser
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class GetSaldoUserInteractor
@Inject constructor(
    val getSaldoUser: GetSaldoUser
){
}