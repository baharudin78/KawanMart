package com.soldig.kawanmart.interactor.home

import com.soldig.kawanmart.datasource.cache.abstraction.PaymentCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.PaymentNetworkDataSource
import com.soldig.kawanmart.model.PaymentCategory
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkBoundResource
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetPaymentMethods
@Inject constructor(
    private val paymentNetworkDataSource: PaymentNetworkDataSource,
    private val paymentCacheDataSource: PaymentCacheDataSource
){

    fun fetch(
        token: String
    ) : Flow<Resource<List<PaymentCategory>>> = networkBoundResource(
        loadFromDB =  {
            paymentCacheDataSource.getAllPaymentCategory()
        },
        shouldFetch = {
            true
        },
        saveFetchResult = { fromApi, fromDb ->
            paymentCacheDataSource.insertPaymentCategories(fromApi)
        },
        fetch = {
            paymentNetworkDataSource.getAllPaymentMethod(token)
        }
    )

}