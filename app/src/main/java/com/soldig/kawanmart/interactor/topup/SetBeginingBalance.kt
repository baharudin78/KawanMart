package com.soldig.kawanmart.interactor.topup

import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

@ViewModelScoped
class SetBeginingBalance
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource,
){
    fun setBeginingBalance(
        balance: Double,
        token: String
    ): Flow<Resource<Boolean>> = networkOnlyCall{
        withContext(IO){
            val setBalanceTransaction = async { userNetworkDataSource.setStartingBalance(balance,token) }
            val updateFCMTransaction = async {  userNetworkDataSource.updateFCM(token) }
            updateFCMTransaction.await()
            setBalanceTransaction.await()
        }
    }

}