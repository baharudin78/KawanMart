package com.soldig.kawanmart.interactor.productdetail

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class ProductDetailInteractor
@Inject constructor(
    val updateProduct: UpdateProduct,
    val getAllAvailableProduct: GetAllAvailableProduct
){
}