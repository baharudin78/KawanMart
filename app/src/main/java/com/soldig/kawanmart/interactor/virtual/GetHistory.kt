package com.soldig.kawanmart.interactor.virtual

import com.soldig.kawanmart.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.kawanmart.model.VirtualProductHistory
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetHistory @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
) {

    fun fetch(
        token: String
    ) : Flow<Resource<List<VirtualProductHistory>>> = networkOnlyCall {
        digiFlazzNetworkDataSource.getTransactionHistory(token)
    }

}