package com.soldig.kawanmart.interactor.schedule

import com.soldig.kawanmart.datasource.network.abstraction.ScheduleNetworkDataSource
import com.soldig.kawanmart.model.Schedule
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


@ViewModelScoped
class ChangeSchedule
@Inject constructor(
    private val scheduleNetworkDataSource: ScheduleNetworkDataSource
){

    fun fetch(
        token: String,
        schedule: Schedule
    ) : Flow<Resource<String>> = networkOnlyCall{
        scheduleNetworkDataSource.postSchedule(token,schedule)
    }

}