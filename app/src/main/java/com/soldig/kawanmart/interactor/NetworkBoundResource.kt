package com.soldig.kawanmart.interactor

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.soldig.ayogrosir.utils.connectivity.DoesNetworkHaveInternet
import com.soldig.kawanmart.R
import com.soldig.kawanmart.utils.other.UIInteraction
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import org.json.JSONObject
import retrofit2.HttpException
import javax.net.SocketFactory

const val UNKNOWN_ERROR = "Error tidak diketahui"

inline fun <T> networkFirstCall(
    crossinline loadFromDB: suspend () -> T,
    crossinline fetch: suspend () -> T,
    crossinline errorSpesificHandler: (errortype: MessageType.StringMessage) -> Resource<T> = {
        Resource.Error<T>(uiInteraction = UIInteraction.GenericMessage(it))
    }
) = flow {
    try {
        var dataSource = DataSource.Network
        val data = try{
            val result = safeApiCall { fetch() }
            result
        }catch (exception : ResourceError){
            dataSource = DataSource.Cache
            safeCacheCall { loadFromDB() }
        }
        emit(Resource.Success(data = data, dataSource = dataSource))
    } catch (exception: ResourceError) {
        emit(
            when (val messageType = exception.errorType) {
                is MessageType.StringMessage -> errorSpesificHandler(messageType)
                is MessageType.ResourceMessage -> Resource.Error<T>(
                    uiInteraction = UIInteraction.GenericMessage(
                        messageType
                    )
                )
            }
        )
    }
}.flowOn(IO)


inline fun <T> networkBoundResource(
    crossinline loadFromDB: suspend () -> T,
    crossinline fetch: suspend () -> T,
    crossinline saveFetchResult: suspend (fromApi : T, fromDb : T) -> Unit,
    crossinline shouldFetch: (T) -> Boolean = { true },
    crossinline errorSpesificHandler: (errortype: MessageType.StringMessage) -> Resource<T> = {
        Resource.Error<T>(uiInteraction = UIInteraction.GenericMessage(it))
    }
) = flow {
    try {
        val start = System.currentTimeMillis()
        var dataSource = DataSource.Cache
        emit(Resource.Loading<T>())
        var data = safeCacheCall {
            loadFromDB()
        }
        if (shouldFetch(data)) {
            try{
                val apiCall = safeApiCall { fetch() }
                dataSource = DataSource.NetworkAndCache
                saveFetchResult(apiCall ,data)
                data = safeCacheCall {
                    loadFromDB()
                }
            }catch (err : Exception){

            }
        }
        println("Total Time dapet dari networkbound resource : ${System.currentTimeMillis() - start}")
        emit(Resource.Success(data = data, dataSource = dataSource))
    } catch (exception: ResourceError) {
        emit(
            when (val messageType = exception.errorType) {
                is MessageType.StringMessage -> errorSpesificHandler(messageType)
                is MessageType.ResourceMessage -> Resource.Error<T>(
                    uiInteraction = UIInteraction.GenericMessage(
                        messageType
                    )
                )
            }
        )
    }
}.flowOn(IO)

inline fun <T> cacheOnlyCall(
    crossinline errorSpesificHandler: (errortype: MessageType.StringMessage) -> Resource<T> = {
        Resource.Error<T>(uiInteraction = UIInteraction.GenericMessage(it))
    },
    crossinline fetch: suspend () -> T
) = flow {
    try {
        emit(Resource.Loading<T>())
        emit(Resource.Success(data = safeCacheCall {
            fetch()
        }, dataSource = DataSource.Cache))

    } catch (exception: ResourceError) {

        emit(
            when (val messageType = exception.errorType) {
                is MessageType.StringMessage -> errorSpesificHandler(messageType)
                is MessageType.ResourceMessage -> Resource.Error<T>(
                    uiInteraction = UIInteraction.GenericMessage(
                        messageType
                    )
                )
            }
        )

    }
}.flowOn(IO)

inline fun <T> networkOnlyCall(
    crossinline errorSpesificHandler: (errortype: MessageType.StringMessage) -> Resource<T> = {
        Resource.Error<T>(uiInteraction = UIInteraction.GenericMessage(it))
    },
    crossinline fetch: suspend () -> T
) = flow {
    try {
        emit(Resource.Loading<T>())
        val result = safeApiCall {
            fetch()
        }
        emit(Resource.Success(data =  result, dataSource = DataSource.Network))
    } catch (exception: ResourceError) {
        FirebaseCrashlytics.getInstance().recordException(exception)
        emit(
            when (val messageType = exception.errorType) {
                is MessageType.StringMessage -> errorSpesificHandler(messageType)
                is MessageType.ResourceMessage -> Resource.Error<T>(
                    uiInteraction = UIInteraction.GenericMessage(
                        messageType
                    )
                )
            }
        )

    }
}.flowOn(IO)

suspend fun <T> safeApiCall(
    apiCall: suspend () -> T
): T {
    return withContext(IO) {
        try {
            // throws TimeoutCancellationException
            withTimeout(2000000) {
                val result = apiCall()
                result
            }
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            FirebaseCrashlytics.getInstance().recordException(throwable)
            val messageType = when (throwable) {
                is TimeoutCancellationException -> {
                    MessageType.ResourceMessage(R.string.network_timeout_error)
                }
                is HttpException -> {
                    convertErrorBody(throwable)?.let {
                        MessageType.StringMessage(it)
                    }
                        ?:  MessageType.ResourceMessage(R.string.network_unknown_error)
                }
                else -> {
                    MessageType.StringMessage(throwable.localizedMessage)
                }
            }
            throw ResourceError(messageType, ErrorType.FromNetwork)
        }
    }
}

suspend fun <T> safeCacheCall(
    cacheCall: suspend () -> T
): T {
    return withContext(IO) {
        try {
            // throws TimeoutCancellationException
            withTimeout(2000000) {
                val result = cacheCall()
                result
            }
        } catch (throwable: Throwable) {
            val log = "Gagal di cache dengan log ${throwable.message}"
            FirebaseCrashlytics.getInstance().log(log)
            throwable.printStackTrace()
            val message = when (throwable) {
                is TimeoutCancellationException -> {
                    MessageType.ResourceMessage(R.string.cache_timeout_error)
                }
                else -> {
                    MessageType.ResourceMessage(R.string.cache_unknown_error)
                }
            }
            throw ResourceError(message, ErrorType.FromCache)
        }
    }
}


private fun convertErrorBody(throwable: HttpException): String? {
    return try {
        val errorBody = throwable.response()?.errorBody() ?: return throwable.message()
        JSONObject(errorBody.charStream().readText())["Message"].toString()
    } catch (exception: Exception) {
        throwable.message()
    }
}

suspend fun isNetworkAvailable(): Boolean {
    return withContext(IO) {
        try {
            withTimeout(200000) {
                DoesNetworkHaveInternet.execute(SocketFactory.getDefault())
            }
        } catch (e: Exception) {
            println("Error : $e")
            false
        }
    }

}