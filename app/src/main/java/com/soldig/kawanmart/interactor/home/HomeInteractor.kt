package com.soldig.kawanmart.interactor.home

import com.soldig.kawanmart.interactor.common.GetAllProduct
import com.soldig.kawanmart.interactor.home.Checkout
import com.soldig.kawanmart.interactor.home.GetPaymentMethods
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class HomeInteractor
@Inject constructor(
    val getAllProduct: GetAllProduct,
    val getPaymentMethods: GetPaymentMethods,
    val checkout: Checkout
){

}