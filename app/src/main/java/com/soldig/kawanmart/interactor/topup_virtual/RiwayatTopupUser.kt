package com.soldig.kawanmart.interactor.topup_virtual

import com.soldig.kawanmart.model.ResponseRiwayatTopup
import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class RiwayatTopupUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){
    fun fetch(
        token: String,
        tgl1: String,tgl2: String,status: String
    ) : Flow<Resource<ResponseRiwayatTopup>> = networkOnlyCall {
        userNetworkDataSource.getRiwayatSaldo(token, tgl1, tgl2, status)
    }
}