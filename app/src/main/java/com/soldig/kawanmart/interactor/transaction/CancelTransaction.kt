package com.soldig.kawanmart.interactor.transaction

import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.model.TransactionStatus
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class CancelTransaction
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource
){

    fun fetch(
        token : String,
        id : String
    ) : Flow<Resource<String>> = networkOnlyCall {
        transactionNetworkDataSource.updateTransactionStatus(token, id, TransactionStatus.DIBATALKAN)
    }

}