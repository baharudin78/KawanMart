package com.soldig.kawanmart.interactor.category

import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import com.soldig.kawanmart.model.POS
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetProductByCategory @Inject constructor(
    private val productNetworkDataSource: ProductNetworkDataSource
){
    fun fetch(
        pos: POS,
        productId: String
    ) : Flow<Resource<List<Product>>> = networkOnlyCall {
        productNetworkDataSource.getAllProductByCategory(pos, productId)
    }
}