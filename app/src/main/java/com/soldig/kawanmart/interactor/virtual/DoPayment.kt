package com.soldig.kawanmart.interactor.virtual

import com.soldig.kawanmart.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.kawanmart.model.SelectedVirtualProduct
import com.soldig.kawanmart.model.VirtualProductResult
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DoPayment @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
){

    fun fetch(
        token: String,
        selectedVirtualProduct: SelectedVirtualProduct
    ) : Flow<Resource<VirtualProductResult>> = networkOnlyCall {
        digiFlazzNetworkDataSource.payment(token, selectedVirtualProduct)
    }

}