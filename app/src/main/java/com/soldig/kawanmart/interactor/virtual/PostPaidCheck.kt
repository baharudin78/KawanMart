package com.soldig.kawanmart.interactor.virtual

import com.soldig.kawanmart.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.kawanmart.interactor.networkOnlyCall
import javax.inject.Inject

class PostPaidCheck @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
) {

    fun fetch(
        token: String,
        skuCode: String,
        number: String
    ) = networkOnlyCall {
        digiFlazzNetworkDataSource.checkPostpaid(token, skuCode, number)
    }

}