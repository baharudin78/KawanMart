package com.soldig.kawanmart.interactor.setting

import com.soldig.kawanmart.interactor.setting.GetCurrentSchedule
import com.soldig.kawanmart.interactor.setting.SetDeliveryStatus
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class SettingInteractor
@Inject constructor(
    val getCurrentSchedule: GetCurrentSchedule,
    val setDeliveryStatus: SetDeliveryStatus
){
}