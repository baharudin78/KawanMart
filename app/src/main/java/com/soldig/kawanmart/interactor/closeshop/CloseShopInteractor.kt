package com.soldig.kawanmart.interactor.closeshop

import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject


@ViewModelScoped
class CloseShopInteractor
@Inject constructor(
    val closeShop: CloseShop,
    val getReport: GetReport
)