package com.soldig.kawanmart.interactor.transaction

import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.model.TransactionHistory
import com.soldig.kawanmart.interactor.DataSource
import com.soldig.kawanmart.interactor.MessageType
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkFirstCall
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetTransactionHistory
@Inject constructor(
    private val transactionNetworkDataSource : TransactionNetworkDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource
){
    private  val emptyList = "Cannot find any data"



    fun fetch(
        pos: POS,
        startDate: Date? = null,
        endDate: Date? = null
    ): Flow<Resource<List<TransactionHistory>>> = networkFirstCall(
        loadFromDB = {
            transactionCacheDataSource.getAllTransactionHistory(pos.phoneNumber)
        },
        fetch = {
            transactionNetworkDataSource.getAllTransactionHistory(pos.token , startDate, endDate)

        },
        errorSpesificHandler = { serverMessage ->
            if(serverMessage.message == emptyList){
                Resource.Success(listOf(), DataSource.Network)
            }else{
                Resource.Error(UIInteraction.GenericMessage(MessageType.StringMessage(serverMessage.message)))
            }

        }
    )



}