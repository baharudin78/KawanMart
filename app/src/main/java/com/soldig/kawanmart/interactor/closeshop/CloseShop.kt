package com.soldig.kawanmart.interactor.closeshop

import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class CloseShop
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
) {
    fun fetch(
        token: String
    ) : Flow<Resource<String>> = networkOnlyCall {
        userNetworkDataSource.closePOS(token)
    }
}