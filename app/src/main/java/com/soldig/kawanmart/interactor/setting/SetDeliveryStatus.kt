package com.soldig.kawanmart.interactor.setting

import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class SetDeliveryStatus
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){

    fun fetch(
        token: String,
        isDeliveryAvailable: Boolean
    ) : Flow<Resource<String>> = networkOnlyCall {
        userNetworkDataSource.setDeliveryStatus(token,isDeliveryAvailable)
    }

}