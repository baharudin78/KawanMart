package com.soldig.kawanmart.interactor.login

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.soldig.kawanmart.datasource.cache.abstraction.POSCacheDataSource
import com.soldig.kawanmart.R
import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.MessageType
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import com.soldig.kawanmart.interactor.safeCacheCall
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.User
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class UserLogin
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource,
    private val posCacheDataSource: POSCacheDataSource
) {
    private val errorList = hashMapOf(
        "Cannot find any data" to R.string.login_fail_no_data
    )
    fun login(
        user: User
    ): Flow<Resource<POS>> = networkOnlyCall(
        errorSpesificHandler = { serverMessage ->
            val messageRes = errorList[serverMessage.message] ?: R.string.login_unkown_error
            Resource.Error(UIInteraction.GenericMessage(MessageType.ResourceMessage(messageRes)))
        }
    ) {
        val pos = userNetworkDataSource.login(user)
        val log = "Hasil login dari network adalah ${pos.name}  ${pos.loginTime} ${pos.alamat}"
        FirebaseCrashlytics.getInstance().log(log)
        safeCacheCall {
            posCacheDataSource.insertPOS(pos)
        }
        pos
    }
}