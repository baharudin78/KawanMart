package com.soldig.kawanmart.interactor.common

import android.util.Log
import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.interactor.*
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ViewModelScoped
class GetAllProduct
@Inject constructor(
    private val networkDataSource: ProductNetworkDataSource,
    private val cacheDataSource: ProductCacheDataSource
) {

    fun fetch(
        pos: POS,
        isNetworkPriorty: Boolean = false
    ): Flow<Resource<List<Product>>> = flow {
        try {
            emit(Resource.Loading())
            val lastTimeUpdateFromServer = safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            val lastTimeUpdateFromCache = if (isNetworkPriorty) "ANOTHER_UNKOWN_STRING" else
                safeCacheCall {
                    cacheDataSource.getLastProductUpdate(pos.phoneNumber)
                }
            if (lastTimeUpdateFromServer != lastTimeUpdateFromCache) {
                val result = safeApiCall {
                    networkDataSource.getAllProductList(pos)
                }
//                val result =  networkDataSource.getAllProductList(pos)
                safeCacheCall {
                    cacheDataSource.insertAll(result)
                    cacheDataSource.updateLastProductUpdate(
                        pos.phoneNumber,
                        lastTimeUpdateFromServer
                    )
                }
                emit(Resource.Success(result, DataSource.NetworkAndCache))
                Log.w("KaWaNSAD XX 1", "product/list: "+ "fwef")
            } else {
                Log.w("KaWaNSAD XX 2", "product/list: "+ "222222222")
                emit(Resource.Success(safeCacheCall {
                    cacheDataSource.getAll(pos.phoneNumber)
                }, DataSource.NetworkAndCache))
            }
        } catch (exception: ResourceError) {
            when (exception.dataSource) {
                ErrorType.FromNetwork -> {
                    try {
                        emit(Resource.Success(safeCacheCall {
                            cacheDataSource.getAll(pos.phoneNumber)
                        }, DataSource.Cache))
                    } catch (exception: ResourceError) {
                        emit(
                            Resource.Error<List<Product>>(
                                uiInteraction = UIInteraction.GenericMessage(
                                    exception.errorType
                                )
                            )
                        )
                    }
                    Log.w("KaWaNSAD XX 3", "product/list: "+ exception.errorType.toString())

                }
                else -> {
                    emit(
                        Resource.Error<List<Product>>(
                            uiInteraction = UIInteraction.GenericMessage(
                                exception.errorType
                            )
                        )
                    )
                    Log.w("KaWaNSAD XX 4", "product/list: "+ exception.errorType.toString())
                }
            }
        }
    }.flowOn(IO)

    fun fetchFromDB(
        pos: POS,
    ): Flow<Resource<List<Product>>> = flow {
        try{
            emit(Resource.Success(safeCacheCall {
                cacheDataSource.getAll(pos.phoneNumber)
            }, DataSource.Cache))

            Log.w("Data ada berapaaaaa", "XXXXXXXXXXXX")
        }catch (exception : ResourceError){
            emit(
                Resource.Error<List<Product>>(
                    uiInteraction = UIInteraction.GenericMessage(
                        exception.errorType
                    )
                )
            )
            Log.w("Data ada berapaaaaa", "YYYYYYYYY")
        }
    }.flowOn(IO)

    suspend fun newFetch(
        pos: POS,
        isNetworkPriorty: Boolean = false
    ): Flow<Resource<List<Product>>> = flow {
        try {
            emit(Resource.Loading())
            val lastTimeUpdateFromServer = safeApiCall {
                networkDataSource.getProductLastUpdate(pos.token)
            }
            val lastTimeUpdateFromCache = if (isNetworkPriorty) "ANOTHER_UNKOWN_STRING" else
                safeCacheCall {
                    cacheDataSource.getLastProductUpdate(pos.phoneNumber)
                }
            if (lastTimeUpdateFromServer != lastTimeUpdateFromCache) {
                val result = safeApiCall {
                    networkDataSource.getAllProductList(pos)
                }
//                val result =  networkDataSource.getAllProductList(pos)
                safeCacheCall {
                cacheDataSource.insertAll(result)
                cacheDataSource.updateLastProductUpdate(
                    pos.phoneNumber,
                    lastTimeUpdateFromServer
                )
                }
                emit(Resource.Success(result, DataSource.NetworkAndCache))
                Log.w("KaWaNSAD XX 1aaaa","product/list: "+ "fwef")
            } else {
                Log.w("KaWaNSAD XX 2aaaa","product/list: "+ "222222222")
                emit(Resource.Success(safeCacheCall {
                    cacheDataSource.getAll(pos.phoneNumber)
                }, DataSource.NetworkAndCache))
            }
        } catch (exception: ResourceError) {
            when (exception.dataSource) {
                ErrorType.FromNetwork -> {
                    try {
                        emit(Resource.Success(safeCacheCall {
                            cacheDataSource.getAll(pos.phoneNumber)
                        }, DataSource.Cache))
                    } catch (exception: ResourceError) {
                        emit(
                            Resource.Error<List<Product>>(
                                uiInteraction = UIInteraction.GenericMessage(
                                    exception.errorType
                                )
                            )
                        )
                    }
                    Log.w("KaWaNSAD XX 3aaaa","product/list: "+ exception.errorType.toString())

                }
                else -> {
                    emit(
                        Resource.Error<List<Product>>(
                            uiInteraction = UIInteraction.GenericMessage(
                                exception.errorType
                            )
                        )
                    )
                    Log.w("KaWaNSAD XX 4aaaa","product/list: "+ exception.errorType.toString())
                }
            }
        }
    }.flowOn(IO)
//    fun fetch(
//        pos: POS
//    ): Flow<Resource<List<Product>>> = networkBoundResource(
//        loadFromDB = {
//            val start = System.currentTimeMillis()
//            val result = cacheDataSource.getAll(pos.phoneNumber)
//            FirebaseCrashlytics.getInstance().log("Dapat dari db ${result.map { it.title }}")
//            println("Total Time dapet data dari db : ${System.currentTimeMillis() - start}")
//            result
//        },
//        fetch = {
//            val start = System.currentTimeMillis()
//            val result = networkDataSource.getAllProductList(pos)
//            FirebaseCrashlytics.getInstance().log("Dapat dari network ${result.map { it.title }}")
//            println("Total Time dapet dari server : ${System.currentTimeMillis() - start}")
//            result
//        },
//        saveFetchResult = { fromApi, fromCache ->
//            val start = System.currentTimeMillis()
//            cacheDataSource.insertAll(fromApi)
//            println("Total Time nyimpen data ke db : ${System.currentTimeMillis() - start}")
//        },
//        shouldFetch = {
//            true
//        }
//    )

//    fun fetch(
//        pos: POS
//    ) : Flow<Resource<List<Product>>> = networkOnlyCall (
//        fetch = {
//            val result = networkDataSource.getAllProductList(pos)
//            FirebaseCrashlytics.getInstance().log("Dapat dari network ${result.map { it.title }}")
//            result
//        }
//    )

//    fun fetch(
//        pos: POS
//    ) : Flow<Resource<List<Product>>> = cacheOnlyCall  (
//        fetch = {
//            val result = cacheDataSource.getAll(pos.phoneNumber)
//            FirebaseCrashlytics.getInstance().log("Dapat dari network ${result.map { it.title }}")
//            result
//        }
//    )
}