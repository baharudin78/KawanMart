package com.soldig.kawanmart.interactor.schedule

import com.soldig.kawanmart.interactor.schedule.ChangeSchedule
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class ScheduleInteractor
@Inject constructor(
    val changeSchedule: ChangeSchedule,
){
}