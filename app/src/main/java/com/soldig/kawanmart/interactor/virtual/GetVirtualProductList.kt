package com.soldig.kawanmart.interactor.virtual

import com.soldig.kawanmart.datasource.network.impl.DigiFlazzNetworkDataSource
import com.soldig.kawanmart.model.VirtualProduct
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetVirtualProductList @Inject constructor(
    private val digiFlazzNetworkDataSource: DigiFlazzNetworkDataSource
){

    fun fetch(
        token: String
    ) : Flow<Resource<VirtualProduct>> = networkOnlyCall {
        digiFlazzNetworkDataSource.getPriceList(token)
    }

}