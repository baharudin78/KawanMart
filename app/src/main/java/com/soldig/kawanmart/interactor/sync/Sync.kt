package com.soldig.kawanmart.interactor.sync

import com.soldig.kawanmart.datasource.cache.abstraction.POSCacheDataSource
import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.interactor.safeApiCall
import com.soldig.kawanmart.interactor.safeCacheCall
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Sync
@Inject constructor(
    private val posCacheDataSource: POSCacheDataSource,
    private val transactionCacheDataSource: TransactionCacheDataSource,
    private val transactionNetworkDataSource: TransactionNetworkDataSource
){
    suspend fun fetch(){
        withContext(IO){
            val posList = safeCacheCall { posCacheDataSource.getAllPos() }
            posList.forEach {
                val allPosTransaction = safeCacheCall { transactionCacheDataSource.getAllTransactionForSync(it.phoneNumber) }
                safeApiCall { transactionNetworkDataSource.checkoutSync(it.token, allPosTransaction) }
                safeCacheCall { transactionCacheDataSource.deleteAllPOSTransaction(it.phoneNumber) }
            }
        }
    }

}