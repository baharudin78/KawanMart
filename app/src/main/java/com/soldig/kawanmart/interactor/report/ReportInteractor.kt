package com.soldig.kawanmart.interactor.report

import com.soldig.kawanmart.interactor.report.GetReport
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class ReportInteractor
@Inject constructor(
    val getReport: GetReport
){
}