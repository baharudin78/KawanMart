package com.soldig.kawanmart.interactor.order


import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.model.TransactionHistory
import com.soldig.kawanmart.model.TransactionStatus
import com.soldig.kawanmart.R
import com.soldig.kawanmart.interactor.DataSource
import com.soldig.kawanmart.interactor.MessageType
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetAllOrderList
@Inject constructor(
    private val transactionNetworkDataSource: TransactionNetworkDataSource
) {

    private  val emptyList = "Cannot find any data"
    fun fetch(
        token: String,
        startDate: Date? = null,
        endDate: Date? = null,
        status: TransactionStatus? = null
    ) : Flow<Resource<List<TransactionHistory>>> = networkOnlyCall(
        errorSpesificHandler = { serverMessage ->
            if(serverMessage.message == emptyList){
                Resource.Success(listOf(), DataSource.Network)
            }else{
                Resource.Error(UIInteraction.GenericMessage(MessageType.ResourceMessage(R.string.login_unkown_error)))
            }
        }
    ){
        transactionNetworkDataSource.getOrderOnlyTransaction(token,startDate,endDate,status)
    }

}