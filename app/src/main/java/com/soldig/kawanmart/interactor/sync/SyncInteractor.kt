package com.soldig.kawanmart.interactor.sync

import com.soldig.kawanmart.interactor.sync.Sync
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SyncInteractor @Inject constructor(
    val sync: Sync
)