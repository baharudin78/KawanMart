package com.soldig.kawanmart.interactor.topup_virtual

import com.soldig.kawanmart.model.ResponseGetSaldo
import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class GetSaldoUser
@Inject constructor(
    private val userNetworkDataSource: UserNetworkDataSource
){
    fun fetch(
        token: String
    ) : Flow<Resource<ResponseGetSaldo>> = networkOnlyCall {
        userNetworkDataSource.getSaldo(token)
    }
}