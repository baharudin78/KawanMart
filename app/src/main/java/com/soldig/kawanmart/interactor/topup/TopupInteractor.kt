package com.soldig.kawanmart.interactor.topup

import com.soldig.kawanmart.interactor.topup.SetBeginingBalance
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TopupInteractor
@Inject constructor(
    private val setBeginingBalance: SetBeginingBalance
){
}