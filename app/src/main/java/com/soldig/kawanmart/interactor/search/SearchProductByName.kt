package com.soldig.kawanmart.interactor.search

import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.cacheOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class SearchProductByName
@Inject constructor(
    private val productCacheDataSource: ProductCacheDataSource
){

    fun fetch(
        nameKey: String,
        phoneNumber : String
    ): Flow<Resource<List<Product>>> = cacheOnlyCall {
        productCacheDataSource.getProductByName(nameKey,phoneNumber)
    }


}