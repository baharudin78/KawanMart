package com.soldig.kawanmart.interactor.report

import com.soldig.kawanmart.datasource.network.abstraction.ReportNetworkDataSource
import com.soldig.kawanmart.model.Report
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.interactor.networkOnlyCall
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@ViewModelScoped
class GetReport
@Inject constructor(
    private val reportNetworkDataSource: ReportNetworkDataSource
){

    fun fetch(
        token: String,
        startDate: Date?,
        endDate: Date?
    ) : Flow<Resource<Report>> = networkOnlyCall {
        reportNetworkDataSource.getReport(token,startDate,endDate)
    }

}