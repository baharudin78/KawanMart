package com.soldig.kawanmart.di

import com.google.firebase.messaging.FirebaseMessaging
import com.soldig.kawanmart.datasource.cache.abstraction.PaymentCacheDataSource
import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.datasource.cache.impl.PaymentCacheDataSourceImpl
import com.soldig.kawanmart.datasource.cache.impl.ProductCacheDataSourceImpl
import com.soldig.kawanmart.datasource.cache.mapper.POSProductEntityMapper
import com.soldig.kawanmart.datasource.cache.mapper.PaymentCategoryEntityMapper
import com.soldig.kawanmart.datasource.cache.mapper.PaymentMethodEntityMapper
import com.soldig.kawanmart.datasource.cache.dao.POSDao
import com.soldig.kawanmart.datasource.cache.dao.POSProductDao
import com.soldig.kawanmart.datasource.cache.dao.PaymentDao
import com.soldig.kawanmart.datasource.cache.dao.ProductDao
import com.soldig.kawanmart.datasource.network.abstraction.*
import com.soldig.kawanmart.datasource.network.impl.*
import com.soldig.kawanmart.datasource.network.mapper.*
import com.soldig.kawanmart.datasource.network.service.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object ViewModelModule{

    @Provides
    @ViewModelScoped
    fun provideUserNetworkDataSource(userService: UserService, userNetworkMapper: UserNetworkMapper, firebaseMessaging: FirebaseMessaging
    ) : UserNetworkDataSource = UserNetworkDataSourceImpl(
        userService,
        userNetworkMapper,
        firebaseMessaging)

    @Provides
    @ViewModelScoped
    fun provideScheduleNetworkDataSource(scheduleService: ScheduleService, scheduleNetworkMapper: ScheduleNetworkMapper) : ScheduleNetworkDataSource = ScheduleNetworkDataSourceImpl(scheduleService, scheduleNetworkMapper)

//    @Provides
//    @ViewModelScoped
//    fun provideProductNetworkDataSource(
//        productService: ProductService,
//        productNetworkMapper: ProductNetworkMapper
//    ): ProductNetworkDataSource = ProductNetworkDataSourceImpl(productService, productNetworkMapper)
//

    @Provides
    @ViewModelScoped
    fun provideProductCacheDataSource(
        productDao: ProductDao,
        posProductDao: POSProductDao,
        productEntityMapper: POSProductEntityMapper,
        posDao: POSDao
    ) : ProductCacheDataSource = ProductCacheDataSourceImpl(productDao, posProductDao, productEntityMapper, posDao)



    @Provides
    @ViewModelScoped
    fun providePaymentNetworkDataSource(
        paymentService: PaymentService,
        paymentCategoryNetworkMapper: PaymentCategoryNetworkMapper
    ) : PaymentNetworkDataSource = PaymentNetworkDataSourceImpl(paymentService, paymentCategoryNetworkMapper)



    @Provides
    @ViewModelScoped
    fun providePaymentCacheDataSource(
        paymentMethodEntityMapper: PaymentMethodEntityMapper,
        paymentCategoryEntityMapper: PaymentCategoryEntityMapper,
        paymentDao: PaymentDao
    ) : PaymentCacheDataSource = PaymentCacheDataSourceImpl(paymentCategoryEntityMapper, paymentMethodEntityMapper, paymentDao)


    @Provides
    @ViewModelScoped
    fun provideCategoryDataSource(
        categoryNetworkMapper: ProductCategoryNetworkMapper,
        categoryService: CategoryService
    ) : CategoryNetworkDataSource = ProductCategoryNetworkDataSourceImpl(categoryService =  categoryService, productCategoryNetworkMapper = categoryNetworkMapper)

    @Provides
    @ViewModelScoped
    fun provideReportNetworkDataSource(
        reportNetworkMapper: ReportNetworkMapper,
        reportService: ReportService
    ) : ReportNetworkDataSource = ReportNetworkDataSourceImpl(reportService, reportNetworkMapper)

}