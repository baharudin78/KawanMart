package com.soldig.kawanmart.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.soldig.kawanmart.datasource.cache.abstraction.POSCacheDataSource
import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.cache.dao.POSDao
import com.soldig.kawanmart.datasource.cache.dao.TransactionDao
import com.soldig.kawanmart.datasource.cache.dao.TransactionProductDao
import com.soldig.kawanmart.datasource.cache.database.AppDatabase
import com.soldig.kawanmart.datasource.cache.impl.POSCacheDataSourceImpl
import com.soldig.kawanmart.datasource.cache.impl.TransactionCacheDataSourceImpl
import com.soldig.kawanmart.datasource.cache.mapper.TransactionForSyncEntityMapper
import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.datasource.network.impl.ProductNetworkDataSourceImpl
import com.soldig.kawanmart.datasource.network.impl.TransactionNetworkDataSourceImpl
import com.soldig.kawanmart.datasource.network.mapper.ProductNetworkMapper
import com.soldig.kawanmart.datasource.network.mapper.TransactionDetailNetworkMapper
import com.soldig.kawanmart.datasource.network.mapper.TransactionForSyncNetworkMapper
import com.soldig.kawanmart.datasource.network.mapper.TransactionHistoryNetworkMapper
import com.soldig.kawanmart.interactor.sync.SyncInteractor
import com.soldig.kawanmart.KawanMartApp
import com.soldig.kawanmart.R
import com.soldig.kawanmart.datasource.DataSourceConst.BASE_URL
import com.soldig.kawanmart.datasource.network.service.*
import com.soldig.kawanmart.worker.MyWorkerFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): KawanMartApp {
        return app as KawanMartApp
    }

    @Singleton
    @Provides
    fun provideFirebaseMessageing(): FirebaseMessaging {
        return FirebaseMessaging.getInstance()
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): SharedPreferences {
        return context
            .getSharedPreferences(
                context.getString(R.string.file_pref_key),
                Context.MODE_PRIVATE
            )
    }

    @Singleton
    @Provides
    fun provideSharedPreferencesEditor(
        sharedPreferences: SharedPreferences
    ): SharedPreferences.Editor {
        return sharedPreferences.edit()
    }

    @Singleton
    @Provides
    fun provideGsonBuilder() : Gson {
        return GsonBuilder()
            .serializeNulls()
            .create()
    }


    @Singleton
    @Provides
    fun provideOkHttpCliendHeader(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS )

        val basic = HttpLoggingInterceptor()
        basic.setLevel(HttpLoggingInterceptor.Level.BASIC )

        val bodyIntreceptor = HttpLoggingInterceptor()
        bodyIntreceptor.setLevel(HttpLoggingInterceptor.Level.BODY )

        return OkHttpClient.Builder().apply {
            addNetworkInterceptor(interceptor)
//            addNetworkInterceptor(basic)
//            addNetworkInterceptor(bodyIntreceptor)
            connectTimeout(30, TimeUnit.MINUTES)
            readTimeout(30, TimeUnit.MINUTES)
        }.build()
    }

    @Singleton
    @Provides
    fun provideRetrofitBuilder(gson: Gson,okHttpClient: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }


    @Provides
    @Singleton
    fun provideProductApi(
        retrofit: Retrofit.Builder
    ): ProductService = retrofit.build().create(ProductService::class.java)



    @Provides
    @Singleton
    fun provideScheduleApi(
        retrofit: Retrofit.Builder
    ): ScheduleService = retrofit.build().create(ScheduleService::class.java)



    @Provides
    @Singleton
    fun provideUserApi(
        retrofit: Retrofit.Builder
    ) : UserService = retrofit.build().create(UserService::class.java)


    @Provides
    @Singleton
    fun providePaymentApi(
        retrofit: Retrofit.Builder
    ) : PaymentService = retrofit.build().create(PaymentService::class.java)


    @Provides
    @Singleton
    fun provideTransactionApi(
        retrofit: Retrofit.Builder
    ) : TransactionService = retrofit.build().create(TransactionService::class.java)

    @Provides
    @Singleton
    fun provideDigiFlazzApi(
        retrofit: Retrofit.Builder
    ) : DigiFlazzService = retrofit.build().create(DigiFlazzService::class.java)


    @Provides
    @Singleton
    fun provideCategoryApi(
        retrofit: Retrofit.Builder
    ) : CategoryService = retrofit.build().create(CategoryService::class.java)

    @Provides
    @Singleton
    fun provideDb(@ApplicationContext appContext: Context): AppDatabase {
        return Room
            .databaseBuilder(appContext, AppDatabase::class.java, AppDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideReport(
        retrofit: Retrofit.Builder
    ) : ReportService = retrofit.build().create(ReportService::class.java)

    @Provides
    @Singleton
    fun provideProductDao(
        db: AppDatabase
    ) = db.productDao()

    @Provides
    @Singleton
    fun providePaymentDao(
        db: AppDatabase
    ) = db.paymentDao()


    @Provides
    @Singleton
    fun providePOSProductDao(
        db: AppDatabase
    ) = db.posProductDao()

    @Provides
    @Singleton
    fun providePOSDao(
        db: AppDatabase
    ) = db.posDao()


    @Provides
    @Singleton
    fun provideTransactionDao(
        db: AppDatabase
    ) = db.transactionDao()

    @Provides
    @Singleton
    fun provideTransactionProductDao(
        db: AppDatabase
    ) = db.transactionProductDao()

    @Singleton
    @Provides
    fun provideWorkerFactory(
        syncInteractor: SyncInteractor
    ) : MyWorkerFactory {
        return MyWorkerFactory(syncInteractor)
    }


    @Provides
    @Singleton
    fun provideTransactionDataSource(
        transactionDao: TransactionDao,
        transactionProductDao: TransactionProductDao,
        transactionForSyncEntityMapper: TransactionForSyncEntityMapper
    ) : TransactionCacheDataSource = TransactionCacheDataSourceImpl(transactionDao, transactionProductDao, transactionForSyncEntityMapper)


    @Provides
    @Singleton
    fun provideTransactionNetworkDataSource(
        transactionService: TransactionService,
        transactionHistoryNetworkMapper: TransactionHistoryNetworkMapper,
        transactionDetailNetworkMapper: TransactionDetailNetworkMapper,
        transactionForSyncNetworkMapper: TransactionForSyncNetworkMapper
    ) : TransactionNetworkDataSource = TransactionNetworkDataSourceImpl(transactionService,transactionHistoryNetworkMapper,transactionDetailNetworkMapper,transactionForSyncNetworkMapper)


    @Provides
    @Singleton
    fun providePOSCacheDataSource(posDao: POSDao) : POSCacheDataSource = POSCacheDataSourceImpl(posDao)


    @Provides
    @Singleton
    fun provideProductNetworkDataSource(
        productService: ProductService,
        productNetworkMapper: ProductNetworkMapper
    ): ProductNetworkDataSource = ProductNetworkDataSourceImpl(productService, productNetworkMapper)

}