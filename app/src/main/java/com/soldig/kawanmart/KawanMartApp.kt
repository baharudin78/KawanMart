package com.soldig.kawanmart

import android.util.Log
import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import com.soldig.kawanmart.worker.MyWorkerFactory
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class KawanMartApp : MultiDexApplication(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: MyWorkerFactory
    override fun getWorkManagerConfiguration(): Configuration =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .setMinimumLoggingLevel(Log.VERBOSE)
            .build()

}