package com.soldig.kawanmart.presentation.features.produkdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseLoggedInActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProdukDetailActivity : BaseLoggedInActivity() {

    companion object {
        const val PRODUCT_EXTRAS_KEY =
            "com.soldig.amanahmart.presentation.feature.detail.ProductDetailActivity.PRODUCT_EXTRAS_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_produk_detail)
    }
}