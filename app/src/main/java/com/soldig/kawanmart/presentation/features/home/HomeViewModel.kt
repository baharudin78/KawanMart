package com.soldig.kawanmart.presentation.features.home

import android.util.Log
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.soldig.kawanmart.interactor.home.HomeInteractor
import com.soldig.kawanmart.presentation.common.productlist.ProductListHorizontalItemType
import com.soldig.kawanmart.presentation.common.productlist.ProductListViewType
import com.soldig.kawanmart.KawanMartApp
import com.soldig.kawanmart.base.BaseViewModel
import com.soldig.kawanmart.interactor.DataSource
import com.soldig.kawanmart.model.*
import com.soldig.kawanmart.utils.other.SingleEvent
import com.soldig.kawanmart.worker.CheckoutWorker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private var homeInteractor: HomeInteractor,
    private val application : KawanMartApp
) : BaseViewModel<HomeViewState>(){

    private val workManager = WorkManager.getInstance(application)
    companion object {
        const val SYNC_UNIQUE_WORKER_NAME = "CHECKOUT_SYNC_WORK_MANAGER"
    }

    override fun initNewViewState(): HomeViewState {
        return HomeViewState()
    }

    fun fetchProductList(pos : POS)  {
        setViewState(getCurrentViewStateOrNew().apply {
            productSection.dataSource
        })
        viewModelScope.launch(IO) {
            homeInteractor.getAllProduct.fetchFromDB(pos).collect{
                onCollect(
                    response = it,
                    onLoading = {
                        setProductLoadingState(it)
                    },
                    executeOnSuccess = {result ->
                        setProductList(result.data, result.dataSource)
                    }
                )
            }
        }
    }
    private fun fetchProductFromServer(pos : POS) {
        viewModelScope.launch(IO) {
            homeInteractor.getAllProduct.fetch(pos).collect{
                onCollect(
                    response = it,
                    onLoading = {
                        setProductLoadingState(it)
                    },
                    executeOnSuccess = { result ->
                        setProductList(result.data, result.dataSource)
                    }
                )
            }
        }
    }
    private fun setProductLoadingState(isLoading : Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            productSection.isFetching = isLoading
        })
    }
    private fun setProductList(list: List<Product>, dataSource: DataSource) {
        viewModelScope.launch(Default) {
            val oldViewState = getCurrentViewStateOrNew()
            val oldCardItem = oldViewState.cartSection.cart.orderList
            val productPilihan = ProductListViewType.produce32EmptyProduct()
            val oldKumpulanList = oldViewState.productSection.getKumpulanFilteredProduct()
            val newList = list.mapIndexed { index, newProduct ->
                val productOrdering = newProduct.ordering
                val product = if(newProduct is Product.POSProduct && oldCardItem.containsKey(newProduct.id)){
                    val oldProduct = oldCardItem[newProduct.id]!!.product
                    if(oldKumpulanList.getOrNull(oldProduct.listIndex) != null){
                        oldKumpulanList[oldProduct.listIndex].productListItem.product
                    }else{
                        newProduct
                    }
                }else{
                    newProduct
                }
                var newProductListItem: HomeViewState.ProductSection.ProductListItem = if (product is Product.POSProduct) {
                    HomeViewState.ProductSection.ProductListItem.POSProductListItem(
                        product,
                        -1,
                        index
                    )
                } else {
                    HomeViewState.ProductSection.ProductListItem.JustProductListItem(
                        product, -1, index
                    )
                }
                if (product.isPilihan && productOrdering != null && productOrdering in 1..32 && product is Product.POSProduct) {
                    newProductListItem = HomeViewState.ProductSection.ProductListItem.POSProductListItem(
                        product,
                        productOrdering - 1,
                        index
                    )
                    productPilihan[productOrdering - 1] =
                        ProductListViewType.Grid(newProductListItem)

                    Log.w("Http http prodlist", productPilihan.size.toString())
                }
                ProductListHorizontalItemType(newProductListItem)
            }
            setViewState(getCurrentViewStateOrNew().apply {
                productSection.dataSource = SingleEvent(dataSource)
                productSection.allProductList = newList
                productSection.pilihanProductList = productPilihan
                productSection.isNewList = SingleEvent(true)
            })
        }
    }
    fun setCurrentProductViewType(currentProductViewType: HomeViewState.ProductSection.ProductViewType) {
        setViewState(getCurrentViewStateOrNew().apply {
            productSection.currentProductViewType = currentProductViewType
        })
    }
    fun checkout(pos: POS, paid: Double, paymentMethod: PaymentMethod) {
        viewModelScope.launch(IO) {
            val newTransaction = Transaction(
                paidMoney = paid,
                paymentMethod = paymentMethod,
                cart = getCurrentViewStateOrNew().cartSection.cart
            )
            homeInteractor.checkout.fetch(
                pos,
                newTransaction
            ).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        setCheckoutLoading(isLoading)
                    },
                    executeOnSuccess = { result ->
                        clearCart()
                        if (!result.data.isCheckoutDoneInServer) {
                            addNetworkManagerRequest()
                        }
                        setCheckoutResult(result.data.transactionResult)
                    }
                )
            }
        }
    }
    private fun addNetworkManagerRequest() {
        val syncRequest = OneTimeWorkRequestBuilder<CheckoutWorker>()
            .setConstraints(
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            )
            .build()
        workManager.enqueueUniqueWork(
            SYNC_UNIQUE_WORKER_NAME,
            ExistingWorkPolicy.KEEP,
            syncRequest
        )
    }

    fun clearCart() {
        setViewState(getCurrentViewStateOrNew().apply {
            cartSection = HomeViewState.CartSection()
        })
    }
    private fun setCheckoutLoading(isLoading: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            with(checkoutSection) {
                isFetching = isLoading
            }
        })
    }
    private fun setCheckoutResult(transactionResult: TransactionDetail) {
        setViewState(getCurrentViewStateOrNew().apply {
            with(checkoutSection) {
                this.transactionResult = SingleEvent(transactionResult)
            }
        })
    }
    fun filterProductKumpulanByName(name: String?) {
        setViewState(getCurrentViewStateOrNew().apply {
            print(name?.lowercase(Locale.getDefault()))
            this.productSection.isKumpuluanFilteredRecently = SingleEvent(true)
            this.productSection.currentKumpulanFilterName = name?.lowercase(Locale.getDefault())
        })
    }
    fun filterProductGridByName(name: String?) {
        setViewState(getCurrentViewStateOrNew().apply {
            print(name?.lowercase(Locale.getDefault()))
            this.productSection.isGridFilteredRecently = SingleEvent(true)
            this.productSection.currentGridFilterName = name?.lowercase(Locale.getDefault())
        })
    }

    fun addProductToCart(productListItem: HomeViewState.ProductSection.ProductListItem.POSProductListItem, isFromGrid: Boolean) {
        viewModelScope.launch(Default) {
            val newViewState = getCurrentViewStateOrNew().apply {
                val product = productListItem.posProduct
                product.apply {
                    qty--
                }
                with(cartSection.cart) {

                    totalItems++
                    totalAfterDiscount += product.sellAfterDiscountPrice
                    totalDiscount += product.getDiscountInMoneyForm()
                    totalBeforeDiscount += product.sellPrice
                    orderList[product.id]?.let { oldOrder ->
                        orderList[product.id] = oldOrder.apply {
                            quantity++
                            currentTotalPrice = product.sellAfterDiscountPrice * quantity
                            this.product.posProduct.apply {
                                qty = product.qty
                            }
                        }
                        println("berhasil nambah dengan ${oldOrder.quantity}")
                    } ?: orderList.put(
                        product.id,
                        Order(productListItem, 1, product.sellAfterDiscountPrice)
                    )
                    println("Udah nambah untuk ${orderList[product.id]?.quantity} ${product.title}")
                }

                if (isFromGrid) {
                    productSection.pilihanProductList =
                        productSection.pilihanProductList.toMutableList().apply {
                            val selectedIndex = productListItem.gridIndex
                            val oldProduct = this.getOrNull(selectedIndex) ?: return@launch
                            if (oldProduct is ProductListViewType.Grid) {
                                val updateQuantityProduct = oldProduct.apply {
                                    this.product.posProduct = product
                                }
                                this[selectedIndex] = updateQuantityProduct
                            }
                        }
                } else {
                    productSection.allProductList =
                        productSection.allProductList.toMutableList().apply {
                            val selectedIndex = productListItem.listIndex
                            val oldProduct = this.getOrNull(selectedIndex) ?: return@launch
                            val updateQuantityProduct = oldProduct.apply {
                                this.productListItem.product = product
                            }
                            this[selectedIndex] = updateQuantityProduct

                        }
                }
                productSection.currentlyUpdatedListIndex = SingleEvent(productListItem.listIndex)
                productSection.currentlyUpdatedGridIndex = SingleEvent(productListItem.gridIndex)

            }
            setViewState(newViewState)
        }
    }

    fun deleteProductFromCart(
        productListItem: HomeViewState.ProductSection.ProductListItem.POSProductListItem,
        amount: Int = 1,
        isFromGrid: Boolean
    ) {
        viewModelScope.launch(Default) {
            val newViewState = getCurrentViewStateOrNew().apply {
                val product = productListItem.posProduct
                product.apply {
                    qty += amount
                }
                with(cartSection.cart) {
                    orderList[product.id]?.let { oldOrder ->
                        totalAfterDiscount -= (product.sellAfterDiscountPrice * amount)
                        totalDiscount -= (product.getDiscountInMoneyForm() * amount)
                        totalBeforeDiscount -= (product.sellPrice * amount)
                        if (oldOrder.quantity == amount) {
                            orderList.remove(oldOrder.product.posProduct.id)
                        } else {
                            orderList[product.id] = oldOrder.apply {
                                quantity -= amount
                                currentTotalPrice = product.sellAfterDiscountPrice * quantity
                                this.product.posProduct.apply {
                                    qty = product.qty
                                }
                            }
                        }

                    }
                }
                if (isFromGrid) {
                    productSection.pilihanProductList =
                        productSection.pilihanProductList.toMutableList().apply {
                            val selectedIndex = productListItem.gridIndex
                            val oldProduct = this.getOrNull(selectedIndex) ?: return@launch
                            if (oldProduct is ProductListViewType.Grid) {
                                val updateQuantityProduct = oldProduct.apply {
                                    this.product.posProduct = product
                                }
                                this[selectedIndex] = updateQuantityProduct
                            }
                        }
                } else {
                    productSection.allProductList =
                        productSection.allProductList.toMutableList().apply {
                            val selectedIndex = productListItem.listIndex
                            val oldProduct = this.getOrNull(selectedIndex) ?: return@launch
                            val updateQuantityProduct = oldProduct.apply {
                                this.productListItem.product = product
                            }
                            this[selectedIndex] = updateQuantityProduct

                        }
                }
                productSection.currentlyUpdatedListIndex = SingleEvent(productListItem.listIndex)
                productSection.currentlyUpdatedGridIndex = SingleEvent(productListItem.gridIndex)
            }
            setViewState(newViewState)
        }

    }

    fun deleteAllProductQtyFromProduct(productListItem: HomeViewState.ProductSection.ProductListItem.POSProductListItem) {
        viewModelScope.launch(Default) {
            val newViewState = getCurrentViewStateOrNew().apply {
                val product = productListItem.posProduct
                productSection.shouldGoToDetail = SingleEvent(product)
                productSection.currentlyUpdatedListIndex = SingleEvent(productListItem.listIndex)
                productSection.currentlyUpdatedGridIndex = SingleEvent(productListItem.gridIndex)
                println("Deleting all product ${product.title}")
                with(cartSection.cart) {
                    orderList[product.id]?.let { oldOrder ->
                        val amount = oldOrder.quantity
                        totalAfterDiscount -= (product.sellAfterDiscountPrice * amount)
                        totalDiscount -= (product.getDiscountInMoneyForm() * amount)
                        totalBeforeDiscount -= (product.sellPrice * amount)
                        if (oldOrder.quantity == amount) {
                            orderList.remove(oldOrder.product.posProduct.id)
                        } else {
                            orderList[product.id] = oldOrder.apply {
                                quantity -= amount
                                currentTotalPrice = product.sellAfterDiscountPrice * quantity
                                this.product.posProduct.apply {
                                    qty = product.qty
                                }
                            }
                        }
                        product.apply {
                            qty += oldOrder.quantity
                        }

                    }
                }
                productSection.allProductList =
                    productSection.allProductList.toMutableList().apply {
                        val selectedIndex = productListItem.listIndex
                        val oldProduct = this.getOrNull(selectedIndex) ?: return@launch
                        val updateQuantityProduct = oldProduct.apply {
                            this.productListItem.product = product
                        }
                        this[selectedIndex] = updateQuantityProduct

                    }
            }
            println("Sebelum set view state ${newViewState.productSection.shouldGoToDetail?.peek()}")
            setViewState(newViewState)
        }

    }

    fun filterProductByCategory(category: ProductCategory? = null) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.productSection.currentKumpulanFilterCategory = category
            this.productSection.isKumpuluanFilteredRecently = SingleEvent(true)
        })
    }
}