package com.soldig.kawanmart.presentation.features.home.payment

import com.soldig.kawanmart.model.PaymentCategory

data class PaymentCategoryListItem(
    val paymentCategory: PaymentCategory,
    var clickedItem: Int = -1,
    var isSelected: Boolean = false
)