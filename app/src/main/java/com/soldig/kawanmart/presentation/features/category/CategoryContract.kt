package com.soldig.kawanmart.presentation.features.category

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.soldig.kawanmart.model.ProductCategory

class CategoryContract : ActivityResultContract<Nothing, ProductCategory?>() {
    override fun createIntent(context: Context, input: Nothing?): Intent =
        Intent(context, KategoryActivity::class.java)

    override fun parseResult(resultCode: Int, intent: Intent?): ProductCategory? {
        return when(resultCode) {
            Activity.RESULT_OK -> {
                intent?.getParcelableExtra<ProductCategory>(KategoryActivity.CATEGORY_CONTRACT_KEY)
            }
            else -> null
        }
    }
}