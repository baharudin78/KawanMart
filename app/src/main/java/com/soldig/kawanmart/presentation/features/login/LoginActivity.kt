package com.soldig.kawanmart.presentation.features.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.soldig.kawanmart.presentation.features.input_saldo.InputSaldoActivity
import com.soldig.kawanmart.base.BaseActivity
import com.soldig.kawanmart.databinding.ActivityLoginBinding
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.User
import com.soldig.kawanmart.utils.ext.showShortSnackbar
import com.soldig.kawanmart.utils.ext.toggleVisibility
import com.soldig.kawanmart.utils.other.UIInteraction
import com.soldig.kawanmart.utils.validator.InputValidation
import com.soldig.kawanmart.utils.validator.PhoneNumberValidator
import com.soldig.kawanmart.utils.validator.PinValidator
import com.soldig.kawanmart.utils.validator.ValidationCallback
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity() {

    private lateinit var binding : ActivityLoginBinding
    private val viewModel : LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initTime(true, binding.toolbarKawanMart)
        observeViewModel()
        initListener()
    }
    private fun observeViewModel() {
        viewModel.viewState.observe(this, { viewState ->
            with(viewState) {
                binding.loading.root.toggleVisibility(isLoading)
                token?.get()?.let {
                    navigateToTopupActivity(it)
                }
            }
        })
        viewModel.userInteraction.observe(this, {
            val interaction = it.get() ?: return@observe
            if (interaction is UIInteraction.GenericMessage) {
                binding.root.showShortSnackbar(interaction.getMessage(this))
            }
        })
    }
    private fun updateButton() {
        with(binding) {
            if (etPhone.isInputValid() && etPin.isInputValid()) {
                btnLogin.isEnabled = true
            }else {
                btnLogin.isEnabled = false
            }
        }
    }
    private fun initListener() {
        with(binding) {
            btnLogin.setOnClickListener {
                var user = User(
                    etPhone.text.toString(),
                    etPin.text.toString()
                )
                viewModel.login(user)
            }
            etPin.setValidator(
                validationCallback = object : ValidationCallback<String, String> {
                    override fun validator(input: String): InputValidation<String> {
                        return PinValidator.validate(input, resources)
                    }

                    override fun onValidationDone(input: String, isValid: Boolean) {
                        updateButton()
                    }
                },
                binding.passwordInputLayout
            )
            etPhone.setValidator(
                validationCallback = object : ValidationCallback<String, String> {
                    override fun validator(input: String): InputValidation<String> {
                        return PhoneNumberValidator.validate(input, resources)
                    }

                    override fun onValidationDone(input: String, isValid: Boolean) {
                        updateButton()
                    }
                },
                binding.emailInputLayout
            )
            btnLogin.isEnabled = false
        }
    }
    private fun navigateToTopupActivity(pos: POS) {
        startActivity(Intent(this, InputSaldoActivity::class.java).apply {
            putExtra(InputSaldoActivity.POS_KEY, pos)
            putExtra(InputSaldoActivity.PASS_KEY, binding.etPin.text.toString())
        })
        finish()
    }

}