package com.soldig.kawanmart.presentation.features.input_saldo

import androidx.lifecycle.viewModelScope
import com.soldig.kawanmart.interactor.home.HomeInteractor
import com.soldig.kawanmart.interactor.splash.SplashSyncInteractor
import com.soldig.kawanmart.interactor.topup.SetBeginingBalance
import com.soldig.kawanmart.base.BaseViewModel
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.other.PasswordValidator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class InputSaldoViewModel @Inject constructor(
    private val setBeginingValue : SetBeginingBalance,
    private val passwordValidator: PasswordValidator,
    private val homeInteractor: HomeInteractor,
    private val syncwithServer : SplashSyncInteractor
) : BaseViewModel<InputSaldoViewState>(){

    override fun initNewViewState(): InputSaldoViewState {
        return InputSaldoViewState()
    }

    private fun setLoading(isLoading : Boolean){
        setViewState(getCurrentViewStateOrNew().apply {
            this.isFetching = isLoading
        })
    }

    private fun setIsSuccess(posPassword : String) {
        viewModelScope.launch(IO) {
            passwordValidator.addNewPassword(posPassword)
            setViewState(getCurrentViewStateOrNew().apply {
                this.inputSaldoIsDone = true
                this.isFetching = true
            })
        }
    }

    fun setBalanceValue(balance : Double) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.inputSaldoValue = balance
        })
    }

    fun procesingInputSaldo(pos : POS, balance : Double, token : String, posPassword : String) {
        setLoading(true)
        syncwithServer(pos, balance, token, posPassword)
    }

    fun posStartingBalance(balance : Double, token : String, posPassword: String) {
        viewModelScope.launch {
            withContext(IO) {
                setBeginingValue.setBeginingBalance(balance, token).collect{
                    onCollect(it) {
                        setIsSuccess(posPassword)
                    }
                }
            }
        }
    }

    fun syncwithServer(pos : POS, balance : Double, token : String, posPassword: String) {
        setLoading(true)
        viewModelScope.launch {
            syncwithServer.fetch(pos).collect{
                when(it) {
                    is Resource.Success -> {
                        fetchProductList(pos, balance, token, posPassword)
                    }
                    is Resource.Error -> {
                        setLoading(false)
                    }
                    else -> {

                    }
                }
            }
        }
    }

    fun fetchProductList(pos: POS, balance : Double, token: String, posPassword: String) {
        viewModelScope.launch(IO) {
            homeInteractor.getAllProduct.fetch(pos).collect{
                onCollect(it) { result ->
                    posStartingBalance(balance, token, posPassword)
                }
            }
        }
    }
}
