package com.soldig.kawanmart.presentation.features.home.payment

import android.os.Parcelable
import com.soldig.kawanmart.model.PaymentCategoryType
import com.soldig.kawanmart.model.PaymentMethod
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class PaymentViewState(
    var isFetching: Boolean = false,
    var currentSelectedMethod: PaymentMethod? = null,
    var paymentType: PaymentCategoryType? = null,
    var selectedPaymentCategoryIndex : Int = -1,
    var minimumPrice: Double = 0.0
) : Parcelable {
    @IgnoredOnParcel
    var paymentList : ArrayList<PaymentCategoryListItem> = ArrayList()
}
