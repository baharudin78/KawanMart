package com.soldig.kawanmart.presentation.features.produkdetail.changediskon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.afollestad.materialdialogs.utils.MDUtil.textChanged
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseDialogFragment
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.FragmentAturDiskonBinding
import com.soldig.kawanmart.model.DiscountType
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.presentation.features.produkdetail.ProdukDetailViewModel
import com.soldig.kawanmart.utils.Constant
import com.soldig.kawanmart.utils.ext.goGone
import com.soldig.kawanmart.utils.ext.goVisible
import com.soldig.kawanmart.utils.validator.CommonStringValidator
import com.soldig.kawanmart.utils.validator.DoubleValidator
import com.soldig.kawanmart.utils.validator.InputValidation
import com.soldig.kawanmart.utils.validator.then
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ChangeDiskonFragment : BaseDialogFragment() {

    private var _binding : FragmentAturDiskonBinding ?= null
    private val binding get() = _binding!!

    private val activityViewModel by activityViewModels<ProdukDetailViewModel>()
    private val viewModel by viewModels<ChangeDiskonViewModel>()
    private lateinit var mListener : OnChangeDiscountListener

    fun setListener(listener : OnChangeDiscountListener) {
        mListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAturDiskonBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeViewModel()

    }

    override fun rootView(): View = binding.root

    private fun observeViewModel() {
        activityViewModel.viewState.observe(viewLifecycleOwner) {
            populateProduct(it.chosenProduct)
        }
        viewModel.viewState.observe(viewLifecycleOwner) {
            handleDiscountTypeChange(it.discountType)
        }
    }

    private fun populateProduct(chosenProduct : Product?) {
        chosenProduct ?: return
        val currentViewState = viewModel.getCurrentViewStateOrNew()
        with(binding) {
            val isRadioButtonGrupIsEmpty =
                !radioNothing.isChecked && !radioNominal.isChecked && !radioPercentage.isChecked
            if (isRadioButtonGrupIsEmpty) {
                when(chosenProduct.discountType) {
                    DiscountType.Nominal -> {
                        val nominalValue = currentViewState.nominalValue
                        etNominal.setText(
                            if (nominalValue == 0.0) chosenProduct.discountValue.toLong().toString()
                        else nominalValue.toLong().toString()
                        )
                    }
                    DiscountType.Percentage -> {
                        val percentageValue = currentViewState.percentageValue
                        etPercentage.setText(if (percentageValue == 0.0) chosenProduct.discountValue.toLong().toString() else percentageValue.toLong().toString())
                    }
                    else -> {

                    }
                }
                viewModel.setCurrentDiscountType(chosenProduct.discountType)
            }
        }
    }

    private fun handleDiscountTypeChange(discountType: DiscountType) {
        with(binding) {
            when (discountType) {
                DiscountType.Nominal -> {
                    radioNominal.isChecked = true
                    etNominal.goVisible()
                    etNominal.setSelection(etNominal.text?.length ?: 0)
                }
                DiscountType.Percentage -> {
                    radioPercentage.isChecked = true
                    etNominal.goGone()
                    etPercentage.setSelection(etPercentage.text.length)
                }
                DiscountType.Nothing -> {
                    radioNothing.isChecked = true
                }
            }
        }
    }

    private fun initView() {
        initListener()
        initCurrencyEditText()
        initEditTextListener()
        setRadioButtonListener()
    }

    private fun initEditTextListener() {
        binding.etNominal.textChanged {
            val nominalValue = binding.etNominal.rawValue.toDouble()
            nominalValidator(nominalValue)
            viewModel.onNominalValueChanged(nominalValue)
        }
        binding.etPercentage.textChanged {
            val percentageValue = it.toString().toDoubleOrNull() ?: 0.0
            percentageValidator(percentageValue)
            viewModel.onPercentageValueChanged(percentageValue)
        }
    }

    private fun initCurrencyEditText() {
        with(binding.etNominal) {
            locale = Locale("id", "ID")
            decimalDigits = 0
        }
    }

    private fun initListener() {
        with(binding) {
            btBack.setOnClickListener {
                dismiss()
            }
            btnSubmit.setOnClickListener {
                dismiss()
                val viewState = viewModel.getCurrentViewStateOrNew()
                mListener.onSaveChangeDiscount(
                    viewState.discountType,
                    viewState.getChoosenTypeValue()
                )
            }
            dialogParentDiskon.setOnClickListener {
                dismiss()
            }
        }
    }
    private fun setRadioButtonListener() {
        val viewState = viewModel.getCurrentViewStateOrNew()
        with(binding) {
            radioNominal.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Nominal)
                nominalValidator(viewState.nominalValue)
            }
            radioPercentage.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Percentage)
                percentageValidator(viewState.percentageValue)
            }
            radioNothing.setOnClickListener {
                viewModel.setCurrentDiscountType(DiscountType.Nothing)
                clearError()
            }
        }
    }

    private fun percentageValidator(percentageValue: Double){
        val validation = CommonStringValidator
            .blankValidator(percentageValue.toString(), "Persentase diskon tidak boleh kosong")
            .then(
                DoubleValidator
                    .maximumThenValidator(
                        percentageValue,
                        100.0,
                        "Diskon tidak boleh lebih dari 100%"
                    )
                    .then(
                        DoubleValidator
                            .minimumThenValidator(percentageValue, 0.0, "Diskon tidak boleh 0%")
                    )
            )

        when (validation) {
            is InputValidation.Success -> clearError()

            is InputValidation.Error -> setError(validation.t)
        }
    }
    private fun nominalValidator(nominalValue : Double){
        val product = activityViewModel.getCurrentViewStateOrNew().chosenProduct ?: return
        val validation = CommonStringValidator
            .blankValidator(nominalValue.toString(), "Persentase diskon tidak boleh kosong")
            .then(
                DoubleValidator
                    .maximumThenValidator(
                        nominalValue,
                        product.sellPrice,
                        "Diskon tidak boleh lebih dari harga jual yaitu ${product.sellPrice.toMoneyFormat(
                            Constant.PREFIX_RP)}"
                    )
                    .then(
                        DoubleValidator
                            .minimumThenValidator(nominalValue, 0.0, "Diskon tidak boleh Rp. 0")
                    )
            )

        when (validation) {
            is InputValidation.Success<*> -> clearError()

            is InputValidation.Error<*> -> setError(validation.t.toString())

        }
    }
    private fun setError(message: String) {
        with(binding){
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnSubmit.isEnabled = false
            btnSubmit.setStrokeColorResource(R.color.disable_btn)
        }
    }
    private fun clearError() {
        with(binding){
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnSubmit.isEnabled = true
            btnSubmit.setStrokeColorResource(R.color.blue_500)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    interface OnChangeDiscountListener {
        fun onSaveChangeDiscount(discountType: DiscountType, discountValue: Double = 0.0)
    }
}