package com.soldig.kawanmart.presentation.common.productlist

import com.soldig.kawanmart.presentation.features.home.HomeViewState


sealed class ProductListViewType{

    companion object {
        fun produce32EmptyProduct(): ArrayList<ProductListViewType> {
            val newList = ArrayList<ProductListViewType>()
            for(i in 1..32){
                newList.add(GridEmpty)
            }
            return newList
        }
    }

    object GridEmpty : ProductListViewType()

    data class Grid(var product: HomeViewState.ProductSection.ProductListItem.POSProductListItem) : ProductListViewType()

    fun areContentTheSame(productListViewType: ProductListViewType) : Boolean {
        return when(productListViewType){
            is GridEmpty ->{
                this is GridEmpty
            }

            is Grid -> {
                if(this is Grid){
                    return false
                }else{
                    false
                }

            }

        }
    }

    fun areItemSame(productListViewType: ProductListViewType) : Boolean{
        return when(productListViewType){
            is GridEmpty ->{
                this is GridEmpty
            }

            is Grid -> {
                if(this is Grid){
                    this.product.posProduct.id  == productListViewType.product.posProduct.id
                }else{
                    false
                }

            }

        }
    }

}