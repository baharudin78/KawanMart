package com.soldig.kawanmart.presentation.features.search

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isEmpty
import androidx.core.widget.addTextChangedListener
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseLoggedInActivity
import com.soldig.kawanmart.databinding.ActivitySearchBinding
import com.soldig.kawanmart.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : BaseLoggedInActivity() {

    private lateinit var binding : ActivitySearchBinding
    private val viewModel : SearchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initView()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(this) {
            with(binding) {
                if (rvListProduct.isEmpty()) {
                    tvEmptyList.visibility = View.VISIBLE
                    tvEmptyList.text = "Produk dengan kata kunci ${it.nameKey} tidak ditemukan"
                }
                tvEmptyList.visibility = View.VISIBLE
                progressBar.toggleVisibility(it.isFetching)
                rvListProduct.toggleVisibility(!it.isFetching && it.productList.isNotEmpty())

                tvBackToSearchResult.text = "Kembali ke hasil pencarian ${it.lastNotEmptySearchKey}"
                btnSearch.isEnabled = !it.isFetching
                btnSearch.setStrokeColorResource(if (it.isFetching) R.color.disable_btn else R.color.blue_500)
                tvEmptyList.toggleVisibility(!it.isFetching && it.isEmpty)
                println("Adalah ${!it.isFetching}  ${!it.isEmpty}")
                tvBackToSearchResult.toggleVisibility(!it.isFetching && it.productList.isNotEmpty())

            }
        }
    }

    private fun initView() {
        initToolbar("Cari Barang", binding.toolbarKawanMart)
        initTime(toolbar = binding.toolbarKawanMart)
        initEditTextListener()
        initClickListener()
    }

    private fun initClickListener() {
        binding.btnSearch.setOnClickListener {
            val intent = Intent().apply {
                putExtra(SearchContract.CONTRACT_RETURN_KEY, viewModel.getCurrentViewStateOrNew().nameKey)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        binding.tvBackToSearchResult.setOnClickListener {
            viewModel.setCurrentMode(SearchViewState.Mode.List)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initEditTextListener() {
        with(binding.etSearch) {
            addTextChangedListener {
                viewModel.setSearchKey(it.toString())
            }
            setOnTouchListener { _ , _ ->
                viewModel.setCurrentMode(SearchViewState.Mode.List)
                false
            }
        }
    }
}