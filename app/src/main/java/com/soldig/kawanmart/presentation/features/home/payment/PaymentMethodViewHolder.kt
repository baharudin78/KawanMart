package com.soldig.kawanmart.presentation.features.home.payment

import android.content.res.ColorStateList
import android.view.View
import androidx.core.content.ContextCompat
import com.soldig.kawanmart.model.PaymentMethod
import com.soldig.kawanmart.R
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.ItemPaymentMethodBinding
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PaymentMethodViewHolder(view : View) : RecyclerViewHolder<PaymentMethod>(view) {

    private val binding = ItemPaymentMethodBinding.bind(view)

    fun bind(paymentMethod: PaymentMethod, isClicked : Boolean, onPaymentClickListener : (paymentMethod : PaymentMethod, index : Int) -> Unit) {
        with(binding.btnPaymentType) {
            text = paymentMethod.name.toDoubleOrNull()?.toMoneyFormat() ?: paymentMethod.name
            setOnClickListener {
                if (isClicked) {
                    onPaymentClickListener(paymentMethod, -1)
                }else {
                    onPaymentClickListener(paymentMethod, adapterPosition)
                }
            }
            val context = binding.root.context
            backgroundTintList =  ColorStateList.valueOf(ContextCompat.getColor(context, if(isClicked) R.color.payment_on_click_bakcground else R.color.white))
            setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, if(isClicked) R.color.payment_on_click_text else R.color.black)))
            setStrokeColorResource(if(isClicked) R.color.payment_on_click_bakcground else R.color.blue_500)
        }
    }
}