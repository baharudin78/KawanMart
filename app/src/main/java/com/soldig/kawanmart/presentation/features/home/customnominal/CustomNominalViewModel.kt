package com.soldig.kawanmart.presentation.features.home.customnominal

import com.soldig.kawanmart.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CustomNominalViewModel @Inject constructor(

) : BaseViewModel<CustomNominalViewState>(){

    override fun initNewViewState(): CustomNominalViewState {
        return CustomNominalViewState()
    }

    fun setMinumumPrice(newMinimumPrice : Double) {
        setViewState(getCurrentViewStateOrNew().apply {
            minimumPrice = newMinimumPrice
        })
    }

    fun setCurrentPrice(currentPrice : Double) {
        setViewState(getCurrentViewStateOrNew().apply {
            inputtedPrice = currentPrice
        })
    }
}