package com.soldig.kawanmart.presentation.features.input_saldo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class InputSaldoViewState(
    var inputSaldoValue : Double = 0.0,
    var inputSaldoIsDone : Boolean = false,
    var isFetching : Boolean = false
): Parcelable