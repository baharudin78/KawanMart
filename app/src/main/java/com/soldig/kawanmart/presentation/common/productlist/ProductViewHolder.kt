package com.soldig.kawanmart.presentation.common.productlist

import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.R
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.ItemGridProdukListBinding
import com.soldig.kawanmart.databinding.ItemProductListBinding
import com.soldig.kawanmart.presentation.features.home.HomeViewState
import com.soldig.kawanmart.utils.Constant
import com.soldig.kawanmart.utils.ext.goGone
import com.soldig.kawanmart.utils.ext.toggleVisibility
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class ProductGridViewHolder(view: View) : RecyclerViewHolder<ProductListViewType.Grid>(view) {

    private val itemBinding: ItemGridProdukListBinding = ItemGridProdukListBinding.bind(view)

    fun bind(productListItem: HomeViewState.ProductSection.ProductListItem.POSProductListItem, onProductClicked: (product:  HomeViewState.ProductSection.ProductListItem.POSProductListItem) -> Unit) {
        val product = productListItem.posProduct
        itemBinding.tvTitle.text = product.title
        itemBinding.tvPrice.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        Glide.with(itemView.context)
            .load(product.getImageURL())
            .into(itemBinding.imgProduct)
        itemBinding.layerEmpty.toggleVisibility(product.qty <= 0 || !product.isActive)
        if(!product.isActive){
            itemBinding.tvKeterangan.text = "Tidak Aktif"
        }else{
            itemBinding.tvKeterangan.text = "Habis"
        }
        itemView.setOnClickListener {
            if (product.qty == 0 || !product.isActive) {
                return@setOnClickListener
            }
            onProductClicked(productListItem)
        }
    }

}


class ProductListViewHolder(view: View) : RecyclerViewHolder<ProductListHorizontalItemType>(view) {

    val itemBinding: ItemProductListBinding = ItemProductListBinding.bind(view)

    @RequiresApi(Build.VERSION_CODES.M)
    fun bind(productListItem: HomeViewState.ProductSection.ProductListItem, onCartClicked: (product: HomeViewState.ProductSection.ProductListItem.POSProductListItem)  -> Unit, onProductClicked: (product: HomeViewState.ProductSection.ProductListItem) -> Unit,) {
        val product = productListItem.product
        itemBinding.tvTitle.text = product.title
        itemBinding.tvPriceExist.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        itemBinding.tvPriceNotExist.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        itemBinding.tvInvisiblePrice.text = product.sellAfterDiscountPrice.toMoneyFormat(Constant.PREFIX_RP)
        Glide.with(itemView.context)
            .load(product.getImageURL())
            .into(itemBinding.imgProduct)
        itemBinding.layerEmpty.goGone()
        itemBinding.tvPriceNotExist.toggleVisibility(product is Product.NotPOSProduct)
        itemBinding.tvPriceExist.toggleVisibility(product is Product.POSProduct)
        itemBinding.icCart.toggleVisibility(product is Product.POSProduct && product.isActive)
        with(itemBinding.icCart){
            imageTintList = ColorStateList.valueOf(itemBinding.root.context.getColor(if(product.qty > 0) R.color.blue_500 else R.color.disable_btn))
        }
        itemBinding.icCart.setOnClickListener {
            if(productListItem is HomeViewState.ProductSection.ProductListItem.POSProductListItem && product.qty > 0){
                onCartClicked(productListItem)
            }
        }
        itemView.setOnClickListener {
            onProductClicked(productListItem)
        }
    }

}


data class ProductListHorizontalItemType(val productListItem: HomeViewState.ProductSection.ProductListItem)

class ProductGridEmptyViewHolder(view : View) : RecyclerViewHolder<ProductListViewType.GridEmpty>(view) {

}
