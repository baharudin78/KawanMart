package com.soldig.kawanmart.presentation.features.home

import android.view.View
import androidx.core.content.ContextCompat
import com.soldig.kawanmart.model.Order
import com.soldig.kawanmart.R
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.ItemPesananHomeListBinding
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class OrderViewHolder(view : View) : RecyclerViewHolder<Order>(view) {

    val binding = ItemPesananHomeListBinding.bind(view)
    fun bind(order: Order,
             onAdd : (product: HomeViewState.ProductSection.ProductListItem.POSProductListItem) -> Unit,
             onDelete : (product: HomeViewState.ProductSection.ProductListItem.POSProductListItem) -> Unit) {
        with(order) {
            val product = order.product.posProduct
            binding.tvName.text = product.title
            binding.tvQty.text = quantity.toString()
            binding.tvValue.text = currentTotalPrice.toMoneyFormat()
            binding.btnPlus.let {
                it.setOnClickListener {
                    if (product.qty > 0 ) {
                        onAdd(order.product)
                    }
                }
                it.background = ContextCompat.getDrawable(
                    binding.root.context, if (order.product.posProduct.qty == 0)
                        R.drawable.bg_circle_overlay else R.drawable.ic_plus
                )
            }
            binding.btnMinus.let {
                it.setOnClickListener {
                    onDelete(order.product)
                }
            }
        }
    }
}