package com.soldig.kawanmart.presentation.features.search

import android.os.Parcelable
import com.soldig.kawanmart.presentation.common.productlist.ProductListHorizontalItemType
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchViewState(
    var isFetching: Boolean,
    var mode: Mode = Mode.List,
    var nameKey: String,
    var isEmpty : Boolean,
    var lastNotEmptySearchKey: String = ""

): Parcelable {
    enum class Mode {
        List
    }
    @IgnoredOnParcel
    var productList : List<ProductListHorizontalItemType> = listOf()
}