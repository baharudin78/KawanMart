package com.soldig.kawanmart.presentation.features.category

import android.os.Parcelable
import com.soldig.kawanmart.model.ProductCategory
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryViewState(
    var isFetching : Boolean
): Parcelable {
    @IgnoredOnParcel
    var categoryList : List<ProductCategory> = listOf()
}