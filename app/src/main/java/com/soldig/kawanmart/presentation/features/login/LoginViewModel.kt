package com.soldig.kawanmart.presentation.features.login

import androidx.lifecycle.viewModelScope
import com.soldig.kawanmart.interactor.login.LoginInteractor
import com.soldig.kawanmart.base.BaseViewModel
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.User
import com.soldig.kawanmart.utils.other.SingleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginInteractor : LoginInteractor,
) : BaseViewModel<LoginViewState>(){

    companion object {
        const val VIEW_STATE_STATE_KEY = "com.soldig.amanahmart.presentation.feature.login.LoginViewModel.statekey"
    }

    override fun initNewViewState(): LoginViewState {
        return LoginViewState()
    }
    fun setUser(user : User) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.user = user
        })
    }

    private fun setPOS(pos : POS) {
        setViewState(getCurrentViewStateOrNew().apply {
            println("New Token : $token")
            this.token = SingleEvent(pos)
        })
    }

    private fun setLoading(isLoading : Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.isLoading = isLoading
        })
    }

    fun login(user: User){
        viewModelScope.launch{
            withContext(IO){
                loginInteractor.userLogin.login(user).collect {
                    onCollect(it,
                        onLoading = { isLoading ->
                            setLoading(isLoading)
                        }
                    ) { resource ->
                        setLoading(false)
                        setPOS(resource.data)
                    }
                }

            }
        }
    }


}