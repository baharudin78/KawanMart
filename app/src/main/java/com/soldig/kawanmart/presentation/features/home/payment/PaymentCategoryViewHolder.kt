package com.soldig.kawanmart.presentation.features.home.payment

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.soldig.kawanmart.model.PaymentMethod
import com.soldig.kawanmart.R
import com.soldig.kawanmart.databinding.ItemPaymentCategoryBinding
import me.ibrahimyilmaz.kiel.adapterOf
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PaymentCategoryViewHolder(view : View) : RecyclerViewHolder<PaymentCategoryListItem>(view) {

    private val binding = ItemPaymentCategoryBinding.bind(view)

    fun bind(payment: PaymentCategoryListItem, onPaymentMethodClicked: ( paymentMethodIndex: Int, paymentCategoryIndex: Int) -> Unit){
        with(binding){
            title.text = payment.paymentCategory.name
            val adapter = adapterOf<PaymentMethod> {
                diff(
                    areItemsTheSame = { old: PaymentMethod, new: PaymentMethod ->
                        old.id == new.id
                    },
                    areContentsTheSame = { old: PaymentMethod, new: PaymentMethod ->
                        false
                    }
                )
                register(
                    viewHolder = ::PaymentMethodViewHolder,
                    layoutResource = R.layout.item_payment_method,
                    onBindViewHolder = {vh, _ , item ->
                        vh.bind(item,payment.clickedItem == vh.adapterPosition && payment.isSelected){ it , index ->
                            onPaymentMethodClicked(index,if(index == -1) -1 else adapterPosition)
                        }
                    }
                )
            }
            rvPaymentMethods.apply {
                this.adapter = adapter
                val flexLayoutManager = FlexboxLayoutManager(binding.root.context).apply {
                    flexDirection = FlexDirection.ROW
                }
                itemAnimator?.changeDuration = 0
                layoutManager = flexLayoutManager
                setRecycledViewPool(RecyclerView.RecycledViewPool())
            }
            adapter.submitList(payment.paymentCategory.paymentMethods)
        }
    }


}