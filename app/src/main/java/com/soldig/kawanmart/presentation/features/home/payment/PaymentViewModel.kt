package com.soldig.kawanmart.presentation.features.home.payment

import androidx.lifecycle.viewModelScope
import com.soldig.kawanmart.interactor.home.HomeInteractor
import com.soldig.kawanmart.model.PaymentCategory
import com.soldig.kawanmart.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val homeInteractor: HomeInteractor
) : BaseViewModel<PaymentViewState>(){

    override fun initNewViewState(): PaymentViewState {
        return PaymentViewState()
    }
    private fun setLoading(isloading : Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            isFetching = isloading
        })
    }
    private fun setPaymentList(paymentList : List<PaymentCategory>){
        viewModelScope.launch(IO){
            val paymentItemList = paymentList.map {
                PaymentCategoryListItem(it)
            }
            setViewState(getCurrentViewStateOrNew().apply {
                this.paymentList = ArrayList(paymentItemList)
            })
        }
    }

    fun getAllPayment(token : String) {
        viewModelScope.launch(IO) {
            homeInteractor.getPaymentMethods.fetch(token).collect {
                onCollect(
                    response = it,
                    onLoading = { isLoading ->
                        setLoading(isLoading)
                    },
                    executeOnSuccess = { successResult ->
                        setPaymentList(successResult.data)
                    }
                )
            }
        }
    }

    fun newSelectedMethod(paymentCategoryIndex: Int,paymentMethodIndex:Int){
        setViewState(getCurrentViewStateOrNew().apply {
            val oldSelected = selectedPaymentCategoryIndex
            selectedPaymentCategoryIndex = paymentCategoryIndex
            if(oldSelected >= 0){
                paymentList[oldSelected] = paymentList[oldSelected].apply {
                    clickedItem = -1
                    isSelected = false
                }
            }
            if(selectedPaymentCategoryIndex >= 0  && paymentMethodIndex >= 0) {
                paymentList[selectedPaymentCategoryIndex] = paymentList[selectedPaymentCategoryIndex].apply {
                    clickedItem = paymentMethodIndex
                    isSelected = true
                }
                currentSelectedMethod = paymentList[paymentCategoryIndex].paymentCategory.paymentMethods[paymentMethodIndex]
                paymentType = paymentList[paymentCategoryIndex].paymentCategory.paymentType
            }else{
                currentSelectedMethod = null
            }

        })
    }

    fun newMinimumPrice(minimumPrice: Double){
        setViewState(getCurrentViewStateOrNew().apply {
            this.minimumPrice = minimumPrice
        })
    }
}