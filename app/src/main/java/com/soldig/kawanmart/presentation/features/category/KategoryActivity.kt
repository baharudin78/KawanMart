package com.soldig.kawanmart.presentation.features.category

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseLoggedInActivity
import com.soldig.kawanmart.databinding.ActivityKategoryBinding
import com.soldig.kawanmart.presentation.features.category.adapter.CategoryAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class KategoryActivity : BaseLoggedInActivity() {

    private lateinit var binding : ActivityKategoryBinding
    private val viewModel : CategoryViewModel by viewModels()

    companion object {
        const val CATEGORY_CONTRACT_KEY = "com.soldig.amanahmart.presentation.feature.category.categorylist.CATEGORY_CONTRACT_KEY"
    }

    private val categoryAdapter = CategoryAdapter(listOf()) {
        val intent = Intent().apply {
            putExtra(CATEGORY_CONTRACT_KEY, it)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKategoryBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initView()
        observeViewModel()
        viewModel.getAllCategory(currentPOS.token)
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(this) {
            categoryAdapter.changeList(it.categoryList)
        }
    }

    private fun initView() {
        initToolbar("Pilih Kategori", binding.toolbarKawanMart)
        initTime(toolbar = binding.toolbarKawanMart)
        binding.rvListCategory.apply {
            adapter = categoryAdapter
        }
    }
}