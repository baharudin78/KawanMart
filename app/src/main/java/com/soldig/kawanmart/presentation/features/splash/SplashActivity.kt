package com.soldig.kawanmart.presentation.features.splash

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.soldig.kawanmart.presentation.features.login.LoginActivity
import com.soldig.kawanmart.base.BaseActivity
import com.soldig.kawanmart.databinding.ActivitySplashBinding
import com.soldig.kawanmart.presentation.HomeActivity
import com.soldig.kawanmart.utils.ext.getUserInSharedPref
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class SplashActivity : BaseActivity() {

    private lateinit var binding : ActivitySplashBinding
    private val viewModel by viewModels<SplashScreenViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        observeViewModel()
        checkIfIsLoggedIn()

    }
    private fun observeViewModel() {
        viewModel.isSyncDataDone.observe(this) {
            if (it) startActivity(Intent(this, HomeActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
            })
        }
    }
    private fun checkIfIsLoggedIn() {
        lifecycleScope.launch(IO) {
            delay(5000)
            withContext(Main) {
                getUserInSharedPref()?.let {
                    startDataSyncText()
                    viewModel.checkSync(it)
                } ?: startActivity(Intent(this@SplashActivity, LoginActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    finish()
                })
            }
        }
    }
    private fun startDataSyncText() {
        lifecycleScope.launch (IO){
            var toBePrintedText = "Loading"
            var index = 0
            delay(5000)
            while (true) {
                if (index == 5) {
                    toBePrintedText = "Loading"
                    index = 0
                }else {
                    toBePrintedText +=" ."
                    index++
                }
                withContext(Main) {
                    binding.tvLoading.text = ""
                }
                delay(250)
            }
        }
    }
}