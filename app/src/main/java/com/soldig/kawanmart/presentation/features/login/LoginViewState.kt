package com.soldig.kawanmart.presentation.features.login

import android.os.Parcelable
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.User
import com.soldig.kawanmart.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class LoginViewState(
    var user : User = User("", ""),
    private var _pos : POS? = null,
    var isLoading : Boolean = false,
) : Parcelable {
    @IgnoredOnParcel
    var token : SingleEvent<POS>? = null
    set(value) {
        println("New Token $value")
        _pos = value?.peek()
        field = value
    }
}