package com.soldig.kawanmart.presentation.features.produkdetail.changediskon

import android.os.Parcelable
import com.soldig.kawanmart.model.DiscountType
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChangeDiskonViewState(
    var discountType: DiscountType = DiscountType.Nothing,
    var percentageValue: Double = 0.0,
    var nominalValue: Double = 0.0,
) : Parcelable {
    fun getChoosenTypeValue() : Double{
        return when(discountType){
            DiscountType.Nominal -> nominalValue
            DiscountType.Nothing -> 0.0
            DiscountType.Percentage -> percentageValue
        }
    }
}