package com.soldig.kawanmart.presentation.features.home.payment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.soldig.kawanmart.model.PaymentCategoryType
import com.soldig.kawanmart.model.PaymentMethod
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseDialogFragment
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.FragmentPaymentMethodBinding
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.presentation.HomeActivity
import com.soldig.kawanmart.utils.ext.goGone
import com.soldig.kawanmart.utils.ext.goVisible
import com.soldig.kawanmart.utils.ext.toggleVisibility
import com.soldig.kawanmart.utils.validator.CommonStringValidator
import com.soldig.kawanmart.utils.validator.DoubleValidator
import com.soldig.kawanmart.utils.validator.InputValidation
import com.soldig.kawanmart.utils.validator.then
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf


@AndroidEntryPoint
class PaymentMethodFragment : BaseDialogFragment(){

    private var _binding : FragmentPaymentMethodBinding ? = null
    private val binding get() = _binding!!


    private val viewModel : PaymentViewModel by viewModels()
    private lateinit var mListener: OnPaymentTypeListener
    private var totalTransactionPrice : Double = 0.0

    private val  pos : POS by lazy {
        val activity = requireActivity()
        if(activity is HomeActivity){
            activity.currentPOS
        }else{
            throw Exception("Make Sure activity intialize current pos")
        }
    }

    private val paymentAdapter = adapterOf<PaymentCategoryListItem> {
        diff(
            areItemsTheSame = { old: PaymentCategoryListItem, new: PaymentCategoryListItem ->
                old.paymentCategory.id == new.paymentCategory.id
            },
            areContentsTheSame = { old: PaymentCategoryListItem, new: PaymentCategoryListItem ->
                false
            }
        )

        register(
            viewHolder = ::PaymentCategoryViewHolder,
            layoutResource = R.layout.item_payment_category,
            onBindViewHolder = { vh, _, item ->
                vh.bind(item){  paymentMethodIndex: Int, paymentCategoryIndex: Int->
                    onItemSelected(
                        categoryIndex = paymentCategoryIndex,
                        methodIndex = paymentMethodIndex
                    )
                }
            }
        )
    }

    fun setTotalTransactionPrice(newTotalTransactionPrice: Double){
        totalTransactionPrice = newTotalTransactionPrice
    }

    fun setListener(listener: OnPaymentTypeListener) {
        mListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentPaymentMethodBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeViewModel()
        viewModel.getAllPayment(pos.token)
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner){
            with(it){
                binding.loading.toggleVisibility(isFetching)
                paymentAdapter.submitList(ArrayList(paymentList))
                binding.tvTransactionPrice.text = "${getString(R.string.payment_type_transaction_price_prefix)} ${totalTransactionPrice.toMoneyFormat()}"
                validateChoosenPayment(currentSelectedMethod, paymentType, totalTransactionPrice)
                binding.btnPay.setOnClickListener {
                    onBtnPayClicked(currentSelectedMethod, paymentType, totalTransactionPrice)
                }
            }
        }
    }

    private fun validateChoosenPayment(
        currentSelectedMethod: PaymentMethod?,
        paymentType: PaymentCategoryType?,
        minimumPrice: Double
    ) {
        if(currentSelectedMethod != null && paymentType != null){
            when(paymentType){
                PaymentCategoryType.CASH -> {
                    val selectedPrice = currentSelectedMethod.name.toDoubleOrNull()
                    if(selectedPrice == null){
                        clearError()
                    }else{
                        priceValidator(selectedPrice, minimumPrice)
                    }
                }
                PaymentCategoryType.NOT_CASH -> {
                    clearError()
                }
            }
        }else{
            setError("Pilih metode pembayaran yang diinginkan")
        }
    }


    private fun onBtnPayClicked(currentSelectedMethod: PaymentMethod?, paymentType: PaymentCategoryType?, minimumPrice: Double) {
        if(currentSelectedMethod != null && paymentType != null){
            mListener.onPay(currentSelectedMethod, paymentType, minimumPrice )
            dismiss()
        }
    }

    private fun priceValidator(nominalValue: Double, minimumPrice: Double) {
        val validation = CommonStringValidator
            .blankValidator(nominalValue.toString(), "Jumlah nominal tidak boleh kosong")
            .then(
                DoubleValidator
                    .minimumThenValidator(nominalValue, minimumPrice, "Nominal harus sama atau lebih dari jumlah pembelian")

            )
        when (validation) {
            is InputValidation.Success -> clearError()

            is InputValidation.Error -> setError(validation.t)

        }
    }


    private fun setError(message: String) {
        with(binding){
            tvErrorText.goVisible()
            tvErrorText.text = message
            btnPay.isEnabled = false
            btnPay.setStrokeColorResource(R.color.disable_btn)
        }
    }

    private fun clearError() {
        with(binding) {
            tvErrorText.goGone()
            tvErrorText.text = ""
            btnPay.isEnabled = true
            btnPay.setStrokeColorResource(R.color.blue_500)
        }
    }

    override fun rootView(): View {
        return binding.root
    }

    private fun initView() {
        initClickListener()
        initRcycleView()
    }

    private fun initRcycleView() {
        binding.rvPaymentCategories.apply {
            adapter = paymentAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initClickListener() {
        binding.btBatalBayar.setOnClickListener {
            dismiss()
        }
    }

    private fun onItemSelected(categoryIndex: Int, methodIndex: Int){
        viewModel.newSelectedMethod(paymentCategoryIndex = categoryIndex, paymentMethodIndex =  methodIndex)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    interface OnPaymentTypeListener {
        fun onPay(paymentMethod: PaymentMethod, paymentCategoryType: PaymentCategoryType, minimumPrice: Double)
    }
}