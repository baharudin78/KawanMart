package com.soldig.kawanmart.presentation.features.input_saldo

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.soldig.kawanmart.R
import com.soldig.kawanmart.base.BaseActivity
import com.soldig.kawanmart.databinding.ActivityInputSaldoBinding
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.presentation.HomeActivity
import com.soldig.kawanmart.utils.ext.*
import com.soldig.kawanmart.utils.other.UIInteraction
import com.soldig.kawanmart.utils.validator.CommonStringValidator
import com.soldig.kawanmart.utils.validator.InputValidation
import com.soldig.kawanmart.utils.validator.ValidationCallback
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@AndroidEntryPoint
class InputSaldoActivity : BaseActivity() {

    companion object {
        const val POS_KEY = "com.soldig.amanahmart.presentation.feature.topup.TopUpActivity_POS_KEY"
        const val PASS_KEY =  "com.soldig.amanahmart.presentation.feature.topup.TopUpActivity_PASS_KEY"
    }
    private lateinit var binding : ActivityInputSaldoBinding
    private val viewModel by viewModels<InputSaldoViewModel>()
    private val currentPOS : POS by lazy {
        (intent.extras?.getParcelable(POS_KEY) as POS?) ?: throw Exception("No Provided")
    }

    private val currentPosPassword : String by lazy {
        (intent.extras?.getString(PASS_KEY)) ?: throw Exception("No Provider")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInputSaldoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initTime(true, binding.toolbarKawanMart)
        initView()
        obseverViewModel()


    }

    private fun obseverViewModel() {
        viewModel.viewState.observe(this) {viewState ->
            with(binding) {
                println("is loading ? ${viewState.isFetching}")
                loading.root.toggleVisibility(viewState.isFetching)
                if (viewState.inputSaldoIsDone) {
                    setUser(currentPOS)
                    navigateToMain()
                }
            }
        }
        viewModel.userInteraction.observe(this) {
            val uiInteraction = it.get() ?: return@observe
            if (uiInteraction !is UIInteraction.DoNothing) {
                binding.root.showShortSnackbar(uiInteraction.getMessage(this))
            }
        }
    }

    private fun initView() {
        initEditTextValidator()
        initListener()
    }

    private fun initEditTextValidator() {
        binding.saldoEditText.setValidator(
            object : ValidationCallback<String, String> {
                override fun validator(input: String): InputValidation<String> {
                    return CommonStringValidator.blankValidator(input, getString(R.string.input_topup_blank))
                }

                override fun onValidationDone(input: String, isValid: Boolean) {
                    viewModel.setBalanceValue(input.toDoubleOrNull() ?: 0.0)
                    if (isValid) binding.btnMasukSaldo.enableButton() else  binding.btnMasukSaldo.disableButton()
                }
            },
            binding.saldoInputLayout
        )
        binding.btnMasukSaldo.disableButton()
    }

    private fun initListener() {
        with(binding) {
            btnMasukSaldo.setOnClickListener {
                saldoEditText.text.toString().toDoubleOrNull()?.let {
                    lifecycleScope.launch(Dispatchers.IO) {
                        delay(500)
                        withContext(Dispatchers.Main) {
                            viewModel.procesingInputSaldo(currentPOS, it, currentPOS.token, currentPosPassword)
                        }
                    }
                }
            }
            btnMasukSaldo.goGone()
            btnMasukSaldo.goVisible()
        }
    }
    private fun navigateToMain() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}