package com.soldig.kawanmart.presentation.common.reportlist

import android.annotation.SuppressLint
import android.view.View
import com.soldig.kawanmart.model.ProductReport
import com.soldig.kawanmart.databinding.ItemReportProdukBinding
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class ReportViewHolder(view : View) : RecyclerViewHolder<ProductReport>(view) {

    private val binding = ItemReportProdukBinding.bind(view);


    @SuppressLint("SetTextI18n")
    fun bind(productReport: ProductReport) {
        with(binding){
            tvName.text = productReport.name
            tvQty.text = ": ${productReport.qty}"
        }
    }

}