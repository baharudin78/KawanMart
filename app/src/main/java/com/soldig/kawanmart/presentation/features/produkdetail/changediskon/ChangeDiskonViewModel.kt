package com.soldig.kawanmart.presentation.features.produkdetail.changediskon

import com.soldig.kawanmart.base.BaseViewModel
import com.soldig.kawanmart.model.DiscountType
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChangeDiskonViewModel @Inject constructor(

) : BaseViewModel<ChangeDiskonViewState>(){

    override fun initNewViewState(): ChangeDiskonViewState {
        return ChangeDiskonViewState()
    }

    fun onNominalValueChanged(newNominalValue : Double) {
        setViewState(getCurrentViewStateOrNew().apply {
            nominalValue = newNominalValue
        })
    }

    fun onPercentageValueChanged(newPercentageValue : Double) {
        setViewState(getCurrentViewStateOrNew().apply {
            percentageValue = newPercentageValue
        })
    }

    fun setCurrentDiscountType(discountType: DiscountType) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.discountType = discountType
        })
    }
}