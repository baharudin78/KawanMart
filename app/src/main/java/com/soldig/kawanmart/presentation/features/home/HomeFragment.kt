package com.soldig.kawanmart.presentation.features.home

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.soldig.kawanmart.model.Order
import com.soldig.kawanmart.model.ParcelableProduct
import com.soldig.kawanmart.model.PaymentCategoryType
import com.soldig.kawanmart.model.PaymentMethod
import com.soldig.ayogrosir.utils.SpacesItemDecoration
import com.soldig.kawanmart.R
import com.soldig.kawanmart.common.dp
import com.soldig.kawanmart.databinding.FragmentHomeBinding
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.presentation.HomeActivity
import com.soldig.kawanmart.presentation.common.productlist.*
import com.soldig.kawanmart.presentation.features.category.CategoryContract
import com.soldig.kawanmart.presentation.features.home.customnominal.CustomNominalFragment
import com.soldig.kawanmart.presentation.features.home.payment.PaymentMethodFragment
import com.soldig.kawanmart.presentation.features.produkdetail.ProdukDetailActivity
import com.soldig.kawanmart.presentation.features.search.SearchContract
import com.soldig.kawanmart.utils.ext.goGone
import com.soldig.kawanmart.utils.ext.goVisible
import com.soldig.kawanmart.utils.ext.launchConfirmationDialog
import com.soldig.kawanmart.utils.ext.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf

@AndroidEntryPoint
class HomeFragment : Fragment(),
        PaymentMethodFragment.OnPaymentTypeListener,
        CustomNominalFragment.OnCustomNominalListener {

    private val gridLayoutManager by lazy { GridLayoutManager(requireContext(), 2) }
    private var _binding: FragmentHomeBinding? = null
    private val viewModel: HomeViewModel by viewModels()
    private val startCategoryList = registerForActivityResult(CategoryContract()) {
        it ?: return@registerForActivityResult
        viewModel.filterProductByCategory(it)
    }

    private lateinit var productListLayoutManager: LinearLayoutManager
    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if (productListLayoutManager.findLastCompletelyVisibleItemPosition() > 10 && viewModel
                    .getCurrentViewStateOrNew()
                    .productSection
                    .currentProductViewType == HomeViewState
                    .ProductSection.ProductViewType.Kumpulan
            ) {
                binding.btnScrollToTop.goVisible()
            } else {
                binding.btnScrollToTop.goGone()
            }
        }
    }

    private val searchActivityDetail = registerForActivityResult(SearchContract()) {
        it ?: return@registerForActivityResult
        if (viewModel.getCurrentViewStateOrNew().productSection.currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan) {
            viewModel.filterProductKumpulanByName(it)
        } else {
            viewModel.filterProductGridByName(it)
        }
    }

    private val binding get() = _binding!!
    private val pos: POS by lazy {
        val activity = requireActivity()
        if (activity is HomeActivity) {
            activity.currentPOS
        } else {
            throw Exception("")
        }
    }

    private val gridAdapter = adapterOf<ProductListViewType> {
        diff(
            areItemsTheSame = { old: ProductListViewType, new: ProductListViewType ->
                old.areItemSame(
                    new
                )
            },
            areContentsTheSame = { old: ProductListViewType, new: ProductListViewType ->
                false
            }
        )
        register(
            viewHolder = ::ProductGridViewHolder,
            layoutResource = R.layout.item_grid_produk_list,
            onBindViewHolder = { vh, _, it ->
                vh.bind(it.product) {
                    println("Trying to go to product $it")
                    viewModel.addProductToCart(it, isFromGrid = true)
                }
            }
        )

        register(
            viewHolder = ::ProductGridEmptyViewHolder,
            layoutResource = R.layout.item_grid_list_empty,
            onBindViewHolder = { _, _, _ -> /* Do Nothing */ }
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private val listAdapter = adapterOf<ProductListHorizontalItemType> {
        diff(
            areItemsTheSame = { old: ProductListHorizontalItemType,
                                new: ProductListHorizontalItemType ->
                old.productListItem.product.productId == new.productListItem.product.productId
            },
            areContentsTheSame = { _: ProductListHorizontalItemType, _: ProductListHorizontalItemType -> false }
        )
        register(
            viewHolder = ::ProductListViewHolder,
            layoutResource = R.layout.item_product_list,
            onBindViewHolder = { viewHolder, _, it ->
                viewHolder.bind(it.productListItem,
                    onCartClicked = { product ->
                        viewModel.addProductToCart(product, false)
                    }
                ) {
                    goToDetailActivity(it)
                }
            }
        )
    }

    private val orderAdapter = adapterOf<Order> {
        diff(
            areContentsTheSame = { old: Order, new: Order ->
                false
            },
            areItemsTheSame = { old: Order, new: Order ->
                old.product.posProduct.id == new.product.posProduct.id
            }
        )
        register(
            viewHolder = ::OrderViewHolder,
            layoutResource = R.layout.item_pesanan_home_list,
            onBindViewHolder = { viewHolder, _, it ->
                viewHolder.bind(
                    it,
                    { viewModel.addProductToCart(it, false) },
                    {
                        viewModel.deleteProductFromCart(
                            productListItem = it,
                            amount = 1,
                            isFromGrid = false
                        )
                    }
                )
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
        
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        observeViewModel()
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchProductList(pos)
    }


    private fun initView() {
        initLoading()
        initClickListener()
        initRecycleView()
    }

    private fun initLoading() {
        binding.loading.root.setBackgroundColor(Color.TRANSPARENT)
    }

    private fun initClickListener() {
        binding.btnScrollToTop.setOnClickListener {
            binding.rvListProduct.scrollToPosition(1)
        }
        with(binding) {
            tabLayout.addTab(binding.tabLayout.newTab().setText("Pilihan"))
            tabLayout.addTab(binding.tabLayout.newTab().setText("Kumpulan"))
            tabLayout.addOnTabSelectedListener(object  : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    when(tabLayout.selectedTabPosition) {
                        0 -> {
                            viewModel.setCurrentProductViewType(HomeViewState.ProductSection.ProductViewType.Pilihan)
                        }
                        1 -> {
                            viewModel.setCurrentProductViewType(HomeViewState.ProductSection.ProductViewType.Kumpulan)
                        }
                    }
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {}

                override fun onTabReselected(tab: TabLayout.Tab?) {}

            })
            btnCategory.setOnClickListener {
                startCategoryList.launch(null)
            }
            tvSearch.setOnClickListener {
                searchActivityDetail.launch(Any())
            }
        }
    }

    private fun initRecycleView() {
        initProductRecyleView()
    }


    private fun initProductRecyleView() {
        with(binding) {
            productListLayoutManager = LinearLayoutManager(requireContext())
            rvListProduct.apply {
                layoutManager = productListLayoutManager
                adapter = listAdapter
                addItemDecoration(SpacesItemDecoration(15.dp))
                addOnScrollListener(onScrollListener)
            }
            rvGridProduct.apply {
                layoutManager = gridLayoutManager
                adapter = gridAdapter
            }
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner) { homeViewState ->
            with(homeViewState.productSection) {
                toogleListLoading(isFetching)
                shouldGoToDetail?.get()?.let {
                    print("Harus ke detail untuk produk ${it.title}");
                    val intent = Intent(context, ProdukDetailActivity::class.java).apply {
                        putExtra(
                            ProdukDetailActivity.PRODUCT_EXTRAS_KEY,
                            ParcelableProduct.buildFromProduct(it)
                        )
                    }
                    startActivity(intent)
                }
                binding.btnScrollToTop.toggleVisibility(currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan &&
                        productListLayoutManager.findLastCompletelyVisibleItemPosition() > 10)
                binding.rvGridProduct.toggleVisibility(currentProductViewType == HomeViewState.ProductSection.ProductViewType.Pilihan)
                binding.rvListProduct.toggleVisibility(currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan)

                var isListUpdate = false;
                isNewList?.get()?.let {
                    gridAdapter.submitList(ArrayList(getGridFilteredProduct()))
                    listAdapter.submitList(ArrayList(getKumpulanFilteredProduct()))
                     isListUpdate = true;
                }
                if (!isListUpdate) {
                    isKumpuluanFilteredRecently?.get()?.let {
                        listAdapter.submitList(ArrayList(getKumpulanFilteredProduct()))
                    }
                }
                if (!isListUpdate){
                    isGridFilteredRecently?.get()?.let {
                        gridAdapter.submitList(ArrayList(getGridFilteredProduct()))
                    }
                }
                homeViewState.productSection.currentKumpulanFilterCategory?.let{
                    binding.chipKey.setText(it.name)
                }
                if(currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan){
                    homeViewState.productSection.currentKumpulanFilterName?.let {
                        binding.chipSearchKey.setText(it)
                    }
                }else{
                    homeViewState.productSection.currentGridFilterName?.let {
                        binding.chipSearchKey.setText(it)
                    }
                }
                binding.chipKey.toggleVisibility(homeViewState.productSection.currentKumpulanFilterCategory != null && homeViewState.productSection.currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan)
                val currentMode = homeViewState.productSection.currentProductViewType
                binding.chipSearchKey.toggleVisibility(
                    (homeViewState.productSection.currentKumpulanFilterName != null && currentMode == HomeViewState.ProductSection.ProductViewType.Kumpulan) ||
                            (homeViewState.productSection.currentGridFilterName != null && currentMode == HomeViewState.ProductSection.ProductViewType.Pilihan)
                )
                currentlyUpdatedGridIndex?.get()?.let {
                    gridAdapter.notifyItemChanged(it)
                }
                currentlyUpdatedListIndex?.get()?.let {
                    listAdapter.notifyItemChanged(it)
                }
                binding.btnCategory.toggleVisibility(homeViewState.productSection.currentProductViewType == HomeViewState.ProductSection.ProductViewType.Kumpulan)

            }
        }
    }

    private fun goToDetailActivity(productListItem : HomeViewState.ProductSection.ProductListItem) {
        val goToProductDetail : () -> Unit = {
            val intent = Intent(context, ProdukDetailActivity::class.java).apply {
                putExtra(
                    ProdukDetailActivity.PRODUCT_EXTRAS_KEY,
                    ParcelableProduct.buildFromProduct(productListItem.product)
                )
            }
            startActivity(intent)
        }
        if (viewModel.getCurrentViewStateOrNew().isProductExistInCart(productListItem.product.productId)) {
            confirmationOnCartExist("Produk akan dihapus dari keranjang bila anda mengakses detail produk , Apakah anda yakin ?", productListItem) {
                if (productListItem is HomeViewState.ProductSection.ProductListItem.POSProductListItem) {
                    println("${productListItem.posProduct.title} adalah pos product")
                    viewModel.deleteAllProductQtyFromProduct(productListItem)
                }else {
                    println("${productListItem.product.title} adalah bukan pos product")
                }
            }
        }else {
            goToProductDetail()
        }
    }

    private fun confirmationOnCartExist(
        message : String,
        productListItem: HomeViewState.ProductSection.ProductListItem ?= null,
        onDismiss : () -> Unit = {},
        onNotConfirmed: () -> Unit = {},
        onConfirmed: () -> Unit
    ) {
        requireContext().launchConfirmationDialog(
            message = message,
            onDismiss = onDismiss,
            onPositiveButton = {
                if (productListItem == null ) {
                    viewModel.clearCart()
                }
                onConfirmed()
                it.dismiss()
            },
            onNegativeButton = {
                onNotConfirmed()
                it.dismiss()
            }
        )
    }
    
    override fun onSubmit(
        customPaymentPrice: Double,
        minimumPrice: Double,
        paymentMethod: PaymentMethod
    ) {
        viewModel.checkout(pos, customPaymentPrice, paymentMethod)
    }

    private fun onCustomNominal(minimumPrice: Double, paymentMethod: PaymentMethod) {
        val customNominalFragment = CustomNominalFragment()
        customNominalFragment.setListener(this)
        customNominalFragment.setMinimumPrice(minimumPrice)
        customNominalFragment.setPaymentMethod(paymentMethod)
        customNominalFragment.show(
            requireActivity().supportFragmentManager,
            "customNominalFragment"
        )
    }
    private fun toogleListLoading(fetching: Boolean) {
        with(binding) {
            loading.root.toggleVisibility(fetching)
            containerView.toggleVisibility(!fetching)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

//    private fun navigateToTransactionStatusActivity(checkoutResult: TransactionDetail) {
//        startActivity<TransactionStatusActivity> {
//            putExtra(TransactionStatusActivity.TRANSACTION_RESULT_BUNDLE_KEY, checkoutResult)
//        }
//    }

    override fun onPay(
        paymentMethod: PaymentMethod,
        paymentCategoryType: PaymentCategoryType,
        minimumPrice: Double
    ) {
        when (paymentCategoryType) {
            PaymentCategoryType.CASH -> {
                val paymentValue: Double? = paymentMethod.name.toDoubleOrNull()
                if (paymentValue != null) {
                    viewModel.checkout(pos, paymentValue, paymentMethod)
                } else {
                    onCustomNominal(minimumPrice, paymentMethod)
                }
            }
            PaymentCategoryType.NOT_CASH -> {
                viewModel.checkout(pos, minimumPrice, paymentMethod)
            }
        }
    }
}