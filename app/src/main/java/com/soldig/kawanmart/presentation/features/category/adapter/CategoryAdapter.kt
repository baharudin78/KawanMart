package com.soldig.kawanmart.presentation.features.category.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.soldig.kawanmart.model.ProductCategory
import com.soldig.kawanmart.databinding.ItemCategoryListBinding

class CategoryAdapter(
    var categoryList : List<ProductCategory>,
    var onCategoryClicked : (productCategory : ProductCategory) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>(){

    fun changeList(newList: List<ProductCategory>){
        categoryList = newList
        notifyDataSetChanged()
    }

    inner class ViewHolder(
        val binding : ItemCategoryListBinding
        ) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflated = ItemCategoryListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(inflated)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(categoryList[position]) {
                binding.tvTitle.text = this.name
                binding.root.setOnClickListener{
                    onCategoryClicked(this)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }


}