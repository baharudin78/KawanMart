package com.soldig.kawanmart.presentation.common.payment

import android.annotation.SuppressLint
import android.view.View
import com.soldig.kawanmart.model.ReportWallet
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.databinding.ItemReportProdukBinding
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

class PaymentViewHolder(view : View) : RecyclerViewHolder<ReportWallet>(view) {

    private val binding = ItemReportProdukBinding.bind(view);


    @SuppressLint("SetTextI18n")
    fun bind(reportWallet: ReportWallet) {
        with(binding){
            tvName.text = reportWallet.name
            tvQty.text = ": ${reportWallet.total.toMoneyFormat()}"
        }
    }

}