package com.soldig.kawanmart.presentation.features.produkdetail

import androidx.lifecycle.viewModelScope
import com.soldig.kawanmart.interactor.productdetail.ProductDetailInteractor
import com.soldig.kawanmart.model.DiscountType
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.base.BaseViewModel
import com.soldig.kawanmart.interactor.MessageType
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.other.SingleEvent
import com.soldig.kawanmart.utils.other.UIInteraction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProdukDetailViewModel @Inject constructor(
    private val productDetailInteractor: ProductDetailInteractor
)  : BaseViewModel<ProductDetailViewState>(){


    override fun initNewViewState(): ProductDetailViewState {
        return ProductDetailViewState()
    }

    fun updateDiscount(pos: POS, discountType: DiscountType, discountValue: Double) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.discountType = discountType
            product.discountValue = discountValue
            updateProduct(pos, product)
        }
    }

    fun updateStock(pos: POS, newStock: Int) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.qty = newStock
            updateProduct(pos, product)
        }
    }

    fun updateSellPrice(pos: POS, newSellPrice: Double) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.sellPrice = newSellPrice
            updateProduct(pos, product)
        }
    }

    fun updateBuyPrice(pos: POS, newBuyPrice: Double) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.apply {
                purchasePrice = newBuyPrice
            }
            updateProduct(pos, product)
        }
    }

    fun toggleProductIsActive(pos: POS) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.isActive = !product.isActive
            updateProduct(pos, product)
        }
    }

    fun makeProductTobePilihan(pos: POS, position: Int) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.apply {
                isPilihan = true
                ordering = position
            }
            updateProductPilihan(pos, product)
        }
    }

    fun deleteProductFromPilihan(pos: POS) {
        viewModelScope.launch(IO) {
            val product = getCurrentViewStateOrNew().chosenProduct ?: return@launch
            product.apply {
                isPilihan = false
                ordering = null
            }
            updateProductPilihan(pos, product)
        }
    }

    suspend fun updateProductPilihan(pos: POS, product: Product.NotPOSProduct) {
        productDetailInteractor.updateProduct.fetch(pos, product).collect {
            onCollect(
                response = it,
                onLoading = { isLoading ->
                    setViewState(getCurrentViewStateOrNew().apply {
                        productSelectionUpdate.isSelectionUpdating = isLoading
                    })
                },
                onError = {
                    when (it.message) {
                        is MessageType.StringMessage -> {
                            println("Pesan adalah ${it.message.message}")
                        }
                        is MessageType.ResourceMessage -> {
                            println("Pesan adalah ${it.message.messageStringRes}")
                        }
                    }
                },
                executeOnSuccess = { result ->
                    setViewState(getCurrentViewStateOrNew().apply {
                        productId = result.data
                        chosenProduct = product
                        productSelectionUpdate.shouldFetchNewList = SingleEvent(true)
                        productSelectionUpdate.successMessage = SingleEvent("Berhasil memperbaharui produk")
                    })
                }
            )
        }
    }

    private suspend fun updateProduct(pos: POS, product: Product.NotPOSProduct) {
        productDetailInteractor.updateProduct.fetch(pos, product).collect {
            onCollect(
                response = it,
                onLoading = { isLoading ->
                    setIsUpdating(isLoading)
                },
                onError = {
                    when (it.message) {
                        is MessageType.StringMessage -> {
                            println("Pesan adalah ${it.message.message}")
                        }
                        is MessageType.ResourceMessage -> {
                            println("Pesan adalah ${it.message.messageStringRes}")
                        }
                    }
                },
                executeOnSuccess = { result ->
                    setProduct(product, result.data)
                    setUserInteraction(
                        UIInteraction.GenericMessage(
                            genericMessage = MessageType.StringMessage(
                                "Berhasil memperbaharui produk"
                            )
                        )
                    )
                }
            )
        }
    }

    private fun setIsUpdating(isUpdating: Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.isUpdating = isUpdating
        })
    }

    fun setProduct(product: Product.NotPOSProduct, productInPOSId: String?) {
        setViewState(getCurrentViewStateOrNew().apply {
            productId = productInPOSId
            chosenProduct = product
        })
    }
}