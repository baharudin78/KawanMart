package com.soldig.kawanmart.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.navigation.ui.AppBarConfiguration
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.soldig.kawanmart.*
import com.soldig.kawanmart.common.startActivity
import com.soldig.kawanmart.databinding.ActivityHomeBinding
import com.soldig.kawanmart.databinding.NavHeaderMainBinding
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.presentation.features.home.HomeFragment
import com.soldig.kawanmart.presentation.features.login.LoginActivity
import com.soldig.kawanmart.utils.ext.getUserInSharedPref
import com.soldig.kawanmart.utils.ext.goVisible
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding : ActivityHomeBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var currentPOS : POS


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        currentPOS = getUser()
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(view)

        val calendar = Calendar.getInstance()
        val id = Locale("in", "ID")
        val simpleDateFormat = SimpleDateFormat("EEEE, dd MMMM yyyyy", id)
        val dateTime = simpleDateFormat.format(calendar.time)
        binding.include.tvTime.text = dateTime

        binding.include.mToolbar.goVisible()
        setSupportActionBar(binding.include.mToolbar)

        val radius = resources.getDimension(R.dimen.roundcorner)
        val navViewBackground = binding.navView.background as MaterialShapeDrawable
        navViewBackground.shapeAppearanceModel = navViewBackground.shapeAppearanceModel
            .toBuilder()
            .setTopRightCorner(CornerFamily.ROUNDED, radius)
            .setBottomRightCorner(CornerFamily.ROUNDED, radius)
            .build()

        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.include.mToolbar,
            R.string.open_drawer_desc,
            R.string.close_drawer_desc
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        binding.navView.setNavigationItemSelectedListener(this)
        binding.navView.getHeaderView(0)?.let {
            val navHeaderMainBinding = NavHeaderMainBinding.bind(it)
            navHeaderMainBinding.usernameTxt.text = currentPOS.name
            Glide
                .with(this)
                .load(currentPOS.getFullImageURL())
                .into(navHeaderMainBinding.imgProPic)
        }
        supportFragmentManager.beginTransaction()
            .replace(R.id.nav_host_fragment, HomeFragment())
            .commit()
        binding.navClose.setOnClickListener {
            startActivity<CloseShopActivity>{ }
        }
        supportActionBar?.let {
            it.title = currentPOS.name
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_drawer)
        }
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_pesanan -> {
                val fragment = HomeFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit()
            }
            R.id.nav_produk_virtual -> {
                val fragment = VirtualProdukFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit()
            }
            R.id.nav_transaksi -> {
                startActivity<HistoryTransaksiActivity> { }
            }
            R.id.nav_laporan -> {
                startActivity<LaporanActivity> { }
            }
            R.id.nav_setting -> {
                startActivity<SettingActivity> { }
            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return false
    }

    private fun getUser() : POS = getUserInSharedPref() ?: logout()

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }else {
            super.onBackPressed()
        }
    }

    private fun logout() : POS {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
        return POS.produceFailedPOS()
    }
//    private fun refreshApps() : POS {
//        startActivity(Intent(this, SplashActivity::class.java).apply {
//            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        })
//        finish()
//        return POS.produceFailedPOS()
//    }
}