package com.soldig.kawanmart.presentation.features.produkdetail

import android.os.Parcelable
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.utils.other.SingleEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductDetailViewState(
    var isUpdating: Boolean = false,
    var productSelectionUpdate: ProductSelectionUpdate = ProductSelectionUpdate(),
    var productId : String? = null

) : Parcelable {

    @Parcelize
    data class ProductSelectionUpdate(
        var isSelectionUpdating: Boolean = false,

        ): Parcelable {
        @IgnoredOnParcel
        var successMessage: SingleEvent<String>? = null

        @IgnoredOnParcel
        var shouldFetchNewList: SingleEvent<Boolean>? = SingleEvent(true)
    }

    @IgnoredOnParcel
    var chosenProduct : Product.NotPOSProduct?  = null

}