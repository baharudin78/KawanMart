package com.soldig.kawanmart.presentation.features.search

import com.soldig.kawanmart.interactor.search.SearchInteractor
import com.soldig.kawanmart.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchInteractor: SearchInteractor
) : BaseViewModel<SearchViewState>(){

    override fun initNewViewState(): SearchViewState {
        return SearchViewState(false,SearchViewState.Mode.List, "", true)
    }

    fun setSearchKey(searchKey : String) {
        setIsEmpty(false)
        setViewState(getCurrentViewStateOrNew().apply {
            nameKey = searchKey
        })
    }

    fun setIsEmpty(isEmpty :Boolean) {
        setViewState(getCurrentViewStateOrNew().apply {
            this.isEmpty = isEmpty
        })
    }

    fun setCurrentMode(listMode: SearchViewState.Mode) {
        val viewState = getCurrentViewStateOrNew()
        if(viewState.mode != listMode){
            setViewState(viewState.apply {
                this.mode = listMode
            })
        }
    }
}