package com.soldig.kawanmart.base

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.soldig.kawanmart.R
import com.soldig.kawanmart.common.hideSystemUI
import com.soldig.kawanmart.databinding.ToolbarKawanmartBinding
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseActivity : AppCompatActivity() {

    open fun shouldHideSystemUI() : Boolean {
        return true
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus && shouldHideSystemUI() ) hideSystemUI()
    }
    @SuppressLint("SimpleDateFormat")
    fun initTime(whiteColor : Boolean = false, toolbar : ToolbarKawanmartBinding) {
        val calender = Calendar.getInstance()
        val id = Locale("in", "ID")
        val simpleDateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", id)
        val dateTime = simpleDateFormat.format(calender.time)
        with(toolbar) {
            tvTime.text = dateTime
            if (whiteColor) {
                tvTime.setTextColor(Color.WHITE)
                tvAppName.setTextColor(Color.WHITE)
                tvClock.setTextColor(Color.WHITE)
            }else {
                tvTime.setTextColor(Color.BLACK)
                tvAppName.setTextColor(Color.BLACK)
                tvClock.setTextColor(Color.BLACK)
            }
        }
    }
    fun initToolbar(title : String = "", toolbar : ToolbarKawanmartBinding) {
        with(toolbar.mToolbar) {
            isVisible = true
            setSupportActionBar(this)
            supportActionBar?.title = title
            supportActionBar?.setDisplayShowHomeEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            setNavigationIcon(R.drawable.ic_group)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (shouldHideSystemUI()) {
            when(item.itemId) {
                android.R.id.home -> {
                    finish()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }
    fun dialogDocument(title : String, onShow : (view : View, dialog : AlertDialog) -> Unit) {
        val dialog : AlertDialog
        val dialogBuilder = AlertDialog.Builder(this)
        val view : View = layoutInflater.inflate(R.layout.dialog_popup_bank, null)
        dialogBuilder.setView(view)
        dialog = dialogBuilder.create()
        dialog.show()
        view.setOnClickListener { }
        onShow(view, dialog)
    }
 }