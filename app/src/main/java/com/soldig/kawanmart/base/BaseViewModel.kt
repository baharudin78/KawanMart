package com.soldig.kawanmart.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.soldig.kawanmart.interactor.Resource
import com.soldig.kawanmart.utils.other.SingleEvent
import com.soldig.kawanmart.utils.other.UIInteraction
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex

abstract class BaseViewModel<ViewState>(

) : ViewModel(){
    private val mutex = Mutex()
    private val _viewState : MutableLiveData<ViewState> = MutableLiveData()
    val viewState : LiveData<ViewState>
        get() = _viewState
    private val _userInteraction : MutableLiveData<SingleEvent<UIInteraction>> = MutableLiveData(
        SingleEvent(UIInteraction.DoNothing)
    )
    val userInteraction : LiveData<SingleEvent<UIInteraction>>
            get() = _userInteraction
    protected fun <T> onCollect(response : Resource<T>, onLoading : (isLoading : Boolean) ->
    Unit = {},onError : (uiInteraction : UIInteraction) -> Unit = {}, executeOnSuccess : (Resource.Success<T>) -> Unit) {
        when(response) {
            is Resource.Success -> {
                onLoading(false)
                executeOnSuccess(response)
            }
            is Resource.Error -> {
                onError(response.uiInteraction)
                onLoading(false)
                setUserInteraction(userInteraction = response.uiInteraction)
            }
            is Resource.Loading -> {
                onLoading(true)
            }
        }
    }
    fun getCurrentViewStateOrNew(): ViewState{
        return viewState.value ?: initNewViewState()
    }
    fun setViewState(viewState: ViewState){
        viewModelScope.launch(Main){
            _viewState.value = viewState!!
        }
    }
    abstract fun initNewViewState(): ViewState
    fun setUserInteraction(userInteraction: UIInteraction) {
        viewModelScope.launch(Main){
            _userInteraction.value = SingleEvent(userInteraction)
        }
    }

}