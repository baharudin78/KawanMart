package com.soldig.kawanmart.base

import android.content.Intent
import com.soldig.kawanmart.presentation.features.login.LoginActivity
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.utils.ext.clearUser
import com.soldig.kawanmart.utils.ext.getUserInSharedPref

abstract class BaseLoggedInActivity : BaseActivity() {

    val currentPOS : POS by lazy {
        getUserInSharedPref() ?: logOut()
    }

    protected fun logOut() : POS {
        clearUser()
        startActivity(Intent(this, LoginActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
        return POS.produceFailedPOS()
    }
}