package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DFResult<Data>(
    @SerializedName("data")
    val data: Data
)