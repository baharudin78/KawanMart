package com.soldig.kawanmart.datasource.network.mapper

import com.soldig.kawanmart.datasource.network.model.ProductCategoryDto
import com.soldig.kawanmart.model.ProductCategory
import com.soldig.kawanmart.datasource.OneWayDomainMapper
import javax.inject.Inject


class ProductCategoryNetworkMapper
@Inject constructor(

) : OneWayDomainMapper<ProductCategoryDto, ProductCategory> {
    override fun mapToDomain(t: ProductCategoryDto): ProductCategory {
        return ProductCategory(
            name = t.name,
            id = t.id
        )
    }
}