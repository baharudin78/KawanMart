package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.digi.DFResultX

data class DigiTransactionHistoryDto(
    @SerializedName("DFResult")
    val dFResult: List<DFResultX>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)