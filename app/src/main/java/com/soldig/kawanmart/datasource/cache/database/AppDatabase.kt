package com.soldig.kawanmart.datasource.cache.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.soldig.kawanmart.datasource.cache.dao.*
import com.soldig.kawanmart.datasource.cache.model.*

@Database(
    entities = [
        ProductEntity::class,
        PaymentCategoryEntity::class,
        PaymentMethodEntity::class,
        POSEntity::class,
        POSProductCrossRef::class,
        TransactionEntity::class,
        TransactionProductCrossRef::class
    ], version = 17
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun paymentDao(): PaymentDao
    abstract fun posDao(): POSDao
    abstract fun posProductDao(): POSProductDao
    abstract fun transactionDao(): TransactionDao
    abstract fun transactionProductDao() : TransactionProductDao

    companion object {
        const val DATABASE_NAME = "temanmart_db"
    }

}