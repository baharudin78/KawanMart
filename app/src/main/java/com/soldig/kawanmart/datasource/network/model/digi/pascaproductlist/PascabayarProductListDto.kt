package com.soldig.kawanmart.datasource.network.model.digi.pascaproductlist


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.digi.pascaproductlist.DFResult

data class PascabayarProductListDto(
    @SerializedName("DFResult")
    val dFResult: List<DFResult>,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)