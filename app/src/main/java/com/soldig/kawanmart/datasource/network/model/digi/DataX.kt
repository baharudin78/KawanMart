package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DataX(
    @SerializedName("admin")
    val admin: Int,
    @SerializedName("buyer_last_saldo")
    val buyerLastSaldo: Int,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("customer_no")
    val customerNo: String,
    @SerializedName("desc")
    val desc: DescX,
    @SerializedName("message")
    val message: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("rc")
    val rc: String,
    @SerializedName("ref_id")
    val refId: String,
    @SerializedName("selling_price")
    val sellingPrice: Int,
    @SerializedName("sn")
    val sn: String,
    @SerializedName("status")
    val status: String
)