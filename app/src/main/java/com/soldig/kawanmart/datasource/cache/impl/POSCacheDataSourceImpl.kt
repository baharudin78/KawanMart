package com.soldig.kawanmart.datasource.cache.impl

import com.soldig.kawanmart.datasource.cache.abstraction.POSCacheDataSource
import com.soldig.kawanmart.datasource.cache.dao.POSDao
import com.soldig.kawanmart.datasource.cache.model.POSEntity
import com.soldig.kawanmart.model.POS
import javax.inject.Inject


class POSCacheDataSourceImpl
@Inject constructor(
    private val posDao: POSDao
) : POSCacheDataSource {


    override suspend fun insertPOS(pos: POS) {
        posDao.upsert(
            POSEntity(
            phoneNumber = pos.phoneNumber,
            token = pos.token,
            namaToko = pos.name,
            alamatToko = pos.alamat,
            lastUpdateProductDate = ""
        )
        )
    }

    override suspend fun getAllPos(): List<POS> {
        return posDao.getAllPos().map { pos ->
            POS(
                phoneNumber = pos.phoneNumber,
                token = pos.token,
                name = pos.namaToko,
                loginTime = 0L,
                alamat = pos.alamatToko
            )
        }
    }
}