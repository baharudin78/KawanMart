package com.soldig.kawanmart.datasource.network.model

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("Phone")
    val phoneNumber: String,
    @SerializedName("Pin")
    val pin: String,

)