package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.digi.Data

data class DFResultXX(
    @SerializedName("data")
    val `data`: Data
)