package com.soldig.kawanmart.datasource.network.service

import com.soldig.kawanmart.datasource.network.model.ReportDto
import com.soldig.kawanmart.datasource.network.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ReportService {

    @GET("pos/transaction/report")
    suspend fun getReport(
        @Header("Authorization") token: String,
        @Query("StartDate") startDate : String? = null,
        @Query("EndDate") endDate : String? = null
        ) : ApiResponse<ReportDto>

    @GET("pos/transaction/close_report")
    suspend fun getCloseReport(
        @Header("Authorization") token: String
    ) : ApiResponse<ReportDto>
}