package com.soldig.kawanmart.datasource.cache.impl

import com.soldig.kawanmart.datasource.cache.abstraction.PaymentCacheDataSource
import com.soldig.kawanmart.datasource.cache.dao.PaymentDao
import com.soldig.kawanmart.datasource.cache.mapper.PaymentCategoryEntityMapper
import com.soldig.kawanmart.datasource.cache.mapper.PaymentMethodEntityMapper
import com.soldig.kawanmart.model.PaymentCategory
import javax.inject.Inject

class PaymentCacheDataSourceImpl
@Inject constructor(
    private val paymentCategoryEntityMapper: PaymentCategoryEntityMapper,
    private val paymentMethodEntityMapper: PaymentMethodEntityMapper,
    private val paymentDao: PaymentDao
) : PaymentCacheDataSource {



    override suspend fun insertPaymentCategories(paymentCategories: List<PaymentCategory>) {
        paymentDao.insertPaymentCategories(paymentCategories.map { paymentCategoryEntityMapper.mapFromDomain(it) })
        paymentCategories.forEach { paymentCategory ->
            paymentDao.insertPaymentMethods(paymentCategory.paymentMethods.map { paymentMethodEntityMapper.mapFromDomain(it) })
        }
    }


    override suspend fun getAllPaymentCategory(): List<PaymentCategory> {
        return paymentDao.getAllPaymentCategoryAndMethod().map { it ->
            paymentCategoryEntityMapper.mapToDomain(it.paymentCategory).apply {
                paymentMethods = it.paymentMethods.map { paymentMethodEntityMapper.mapToDomain(it) }
            }
        }
    }
}