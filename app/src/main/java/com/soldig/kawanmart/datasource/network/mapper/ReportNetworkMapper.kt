package com.soldig.kawanmart.datasource.network.mapper

import com.soldig.kawanmart.datasource.network.model.ReportDto
import com.soldig.kawanmart.model.ProductReport
import com.soldig.kawanmart.model.Report
import com.soldig.kawanmart.model.ReportWallet
import com.soldig.kawanmart.datasource.OneWayDomainMapper
import javax.inject.Inject

class ReportNetworkMapper
@Inject constructor(
) : OneWayDomainMapper<ReportDto, Report> {
    override fun mapToDomain(t: ReportDto): Report {
        return Report(
            billTotal = t.totalBill,
            soldProductQty = t.soldProduct,
            canceledBill = t.canceledBill,
            canceledProductQty = t.canceledProduct,
            totalCanceledAmount = t.canceledAmount,
            discountTotal = t.discountAmount,
            totalAmountWithoutDiscount = t.soldAmount,
            totalAmountAfterDiscount = t.totalAmount,
            soldProduct = t.itemsSold.map { ProductReport(it.name,it.quantity) },
            canceledProduct = t.itemsCanceled.map { ProductReport(it.name,it.quantity) },
            beginningBalanceTotal = t.totalShopBeginningBalance,
            totalAllCash = t.totalWholeCash,
            totalAmountPaidWithCash = t.totalCashTransaction,
            totalAmountPaidWithNotCash = t.totalNonCashTransaction,
            reportWallet = t.reportWallet.map { ReportWallet(it.jumlahPerWallet.toDoubleOrNull() ?: 0.0 , it.namaWallet) }
        )
    }
}