package com.soldig.kawanmart.datasource.cache.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.soldig.kawanmart.model.DiscountType


@Entity(
    tableName = "transaction_product",
    foreignKeys = [
        ForeignKey(
            entity = TransactionEntity::class,
            parentColumns = ["transaction_id"],
            childColumns = ["transactionId"],
            onUpdate = ForeignKey.NO_ACTION,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ProductEntity::class,
            onDelete = ForeignKey.NO_ACTION,
            onUpdate = ForeignKey.NO_ACTION,
            parentColumns = ["product_id"],
            childColumns = ["productId"]
        ),
        ForeignKey(
            entity = POSProductCrossRef::class,
            onDelete = ForeignKey.NO_ACTION,
            onUpdate = ForeignKey.NO_ACTION,
            parentColumns = ["pos_product_id"],
            childColumns = ["posProductId"]
        )
    ],
    indices = [
        Index(value = ["transactionId"], unique = false),
        Index(value = ["productId"], unique = false),
        Index(value = ["posProductId"], unique = false)
    ]
)
data class TransactionProductCrossRef(
    @PrimaryKey
    val transactionProductId: String,
    val transactionId: String,
    val productId: String,
    val posProductId: String,
    val qty: Int,
    val discount: Double = 0.0,
    val discountType: DiscountType,
    val priceBeforeDiscount: Double = 0.0,
    val priceAfterDiscount: Double = 0.0,
)