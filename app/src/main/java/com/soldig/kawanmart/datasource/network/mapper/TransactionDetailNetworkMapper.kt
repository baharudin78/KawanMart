package com.soldig.kawanmart.datasource.network.mapper

import com.soldig.kawanmart.datasource.network.model.OrderItemDto
import com.soldig.kawanmart.datasource.network.model.TransactionDto
import com.soldig.kawanmart.model.TransactionDetail
import com.soldig.kawanmart.model.TransactionProductDetail
import com.soldig.kawanmart.model.TransactionStatus
import com.soldig.kawanmart.datasource.OneWayDomainMapper
import javax.inject.Inject

class TransactionDetailNetworkMapper
@Inject constructor(
    private val transactionProductNetworkMapper: TransactionProductNetworkMapper
) : OneWayDomainMapper<TransactionDto, TransactionDetail> {
    override fun mapToDomain(t: TransactionDto): TransactionDetail {
        var totalItem = 0
        t.details.forEach {
            totalItem += it.quantity
        }
        return TransactionDetail(
            id = t.id,
            orderNumber = t.transNo,
            paymentWith = t.payWith ?: if(t.isPaid) "Tidak diketahui" else "Belum dilakukan pembayaran",
            date = t.created,
            productList = t.details.map { transactionProductNetworkMapper.mapToDomain(it) },
            totalItem = totalItem,
            totalPaid = t.paid,
            totalChange = t.change,
            totalDiscount = t.discount,
            totalBeforeDiscount = t.amount,
            totalAfterDiscount = t.totalAmount,
            status = t.status ?: "" ,
            buyerAddress = t.address,
            buyerName = t.customerName,
            buyerPhoneNumber = t.customerPhone,
            statusType = TransactionStatus.getByValue(t.transStatus) ?: TransactionStatus.UNKNOWN
        )
    }
}

class TransactionProductNetworkMapper
@Inject constructor() : OneWayDomainMapper<OrderItemDto, TransactionProductDetail>{
    override fun mapToDomain(t: OrderItemDto): TransactionProductDetail {
        return TransactionProductDetail(
            name = t.name,
            jml = t.quantity,
            productId = t.mShopproductId,
            discountValue = t.discount,
            priceBeforeDiscount = t.actualPrice,
            priceAfterDiscount = t.price
        )
    }
}

