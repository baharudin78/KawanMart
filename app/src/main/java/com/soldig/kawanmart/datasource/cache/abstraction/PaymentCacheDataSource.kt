package com.soldig.kawanmart.datasource.cache.abstraction

import com.soldig.kawanmart.model.PaymentCategory

interface PaymentCacheDataSource {


    suspend fun insertPaymentCategories(paymentCategories: List<PaymentCategory>)

    suspend fun getAllPaymentCategory() : List<PaymentCategory>

}