package com.soldig.kawanmart.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.digi.cektagihan.DFResult

data class DigiCheckTagihanDto(
    @SerializedName("DFResult")
    val dFResult: DFResult,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)