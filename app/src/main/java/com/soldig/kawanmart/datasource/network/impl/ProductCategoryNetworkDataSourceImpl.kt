package com.soldig.kawanmart.datasource.network.impl

import com.soldig.kawanmart.datasource.network.abstraction.CategoryNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.ProductCategoryNetworkMapper
import com.soldig.kawanmart.datasource.network.service.CategoryService
import com.soldig.kawanmart.model.ProductCategory
import javax.inject.Inject


class ProductCategoryNetworkDataSourceImpl
@Inject constructor(
    private val productCategoryNetworkMapper: ProductCategoryNetworkMapper,
    private val categoryService: CategoryService
) : CategoryNetworkDataSource {
    override suspend fun getAllCategory(token: String): List<ProductCategory> {
        return categoryService.getAllCategory(token).data.map { productCategoryNetworkMapper.mapToDomain(it) }
    }
}