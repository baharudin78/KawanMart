package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DetailX(
    @SerializedName("admin")
    val admin: String,
    @SerializedName("denda")
    val denda: String,
    @SerializedName("nilai_tagihan")
    val nilaiTagihan: String,
    @SerializedName("periode")
    val periode: String
)