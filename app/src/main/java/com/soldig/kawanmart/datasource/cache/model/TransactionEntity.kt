package com.soldig.kawanmart.datasource.cache.model

import androidx.room.*
import com.soldig.kawanmart.model.TransactionStatus

@Entity(
    tableName = "transaction_entity",
    foreignKeys = [
        ForeignKey(
            entity = POSEntity::class,
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.NO_ACTION,
            parentColumns = ["phone_number"],
            childColumns = ["posId"]
        )
    ],
    indices = [
        Index(value= ["posId"], unique = false)
    ]
)
data class TransactionEntity(
    @PrimaryKey
    @ColumnInfo(name ="transaction_id")
    val id: String,
    val posId:String,
    val transactionNumber: String,
    val payWith: String,
    val paymentId: String,
    val transactionDate: Long,
    val totalItems: Int = 0,
    val totalDiscount: Double = 0.0,
    val totalBeforeDiscount: Double = 0.0,
    val totalAfterDiscount: Double = 0.0,
    val totalPaid: Double = 0.0,
    val totalChange: Double = 0.0,
    val transactionStatus: TransactionStatus,
    val buyerName: String? = null,
    var buyerPhoneNumber: String? = null,
    val buyerAddress: String? = null,
)
