package com.soldig.kawanmart.datasource.cache.abstraction

import com.soldig.kawanmart.model.POS

interface POSCacheDataSource {

    suspend fun insertPOS(pos: POS)

    suspend fun getAllPos() : List<POS>


}