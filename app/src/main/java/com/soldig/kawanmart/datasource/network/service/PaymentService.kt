package com.soldig.kawanmart.datasource.network.service

import com.soldig.kawanmart.datasource.network.model.PaymentCategoryDto
import com.soldig.kawanmart.datasource.network.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Header

interface PaymentService {

    @GET("paymentcategory/list")
    suspend fun getAllPaymentMethode(
        @Header("Authorization") token: String
    ) : ApiResponse<List<PaymentCategoryDto>>
}