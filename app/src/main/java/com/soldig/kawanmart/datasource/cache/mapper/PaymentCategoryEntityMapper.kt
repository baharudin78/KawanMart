package com.soldig.kawanmart.datasource.cache.mapper

import com.soldig.kawanmart.datasource.cache.model.PaymentCategoryEntity
import com.soldig.kawanmart.model.PaymentCategory
import com.soldig.kawanmart.datasource.DomainMapper
import javax.inject.Inject

class PaymentCategoryEntityMapper
@Inject constructor() : DomainMapper<PaymentCategoryEntity, PaymentCategory> {
    override fun mapFromDomain(domain: PaymentCategory): PaymentCategoryEntity {
        return PaymentCategoryEntity(
            paymentCategoryId = domain.id,
            name = domain.name,
            paymentType = domain.paymentType
        )
    }

    override fun mapToDomain(t: PaymentCategoryEntity): PaymentCategory {
        return PaymentCategory(
            id= t.paymentCategoryId,
            name = t.name,
            paymentType = t.paymentType
        )
    }
}