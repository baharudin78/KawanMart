package com.soldig.kawanmart.datasource

interface DomainMapper<T , Domain>{

    fun mapFromDomain(domain : Domain) : T

    fun mapToDomain(t : T) : Domain
}
interface OneWayDomainMapper<T, Domain>{
    fun mapToDomain(t: T) : Domain
}
interface OneWayMapper<Input, Output >{
    fun mapToDomain(t: Input) : Output
}
