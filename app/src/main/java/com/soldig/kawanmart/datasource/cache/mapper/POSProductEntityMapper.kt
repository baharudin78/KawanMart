package com.soldig.kawanmart.datasource.cache.mapper

import com.soldig.kawanmart.datasource.cache.model.POSProductCrossRef
import com.soldig.kawanmart.datasource.cache.model.POSWithProduct
import com.soldig.kawanmart.datasource.cache.model.ProductEntity
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.datasource.DomainMapper
import java.util.*
import javax.inject.Inject

class POSProductEntityMapper  @Inject constructor() : DomainMapper<POSWithProduct, Product> {

    override fun mapToDomain(t: POSWithProduct): Product {
        val productEntity = t.productEntity
        val posProduct = t.posProductEntity
        if(posProduct.isOwned){
            return Product.POSProduct(
                id = posProduct.posProductID,
                posId = posProduct.posId,
                productId = productEntity.productId,
                categoryId = productEntity.categoryId,
                img = productEntity.img,
                title = productEntity.title,
                qty = posProduct.qty,
                sellPrice = posProduct.sellPrice,
                purchasePrice = posProduct.purchasePrice,
                sellAfterDiscountPrice = posProduct.sellAfterDiscountPrice,
                discountType = posProduct.discountType,
                discountValue = posProduct.discountValue,
                isActive = posProduct.isActive,
                isPilihan = posProduct.isPilihan,
                isOwned = posProduct.isOwned,
                ordering = posProduct.ordering,
                packSize =  productEntity.packSize
            )
        }
        return Product.NotPOSProduct(
            posId = posProduct.posId,
            productId = productEntity.productId,
            categoryId = productEntity.categoryId,
            img = productEntity.img,
            title = productEntity.title,
            qty = posProduct.qty,
            sellPrice = posProduct.sellPrice,
            purchasePrice = posProduct.purchasePrice,
            sellAfterDiscountPrice = posProduct.sellAfterDiscountPrice,
            discountType = posProduct.discountType,
            discountValue = posProduct.discountValue,
            isActive = posProduct.isActive,
            isPilihan = posProduct.isPilihan,
            isOwned = posProduct.isOwned,
            ordering = posProduct.ordering,
            packSize = productEntity.packSize
        )

    }

    override fun mapFromDomain(domain: Product): POSWithProduct {
        return POSWithProduct(
            productEntity = ProductEntity(
                productId = domain.productId,
                categoryId = domain.categoryId,
                img = domain.img,
                title = domain.title,
                packSize = domain.packSize
            ),
            posProductEntity = POSProductCrossRef(
                posId = domain.posId,
                qty = domain.qty,
                posProductID =  if(domain is Product.POSProduct) domain.id else UUID.randomUUID().toString(),
                productId = domain.productId,
                sellPrice = domain.sellPrice,
                purchasePrice = domain.purchasePrice,
                sellAfterDiscountPrice = domain.sellAfterDiscountPrice,
                discountType = domain.discountType,
                discountValue = domain.discountValue,
                isActive = domain.isActive,
                isPilihan = domain.isPilihan,
                isOwned = domain.isOwned,
                ordering = domain.ordering
            )
        )
    }
}