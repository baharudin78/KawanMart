package com.soldig.kawanmart.datasource.cache.model

import androidx.room.Embedded

data class POSWithProduct(
    @Embedded val productEntity: ProductEntity,
    @Embedded val posProductEntity: POSProductCrossRef
)