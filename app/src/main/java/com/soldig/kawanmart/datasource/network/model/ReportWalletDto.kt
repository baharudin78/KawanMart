package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ReportWalletDto(
    @SerializedName("JumlahPerWallet")
    val jumlahPerWallet: String,
    @SerializedName("NamaWallet")
    val namaWallet: String
)