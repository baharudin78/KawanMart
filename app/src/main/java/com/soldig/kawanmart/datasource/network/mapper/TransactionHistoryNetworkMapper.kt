package com.soldig.kawanmart.datasource.network.mapper

import com.soldig.kawanmart.datasource.network.model.TransactionDto
import com.soldig.kawanmart.model.TransactionHistory
import com.soldig.kawanmart.model.TransactionStatus
import com.soldig.kawanmart.datasource.OneWayDomainMapper
import javax.inject.Inject


class TransactionHistoryNetworkMapper
@Inject constructor(

) : OneWayDomainMapper<TransactionDto, TransactionHistory> {
    override fun mapToDomain(t: TransactionDto): TransactionHistory {
        return TransactionHistory(
            orderDate = t.created,
            orderNumber = t.transNo,
            totalItem = t.totalItem,
            totalPrice = t.totalAmount,
            id = t.id,
            status = TransactionStatus.getByValue(t.transStatus) ?: TransactionStatus.UNKNOWN
        )
    }

}