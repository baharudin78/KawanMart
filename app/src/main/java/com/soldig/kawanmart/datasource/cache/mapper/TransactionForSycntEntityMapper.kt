package com.soldig.kawanmart.datasource.cache.mapper

import android.annotation.SuppressLint
import com.soldig.kawanmart.datasource.cache.model.TransactionForSyncRelation
import com.soldig.kawanmart.model.ProductTransactionForSync
import com.soldig.kawanmart.model.TransactionForSync
import com.soldig.kawanmart.datasource.OneWayDomainMapper
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionForSyncEntityMapper
    @Inject constructor(

    ): OneWayDomainMapper<TransactionForSyncRelation, TransactionForSync> {
    override fun mapToDomain(t: TransactionForSyncRelation): TransactionForSync {
        val transaction = t.transaction
        val productList = t.transactionProductCrossRef
        return TransactionForSync(
            transactionId = transaction.id,
            paymentId = transaction.paymentId,
            paid = transaction.totalPaid,
            transactionDate = dateToStringForm(Date(transaction.transactionDate)),
            listProduct = productList.map {
                ProductTransactionForSync(
                    shopProductId = it.posProductId,
                    discountType = it.discountType,
                    sellPrice = it.priceBeforeDiscount,
                    discountValue = it.discount,
                    currentPrice = it.priceAfterDiscount,
                    quantity = it.qty
                )
            }
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateToStringForm(date: Date): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }
}
