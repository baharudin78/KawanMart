package com.soldig.kawanmart.datasource.network.service

import com.soldig.kawanmart.datasource.network.model.ProductCategoryDto
import com.soldig.kawanmart.datasource.network.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Header


interface CategoryService {

    @GET("product_category/list")
    suspend fun getAllCategory(
        @Header("Authorization") token: String
    ) : ApiResponse<List<ProductCategoryDto>>

}