package com.soldig.kawanmart.datasource.cache.impl

import android.annotation.SuppressLint
import com.soldig.kawanmart.datasource.cache.abstraction.TransactionCacheDataSource
import com.soldig.kawanmart.datasource.cache.dao.TransactionDao
import com.soldig.kawanmart.datasource.cache.dao.TransactionProductDao
import com.soldig.kawanmart.datasource.cache.mapper.TransactionForSyncEntityMapper
import com.soldig.kawanmart.datasource.cache.model.TransactionEntity
import com.soldig.kawanmart.datasource.cache.model.TransactionProductCrossRef
import com.soldig.kawanmart.model.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TransactionCacheDataSourceImpl
@Inject constructor(
    private val transactionDao: TransactionDao,
    private val transactionProductDao: TransactionProductDao,
    private val transactionForSyncEntityMapper: TransactionForSyncEntityMapper
) : TransactionCacheDataSource {

    override suspend fun checkout(
        phoneNumber: String,
        transaction: Transaction
    ): TransactionDetail {
        val transactionId = generateID()
        val transactionNumber = generateID()
        val transactionTime = Date().time
        transactionDao.insert(
            TransactionEntity(
                id = transactionId,
                posId = phoneNumber,
                transactionNumber = transactionNumber,
                transactionDate = transactionTime,
                totalItems = transaction.cart.totalItems,
                totalDiscount = transaction.cart.totalDiscount,
                totalBeforeDiscount = transaction.cart.totalBeforeDiscount,
                totalAfterDiscount = transaction.cart.totalAfterDiscount,
                totalPaid = transaction.paidMoney,
                transactionStatus = TransactionStatus.SELESAI,
                totalChange = transaction.paidMoney - transaction.cart.totalAfterDiscount,
                buyerName = null,
                buyerPhoneNumber = null,
                buyerAddress = null,
                payWith = transaction.paymentMethod.name,
                paymentId = transaction.paymentMethod.id
            )
        )
        transactionProductDao.insert(transaction.cart.orderList.values.map {
            TransactionProductCrossRef(
                transactionProductId = generateID(),
                transactionId = transactionId,
                productId = it.product.posProduct.productId,
                posProductId = it.product.posProduct.id,
                qty = it.quantity,
                discount = it.product.posProduct.discountValue,
                discountType = it.product.posProduct.discountType,
                priceBeforeDiscount = it.product.posProduct.sellPrice,
                priceAfterDiscount = it.product.posProduct.sellAfterDiscountPrice
            )
        })

        return TransactionDetail(
            id = transactionId,
            orderNumber = transactionNumber,
            status = TransactionStatus.SELESAI.name,
            paymentWith = transaction.paymentMethod.name,
            date = dateToStringForm(Date(transactionTime)),
            totalItem = transaction.cart.totalItems,
            totalPaid = transaction.paidMoney,
            totalChange = transaction.paidMoney - transaction.cart.totalAfterDiscount,
            totalDiscount = transaction.cart.totalDiscount,
            totalBeforeDiscount = transaction.cart.totalBeforeDiscount,
            totalAfterDiscount = transaction.cart.totalAfterDiscount,
            buyerName = null,
            buyerPhoneNumber = null,
            buyerAddress = null,
            statusType = TransactionStatus.SELESAI,
            productList = transaction.cart.orderList.map {
                with(it.value){
                    TransactionProductDetail(
                        productId = product.posProduct.productId ,
                        name = product.posProduct.title,
                        jml = quantity,
                        priceAfterDiscount = product.posProduct.sellAfterDiscountPrice,
                        priceBeforeDiscount = product.posProduct.sellPrice,
                        discountValue = product.posProduct.discountValue
                    )
                }
            }
        )
    }


    override suspend fun getTransactionDetail(transactionId: String): TransactionDetail {
            val transactionResult = transactionDao.getTransactionDetail(transactionId)
            val transactionEntity = transactionResult[0].transaction
            val transactionProducts = transactionResult.map {
                TransactionProductDetail(
                    productId = it.product.productId ,
                    name = it.product.title,
                    jml = it.transactionProductCrossRef.qty,
                    priceAfterDiscount = it.transactionProductCrossRef.priceAfterDiscount,
                    priceBeforeDiscount = it.transactionProductCrossRef.priceBeforeDiscount,
                    discountValue = it.transactionProductCrossRef.discount
                )
            }
            return TransactionDetail(
                id = transactionEntity.id,
                orderNumber = transactionEntity.transactionNumber,
                status = transactionEntity.transactionStatus.getInString(),
                paymentWith = transactionEntity.payWith,
                date = dateToStringForm(Date(transactionEntity.transactionDate)),
                totalItem = transactionEntity.totalItems,
                totalPaid = transactionEntity.totalPaid,
                totalChange = transactionEntity.totalChange,
                totalDiscount = transactionEntity.totalDiscount,
                totalBeforeDiscount = transactionEntity.totalBeforeDiscount,
                totalAfterDiscount = transactionEntity.totalAfterDiscount,
                buyerAddress = transactionEntity.buyerAddress,
                buyerPhoneNumber = transactionEntity.buyerPhoneNumber,
                buyerName = transactionEntity.buyerName,
                statusType = transactionEntity.transactionStatus,
                productList = transactionProducts
            )
    }

    override suspend fun getAllTransactionHistory(phoneNumber: String): List<TransactionHistory> {
        return transactionDao.getTransactionByPOS(phoneNumber).map {
            TransactionHistory(
                id = it.id,
                orderNumber = it.transactionNumber,
                orderDate = dateToStringForm(Date(it.transactionDate)),
                totalItem = it.totalItems,
                totalPrice = it.totalAfterDiscount,
                status = it.transactionStatus
            )
        }
    }

    override suspend fun getAllTransactionForSync(phoneNumber: String): List<TransactionForSync> {
        return transactionDao.getTransactionDataForSync(phoneNumber).map {
            transactionForSyncEntityMapper.mapToDomain(it)
        }
    }

    override suspend fun deleteAllPOSTransaction(phoneNumber: String) {
        return transactionDao.deleteAllTransactionByPOS(phoneNumber)
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateToStringForm(date: Date): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        return simpleDateFormat.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateLongFromString(dateString: String): Long {
        val format = "yyyy-MM-dd HH:mm"
        val simpleDateFormat = SimpleDateFormat(format)
        return simpleDateFormat.parse(dateString).time
    }

    private fun generateID() : String {
        return UUID.randomUUID().toString()
    }


}