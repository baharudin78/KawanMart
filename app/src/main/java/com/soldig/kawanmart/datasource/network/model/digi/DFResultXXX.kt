package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.digi.DataX

data class DFResultXXX(
    @SerializedName("data")
    val `data`: DataX
)