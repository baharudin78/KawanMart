package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.Schedule
import com.soldig.kawanmart.model.SettingModel


interface ScheduleNetworkDataSource {

    suspend fun getScheduleAndDeliveryStatus(token:String) : SettingModel

    suspend fun postSchedule(token:String, schedule: Schedule) : String

}