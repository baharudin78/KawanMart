package com.soldig.kawanmart.datasource.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soldig.kawanmart.model.PaymentCategoryType

@Entity(tableName = "payment_category")
data class PaymentCategoryEntity(
    @PrimaryKey
    val paymentCategoryId: String,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "payment_type")
    val paymentType: PaymentCategoryType
)