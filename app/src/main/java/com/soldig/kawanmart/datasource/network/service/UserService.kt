package com.soldig.kawanmart.datasource.network.service

import com.soldig.kawanmart.model.ResponseGetSaldo
import com.soldig.kawanmart.model.ResponseRiwayatTopup
import com.soldig.kawanmart.datasource.network.ApiResponse
import com.soldig.kawanmart.datasource.network.model.BalanceDto
import com.soldig.kawanmart.datasource.network.model.POSDto
import com.soldig.kawanmart.datasource.network.model.UserDto
import com.soldig.kawanmart.model.ResponseTopup
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface UserService {

    @Headers("Content-Type: application/json")
    @POST("pos/login")
    suspend fun login(
        @Body user: UserDto
    ) : ApiResponse<POSDto>


    @POST("pos/shop/set_beginning_balance")
    suspend fun startBeginingBalance(
        @Header("Authorization") token: String,
        @Body balance: BalanceDto
    ) : ApiResponse<Any?>

    @POST("pos/shop/set_delivery")
    suspend fun setDeliveryStatus(
        @Header("Authorization") token: String,
        @Body status :  HashMap<String, Boolean>
    ) : ApiResponse<Nothing?>

    @POST("pos/shop/close")
    suspend fun closePOS(
        @Header("Authorization") token: String
    ) : ApiResponse<Nothing?>

    @POST("pos/shop/update_fcm")
    suspend fun updateFCM(
        @Header("Authorization") token: String,
        @Body fcmData :  HashMap<String, String>
    ) : ApiResponse<Nothing?>

    @Multipart
    @POST("pay/transfer_warung")
    suspend fun topup_virtual(
        @Header("Authorization") token: String,
        @PartMap param: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part bukti_transfer: MultipartBody.Part
    ) : ResponseTopup

    @GET("pay/warung_history_topup")
    suspend fun getRiwayatTopup(
        @Header("Authorization") token: String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String,
        @Query("status") status: String
    ) : ResponseRiwayatTopup

    @GET("pay/warung_saldo")
    suspend fun getSaldo(
        @Header("Authorization") token: String
    ) : ResponseGetSaldo

}