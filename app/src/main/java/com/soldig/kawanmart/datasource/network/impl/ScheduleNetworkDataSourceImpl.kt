package com.soldig.kawanmart.datasource.network.impl

import com.soldig.kawanmart.datasource.network.abstraction.ScheduleNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.ScheduleNetworkMapper
import com.soldig.kawanmart.datasource.network.service.ScheduleService
import com.soldig.kawanmart.model.Schedule
import com.soldig.kawanmart.model.SettingModel
import com.soldig.ayogrosir.utils.ext.getDayOfweek
import com.soldig.ayogrosir.utils.ext.toBooleanOrNull
import com.soldig.ayogrosir.utils.ext.translateDate
import java.util.*
import javax.inject.Inject

class ScheduleNetworkDataSourceImpl
@Inject
constructor(
    private val scheduleService: ScheduleService,
    private val scheduleNetworkMapper: ScheduleNetworkMapper
): ScheduleNetworkDataSource {

    override suspend fun getScheduleAndDeliveryStatus(token: String): SettingModel {
        val requestResult = scheduleService.getSchedule(token)
        val scheduleList = requestResult.data.mapNotNull {
           if(!isStringADate(it.key)){
                null
           }else{
               scheduleNetworkMapper.mapToDomain(Pair(it.key,it.value))
           }
        }.sortedBy {
            val dayOfWeek = getDayOfweek(it.day)
            if( dayOfWeek == 1) 9 else dayOfWeek
        }
        return SettingModel(
            requestResult.data["HasDelivery"]?.toBooleanOrNull() ?: false,
            scheduleList
        )
    }

    override suspend fun postSchedule(token: String, schedule: Schedule) : String {
        val schedulePair = scheduleNetworkMapper.mapFromDomain(schedule)
        return scheduleService.postSchedule(token, hashMapOf(
            schedulePair.first to schedulePair.second
        )).message
    }

    private fun isStringADate(string : String) : Boolean {
        return try{
            //Try to parse the string in to date
            translateDate(Locale.ENGLISH, Locale.ENGLISH, string)
            true
        }catch(exception : Exception){
            false
        }
    }

}