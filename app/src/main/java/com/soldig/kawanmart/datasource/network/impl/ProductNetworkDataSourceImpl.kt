package com.soldig.kawanmart.datasource.network.impl

import com.soldig.kawanmart.datasource.network.abstraction.ProductNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.ProductNetworkMapper
import com.soldig.kawanmart.datasource.network.service.ProductService
import com.soldig.kawanmart.model.DiscountType
import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.model.POS
import javax.inject.Inject

class ProductNetworkDataSourceImpl
@Inject constructor(
    private val productService: ProductService,
    private val productNetworkMapper: ProductNetworkMapper
) : ProductNetworkDataSource {
    override suspend fun getAllProductList(pos: POS): List<Product> {
        val prod = productService.getAllProduct(pos.token).data.map {
            productNetworkMapper.mapToDomainWithNewPOSID(it, pos.phoneNumber)
        }
        return prod
    }
    override suspend fun getAllAvailableProductOnly(pos:POS): List<Product.POSProduct> {
        return productService.getAllAvailableProductOnly(pos.token).data.mapNotNull{
            val mapResult = productNetworkMapper.mapToDomainWithNewPOSID(it, pos.phoneNumber)
            if(mapResult is Product.POSProduct) (mapResult as Product.POSProduct) else null
        }
    }
    override suspend fun updateProduct(token:String, product: Product) : Product.POSProduct{
        val input = HashMap<String, Any?>().apply {
            put("PurchasePrice", product.purchasePrice)
            put("SellPrice",product.sellPrice)
            put("Stock",product.qty)
            put("DiscountType", productNetworkMapper.discountTypeToId(product.discountType))
            put("Discount", if(product.discountType != DiscountType.Nothing) product.discountValue else null)
            put("IsFeatured", product.isPilihan)
            put("IsActive",product.isActive)
            put("Ordering",product.ordering)
        }
        input.forEach {
            println("Untuk ${it.key}  adalah ${it.value}")
        }
        val productUpdateResult = productService.updateProduct(token,product.productId,input).data
        return Product.POSProduct(
            id = productUpdateResult.id,
            posId = product.posId,
            productId = product.productId,
            categoryId = product.categoryId,
            img = product.img,
            title = product.title,
            qty = productUpdateResult.stock,
            sellPrice = productUpdateResult.sellPrice,
            sellAfterDiscountPrice = productUpdateResult.currentPrice,
            purchasePrice = productUpdateResult.purchasePrice,
            discountType = discountIdToType(productUpdateResult.discountType) ,
            discountValue = productUpdateResult.discountValue,
            isActive = productUpdateResult.isActive == 1,
            isPilihan = productUpdateResult.isFeatured == 1,
            isOwned = true,
            ordering = productUpdateResult.ordering ,
            packSize = product.packSize
        )
    }
    private fun discountIdToType(discountId : Int?) : DiscountType {
        return when(discountId) {
            1 -> DiscountType.Percentage
            2 -> DiscountType.Nominal
            else -> DiscountType.Nothing
        }
    }
    override suspend fun getAllProductByCategory(
        pos:POS,
        productId: String
    ): List<Product> {
        return productService.getAllProductByCategory(pos.token,productId).data.map {
             productNetworkMapper.mapToDomainWithNewPOSID(it, pos.phoneNumber)
        }
    }
    override suspend fun getProductLastUpdate(token: String): String {
        return productService.getLastUpdateTime(token).data
    }


}