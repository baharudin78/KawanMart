package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiTopupResultDto(
    @SerializedName("DFResult")
    val dFResult: DFResultXX,
    @SerializedName("FeeData")
    val feeData: FeeData,
    @SerializedName("Message")
    val message: String,
    @SerializedName("Response")
    val response: String
)