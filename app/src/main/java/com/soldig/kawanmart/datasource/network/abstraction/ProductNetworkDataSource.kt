package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.Product
import com.soldig.kawanmart.model.POS

interface ProductNetworkDataSource {

    suspend fun getAllProductList(pos: POS) : List<Product>

    suspend fun getAllAvailableProductOnly(pos:POS) : List<Product.POSProduct>

    suspend fun updateProduct(token:String, product: Product) : Product.POSProduct

    suspend fun getAllProductByCategory(pos:POS, productId: String) : List<Product>

    suspend fun getProductLastUpdate(token: String) : String


}