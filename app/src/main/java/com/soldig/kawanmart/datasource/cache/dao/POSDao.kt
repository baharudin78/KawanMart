package com.soldig.kawanmart.datasource.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soldig.kawanmart.datasource.cache.model.POSEntity

@Dao
abstract class POSDao : BaseDao<POSEntity>() {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract  suspend fun insertPOS(posEntity: POSEntity)


    @Query("SELECT * FROM pos WHERE phone_number=:posPhoneNumber")
    abstract  suspend fun getPOS(posPhoneNumber: String) : POSEntity


    @Query("SELECT * FROM pos")
    abstract suspend fun getAllPos() : List<POSEntity>
}