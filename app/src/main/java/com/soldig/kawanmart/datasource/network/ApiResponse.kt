package com.soldig.kawanmart.datasource.network

import com.google.gson.annotations.SerializedName

data class ApiResponse<Data>(
    @SerializedName("Message")
    val message: String,
    @SerializedName("Data")
    val data: Data,
    @SerializedName("Response")
    val response: Response
)

data class Response(
    @SerializedName("Status")
    val status: String,
    @SerializedName("Code")
    val code: Int
)
