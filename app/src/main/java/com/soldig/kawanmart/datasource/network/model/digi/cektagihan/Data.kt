package com.soldig.kawanmart.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("admin")
    val admin: Int,
    @SerializedName("buyer_last_saldo")
    val buyerLastSaldo: Int,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("customer_no")
    val customerNo: String,
    @SerializedName("desc")
    val desc: Desc,
    @SerializedName("message")
    val message: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("rc")
    val rc: String,
    @SerializedName("ref_id")
    val refId: String,
    @SerializedName("selling_price")
    val sellingPrice: Double = 0.0,
    @SerializedName("sn")
    val sn: String,
    @SerializedName("status")
    val status: String
)