package com.soldig.kawanmart.datasource.network.impl

import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.messaging.FirebaseMessaging
import com.soldig.kawanmart.model.ResponseGetSaldo
import com.soldig.kawanmart.model.ResponseRiwayatTopup
import com.soldig.kawanmart.datasource.network.abstraction.UserNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.UserNetworkMapper
import com.soldig.kawanmart.datasource.network.model.BalanceDto
import com.soldig.kawanmart.datasource.network.service.UserService
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.ResponseTopup
import com.soldig.kawanmart.model.User
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

class UserNetworkDataSourceImpl @Inject constructor(
    private val userService: UserService,
    private val userNetworkMapper: UserNetworkMapper,
    private val firebaseMessaging: FirebaseMessaging
) : UserNetworkDataSource {
    override suspend fun login(user: User): POS {
        val posDto = userService.login(userNetworkMapper.mapFromDomain(user)).data
        return POS(
            phoneNumber = user.phoneNumber,
            token = posDto.token,
            name = posDto.name,
            loginTime = Date().time,
            alamat = posDto.address,
            profileImage = posDto.profilePic
        )
    }

    override suspend fun setStartingBalance(startTingBalance: Double, token: String): Boolean {
        userService.startBeginingBalance(token, BalanceDto(startTingBalance))
        return true
    }

    override suspend fun setDeliveryStatus(token: String, isDeliveryOpen: Boolean): String {
        return userService.setDeliveryStatus(token, hashMapOf(
            "Deliver" to isDeliveryOpen
        )).message
    }

    override suspend fun closePOS(token: String): String {
        val closeResult = userService
        return closeResult.closePOS(token).message
    }

    override suspend fun updateFCM(token: String): String {
        return withContext(IO){
            val fcmToken = async { firebaseMessaging.token.await() }
            val deviceId = async { FirebaseInstallations.getInstance().id.await() }
            userService.updateFCM(token, hashMapOf(
                "FcmId" to fcmToken.await(),
                "DeviceId" to deviceId.await()
            )).message
        }

    }

    override suspend fun topupVirtual(
        token: String,
        param: HashMap<String, @JvmSuppressWildcards RequestBody>,
        partFile: MultipartBody.Part?
    ): ResponseTopup {
        return withContext(IO) {
            userService.topup_virtual(token, param, partFile!!)
        }
    }

    override suspend fun getRiwayatSaldo(
        token: String,
        tgl1: String,
        tgl2: String,
        status: String
    ): ResponseRiwayatTopup {
        return userService.getRiwayatTopup(token, tgl1, tgl2, status)
    }

    override suspend fun getSaldo(token: String): ResponseGetSaldo {
        return userService.getSaldo(token)
    }
}