package com.soldig.kawanmart.datasource.cache.dao

import androidx.room.*
import com.soldig.kawanmart.datasource.cache.model.POSProductCrossRef
import com.soldig.kawanmart.datasource.cache.model.POSWithProduct

@Dao
abstract class  POSProductDao  : BaseDao<POSProductCrossRef>(){

    @Transaction
    @Query("""
    SELECT * FROM pos_product
    INNER JOIN product ON product.product_id = pos_product.productId
    WHERE pos_id=:posPhoneNumber
    ORDER BY
        pos_product.isactive DESC
    """)
    abstract suspend fun getPOSProduct(posPhoneNumber: String) : List<POSWithProduct>


    @Query("SELECT * FROM pos_product")
    abstract suspend fun getAllPosProduct() : List<POSProductCrossRef>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addProductPos(crossRef: POSProductCrossRef)


    @Transaction
    @Query("""
    SELECT * FROM pos_product
    INNER JOIN product ON product.product_id = pos_product.productId
    WHERE pos_id=:posPhoneNumber AND  title LIKE :key
    """)
    abstract suspend fun getPosProductByName(posPhoneNumber: String, key: String) : List<POSWithProduct>


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun addBunchProductPos(crossRef: List<POSProductCrossRef>) : Array<Long>

}