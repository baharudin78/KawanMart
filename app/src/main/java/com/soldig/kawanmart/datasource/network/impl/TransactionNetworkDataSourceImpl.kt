package com.soldig.kawanmart.datasource.network.impl

import android.annotation.SuppressLint
import com.soldig.kawanmart.datasource.network.abstraction.TransactionNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.TransactionDetailNetworkMapper
import com.soldig.kawanmart.datasource.network.mapper.TransactionForSyncNetworkMapper
import com.soldig.kawanmart.datasource.network.mapper.TransactionHistoryNetworkMapper
import com.soldig.kawanmart.datasource.network.model.TransactionListForSyncDto
import com.soldig.kawanmart.datasource.network.service.TransactionService
import com.soldig.kawanmart.model.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TransactionNetworkDataSourceImpl
@Inject constructor(
    private val transactionService: TransactionService,
    private val transactionHistoryNetworkMapper: TransactionHistoryNetworkMapper,
    private val transactionDetailNetworkMapper: TransactionDetailNetworkMapper,
    private val transactionForSyncNetworkMapper: TransactionForSyncNetworkMapper
) : TransactionNetworkDataSource {

    override suspend fun checkout(token: String, transaction: Transaction): TransactionDetail {
        val itemList = ArrayList<HashMap<String, Any>>()
        transaction.cart.orderList.forEach {
            val product = it.value.product
            val itemPair = hashMapOf<String, Any>()
            itemPair["ShopProductId"] = product.posProduct.id
            itemPair["Quantity"] = it.value.quantity
            itemList.add(itemPair)
        }
        val body = hashMapOf<String, Any?>().apply {
            put("PaymentId", transaction.paymentMethod.id)
            put("Paid", transaction.paidMoney)
            put("CartItems", itemList)
        }
        val result = transactionService.checkout(token, body).data
        return transactionDetailNetworkMapper.mapToDomain(result)
    }

    override suspend fun checkoutSync(token: String, listTransaction: List<TransactionForSync>) {
            transactionService.checkoutSync(token , TransactionListForSyncDto(listTransaction.map { transactionForSyncNetworkMapper.mapToDomain(it) }))
    }

    override suspend fun getAllTransactionHistory(
        token: String,
        startDate: Date?,
        endDate: Date?
    ): List<TransactionHistory> {
        val startDateInString = if(startDate != null) dateQueryString(startDate) else null
        val endDateInString = if(endDate != null) dateQueryString(endDate) else null
        return transactionService.transactionHistoryList(token,
            startDate =  startDateInString,
            endDate = endDateInString,
            transactionStatus = null ,
            transactionFrom = null
        ).data.map { transactionHistoryNetworkMapper.mapToDomain(it) }
    }

    override suspend fun getOrderOnlyTransaction(
        token: String,
        startDate: Date?,
        endDate: Date?,
        status: TransactionStatus?
    ): List<TransactionHistory> {
        val startDateInString = if(startDate != null) dateQueryString(startDate) else null
        val endDateInString = if(endDate != null) dateQueryString(endDate) else null

        return transactionService.transactionHistoryList(token,
            startDate =  startDateInString,
            endDate = endDateInString,
            transactionStatus = status?.value ,
            transactionFrom = 2
        ).data.map { transactionHistoryNetworkMapper.mapToDomain(it) }
    }

    override suspend fun getTransactionDetail(token: String, id: String): TransactionDetail {
        return transactionDetailNetworkMapper.mapToDomain(transactionService.getTransactionDetail(token,id).data)
    }

    override suspend fun updateTransactionStatus(
        token: String,
        id: String,
        transactionStatus: TransactionStatus
    ): String {
        return transactionService.updateTransactionId(token, id, hashMapOf("Status" to transactionStatus.value)).message
    }


    /*
        Required format for date in api : 2021-06-11 00:00:00
     */
    @SuppressLint("SimpleDateFormat")
    private fun dateQueryString(date: Date): String{
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return simpleDateFormat.format(date)
    }
}