package com.soldig.kawanmart.datasource.network.model.digi.cektagihan


import com.google.gson.annotations.SerializedName

data class FeeData(
    @SerializedName("FeeApp")
    val feeApp: Double = 0.0,
    @SerializedName("FeeShop")
    val feeShop: Double = 0.0,
    @SerializedName("Id")
    val id: String
)