package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DataXX(
    @SerializedName("admin")
    val admin: Int,
    @SerializedName("buyer_last_saldo")
    val buyerLastSaldo: Int,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("customer_no")
    val customerNo: String,
    @SerializedName("desc")
    val desc: DescXX,
    @SerializedName("message")
    val message: String,
    @SerializedName("price")
    val price: Double,
    @SerializedName("rc")
    val rc: String,
    @SerializedName("ref_id")
    val refId: String = "",
    @SerializedName("selling_price")
    val sellingPrice: Double,
    @SerializedName("sn")
    val sn: String,
    @SerializedName("status")
    val status: String
)