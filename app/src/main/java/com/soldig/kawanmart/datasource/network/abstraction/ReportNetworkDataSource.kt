package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.Report
import java.util.*

interface ReportNetworkDataSource {

    suspend fun getReport(token: String, startDate: Date?, endDate: Date? = null) : Report

    suspend fun getCloseReport(token: String) : Report

}