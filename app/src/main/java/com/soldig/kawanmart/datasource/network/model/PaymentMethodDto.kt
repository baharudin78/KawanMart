package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName

data class PaymentMethodDto(
    @SerializedName("Created")
    val created: String = "",
    @SerializedName("CreatedBy")
    val createdBy: String = "",
    @SerializedName("Description")
    val description: String = "",
    @SerializedName("Id")
    val id: String,
    @SerializedName("M_Paymentcategory_Id")
    val mPaymentcategoryId: String,
    @SerializedName("Modified")
    val modified: Any? = null,
    @SerializedName("ModifiedBy")
    val modifiedBy: String = "",
    @SerializedName("Name")
    val name: String
)