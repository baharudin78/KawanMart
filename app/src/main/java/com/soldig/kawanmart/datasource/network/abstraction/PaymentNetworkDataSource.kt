package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.PaymentCategory

interface PaymentNetworkDataSource{


    suspend fun getAllPaymentMethod(token: String): List<PaymentCategory>

}