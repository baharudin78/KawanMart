package com.soldig.kawanmart.datasource.cache.model

import androidx.room.Embedded
import androidx.room.Relation


data class PaymentCategoryWithMethodsRelation(
    @Embedded val paymentCategory: PaymentCategoryEntity,
    @Relation(
        parentColumn = "paymentCategoryId",
        entityColumn = "category_id"
    )
    val paymentMethods : List<PaymentMethodEntity>
)