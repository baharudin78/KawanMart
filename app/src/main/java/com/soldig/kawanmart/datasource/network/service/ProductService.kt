package com.soldig.kawanmart.datasource.network.service

import com.soldig.kawanmart.datasource.network.model.ProductDto
import com.soldig.kawanmart.datasource.network.model.ProductUpdateResultDto
import com.soldig.kawanmart.datasource.network.ApiResponse
import retrofit2.http.*

interface ProductService {

    @GET("pos/product/list")
    suspend fun getAllProduct(
        @Header("Authorization") token: String
    ) : ApiResponse<List<ProductDto>>
    
    @GET("pos/product/list_available?HasOrdering=true")
    suspend fun getAllAvailableProductOnly(
        @Header("Authorization") token: String
    ) : ApiResponse<List<ProductDto>>

    @Headers("Content-Type: application/json")
    @POST("pos/product/update/{id}")
    suspend fun updateProduct(
        @Header("Authorization") token: String,
        @Path("id") id: String,
        @Body productInput : HashMap<String, Any?>
    ) : ApiResponse<ProductUpdateResultDto>

    @GET("pos/product/by_category/{id}?Name=")
    suspend fun getAllProductByCategory(
        @Header("Authorization") token: String,
        @Path("id") productId: String
    ) : ApiResponse<List<ProductDto>>

    @GET("pos/product/last_update")
    suspend fun getLastUpdateTime(
        @Header("Authorization") token: String
    ) : ApiResponse<String>
}