package com.soldig.kawanmart.datasource.network.impl

import com.soldig.kawanmart.datasource.network.service.DigiFlazzService
import com.soldig.kawanmart.common.toMoneyFormat
import com.soldig.kawanmart.model.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class DigiFlazzNetworkDataSource @Inject constructor(
    private val digiFlazzService: DigiFlazzService
) {

    suspend fun getPriceList(token: String): VirtualProduct {
        return withContext(IO) {
            val pulsaProductList = hashMapOf<PulsaProvider, List<PulsaProduct>>()
            val productPLNList = ArrayList<ListrikProduct>()
            val prabayarProductTransactions = async {
                val products = digiFlazzService.getAllPrabayarList(token);
                for (product in products.dFResult) {
                    if (product.category == "Pulsa") {
                        val provider = PulsaProvider.getPulsaByName(product.brand)
                        if (provider != PulsaProvider.Invalid) {
                            val oldPulsa = pulsaProductList[provider] ?: listOf()
                            pulsaProductList[provider] = oldPulsa.toMutableList().apply {
                                add(
                                    PulsaProduct(
                                        pulsaProvider = provider,
                                        skuCode = product.buyerSkuCode,
                                        subtotal = product.price,
                                        productName = product.productName,
                                        adminFee = product.feeApp,
                                        warungFee = product.feeShop,
                                        totalPrice = product.price + product.feeApp + product.feeShop
                                    )
                                )
                            }.sortedBy {
                                it.totalPrice
                            }
                        }
                    } else if (product.category == "Listrik") {
                        productPLNList.add(
                            ListrikProduct(
                                productName = product.productName,
                                subTotal = product.price,
                                skuCode = product.buyerSkuCode,
                                adminFee = product.feeApp,
                                warungFee = product.feeShop,
                                totalPrice = product.price + product.feeApp + product.feeShop
                            )
                        )
                    }
                }
            }
            val listrikListPascaBayar: ArrayList<ListrikPascaBayar> = ArrayList()
            val pulsaPascaBayarList: ArrayList<PulsaPascaBayar> = ArrayList()
            val pdamList: ArrayList<PDAMPascaBayar> = ArrayList()
            val pascaBayarProductTransactions = async {
                val products = digiFlazzService.getAllPascabayarList(token)
                products.dFResult.forEach {
                    val productName = it.productName.lowercase(Locale.getDefault());
                    if (productName.contains("pln")) {
                        listrikListPascaBayar.add(
                            ListrikPascaBayar(skuCode = it.buyerSkuCode, it.productName)
                        )
                    } else if (it.brand == "PDAM") {
                        pdamList.add(
                            PDAMPascaBayar(skuCode = it.buyerSkuCode, productName = it.productName)
                        )
                    } else {
                        pulsaPascaBayarList.add(
                            PulsaPascaBayar(skuCode = it.buyerSkuCode, it.productName)
                        )
                    }
                }
            }

            prabayarProductTransactions.await()
            pascaBayarProductTransactions.await()
            VirtualProduct(
                pulsaProductList,
                productPLNList.sortedBy { it.totalPrice },
                listrikListPascaBayar,
                pulsaPascaBayarList,
                pdamList
            )
        }
    }

    suspend fun checkPostpaid(
        token: String,
        skuCode: String,
        postpaidNumber: String
    ): PostPaidProductResult {
        val result = digiFlazzService.cekTagihan(token, skuCode, postpaidNumber);
        val digiData = result.dFResult.data
        val amanahData = result.dFResult.feeData
        return PostPaidProductResult(
            refId = digiData.refId,
            postPaidStatus = rcExtractorToPostPaidStatus(digiData.rc),
            totalPrice = digiData.sellingPrice + amanahData.feeApp + amanahData.feeShop,
            message = digiData.message,
            adminFee = amanahData.feeApp,
            subTotal = digiData.sellingPrice,
            warungFee = amanahData.feeShop
        )
    }

    suspend fun payment(
        token: String,
        selectedVirtualProduct: SelectedVirtualProduct,
    ): VirtualProductResult {
        val refId = selectedVirtualProduct.refId
        return if (refId == null) {
            val result = digiFlazzService.topup(
                token,
                selectedVirtualProduct.skuCode,
                selectedVirtualProduct.customerNumber,
                selectedVirtualProduct.productName,
                selectedVirtualProduct.subTotal,
                selectedVirtualProduct.productCategory
            )
            val digiData = result.dFResult
            val amanahData =result.feeData
            VirtualProductResult(
                status = rcExtractorToVirtualProductResult(digiData.data.rc),
                refId = digiData.data.refId,
                customerNumber = digiData.data.customerNo,
                virtualProductType = selectedVirtualProduct.virtualType,
                virtualProductPaymentType = selectedVirtualProduct.virtualPaymentType,
                message = digiData.data.message,
                adminFee = amanahData.feeApp,
                totalPrice = amanahData.feeTotal,
                warungFee = amanahData.feeShop,
                subTotal = amanahData.hargaProduk,
                productNaem = selectedVirtualProduct.productName
                )
        } else {
            val result = digiFlazzService.bayarTagihan(
                token,
                selectedVirtualProduct.skuCode,
                selectedVirtualProduct.customerNumber,
                selectedVirtualProduct.productName,
                selectedVirtualProduct.subTotal,
                selectedVirtualProduct.productCategory
            )
            val digiResult = result.dFResult.data
            val amanahResult = result.feeData
            VirtualProductResult(
                status = rcExtractorToVirtualProductResult(digiResult.rc),
                refId = digiResult.refId,
                customerNumber = digiResult.customerNo,
                virtualProductType = selectedVirtualProduct.virtualType,
                virtualProductPaymentType = selectedVirtualProduct.virtualPaymentType,
                message = result.message,
                adminFee = amanahResult.feeApp,
                totalPrice = amanahResult.feeTotal,
                warungFee = amanahResult.feeShop,
                subTotal = amanahResult.hargaProduk,
                productNaem = selectedVirtualProduct.productName
            )
        }
    }



    suspend fun getTransactionHistory(
        token: String
    ) : List<VirtualProductHistory> {
        return digiFlazzService.getTransactionHistory(token).dFResult.map {
            VirtualProductHistory(
                it.refId,
                date = it.created,
                jenis = it.itemDetail,
                subTotal = it.subTotal.toMoneyFormat(),
                feeAdmin = it.feeApp.toMoneyFormat(),
                feeWarung = it.feeShop.toMoneyFormat(),
                total = it.feeTotal.toMoneyFormat()
            )
        }
    }

    private fun rcExtractorToPostPaidStatus(rc: String): PostPaidStatus {
        return when (rc.toInt()) {
            0 -> PostPaidStatus.Success
            3 -> PostPaidStatus.Pending
            else -> PostPaidStatus.Fail
        }
    }

    private fun rcExtractorToVirtualProductResult(rc: String): VirtualProductResultStatus {
        return when (rc.toIntOrNull() ?: -1) {
            0 -> VirtualProductResultStatus.Success
            3 -> VirtualProductResultStatus.Pending
            else -> VirtualProductResultStatus.Fail
        }
    }



}