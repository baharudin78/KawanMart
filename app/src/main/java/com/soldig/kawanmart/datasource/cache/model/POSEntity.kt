package com.soldig.kawanmart.datasource.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "pos")
data class POSEntity(

    @PrimaryKey
    @ColumnInfo(name = "phone_number")
    val phoneNumber: String,

    @ColumnInfo(name = "toko")
    val token : String,

    @ColumnInfo(name = "nama_toko")
    val namaToko: String,


    @ColumnInfo(name = "alamat_toko")
    val alamatToko: String,

    @ColumnInfo(name = "last_update_product")
    val lastUpdateProductDate: String
    
)