package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.ProductTransactionForSyncDto

data class TransactionForSyncDto(
    @SerializedName("Items")
    val items: List<ProductTransactionForSyncDto>,
    @SerializedName("Noref")
    val noref: String,
    @SerializedName("Paid")
    val paid: Double,
    @SerializedName("PaymentId")
    val paymentId: String,
    @SerializedName("TransDate")
    val transDate: String
)