package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.ProductCategory

interface CategoryNetworkDataSource {


    suspend fun getAllCategory(token: String) : List<ProductCategory>

}