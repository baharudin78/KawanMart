package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName
import com.soldig.kawanmart.datasource.network.model.TransactionForSyncDto

data class TransactionListForSyncDto(
    @SerializedName("Transactions")
    val transactions: List<TransactionForSyncDto>
)