package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.*
import java.util.*

interface TransactionNetworkDataSource {

    suspend fun checkout(token:String, transaction: Transaction) : TransactionDetail

    suspend fun checkoutSync(token:String, listTransaction : List<TransactionForSync>)

    suspend fun getAllTransactionHistory(token: String, startDate: Date? = null, endDate: Date?=null) : List<TransactionHistory>

    suspend fun getOrderOnlyTransaction(token: String, startDate: Date? = null , endDate: Date?= null, status: TransactionStatus? = null) : List<TransactionHistory>

    suspend fun getTransactionDetail(token: String, id: String) : TransactionDetail

    suspend fun updateTransactionStatus(token: String, id: String, transactionStatus: TransactionStatus) : String

}

