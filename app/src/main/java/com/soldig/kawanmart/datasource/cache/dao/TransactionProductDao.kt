package com.soldig.kawanmart.datasource.cache.dao

import androidx.room.Dao
import com.soldig.kawanmart.datasource.cache.model.TransactionProductCrossRef

@Dao
abstract class TransactionProductDao : BaseDao<TransactionProductCrossRef>(){
}