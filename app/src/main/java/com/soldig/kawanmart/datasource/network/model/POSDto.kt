package com.soldig.kawanmart.datasource.network.model

import com.google.gson.annotations.SerializedName

data class POSDto(
    @SerializedName("Token")
    val token: String,
    @SerializedName("Shop_Name")
    val name: String,
    @SerializedName("Shop_Address")
    val address: String,
    @SerializedName("Shop_FrontShopPicture")
    val profilePic:String
)
