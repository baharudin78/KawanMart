package com.soldig.kawanmart.datasource.network.model.sample

data class Response(
    val Code: Int,
    val Status: String
)