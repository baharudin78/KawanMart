package com.soldig.kawanmart.datasource.cache.model

import androidx.room.Embedded
import com.soldig.kawanmart.datasource.cache.model.ProductEntity
import com.soldig.kawanmart.datasource.cache.model.TransactionEntity
import com.soldig.kawanmart.datasource.cache.model.TransactionProductCrossRef

data class TransactionWithProduct(
    @Embedded val transaction : TransactionEntity,
    @Embedded val product : ProductEntity,
    @Embedded val transactionProductCrossRef: TransactionProductCrossRef
)
