package com.soldig.kawanmart.datasource.network.model.digi.pascaproductlist


import com.google.gson.annotations.SerializedName

data class DFResult(
    @SerializedName("admin")
    val admin: Int,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("buyer_product_status")
    val buyerProductStatus: Boolean,
    @SerializedName("buyer_sku_code")
    val buyerSkuCode: String,
    @SerializedName("category")
    val category: String,
    @SerializedName("commission")
    val commission: Int,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("FeeApp")
    val feeApp: String,
    @SerializedName("FeeShop")
    val feeShop: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("seller_name")
    val sellerName: String,
    @SerializedName("seller_product_status")
    val sellerProductStatus: Boolean
)