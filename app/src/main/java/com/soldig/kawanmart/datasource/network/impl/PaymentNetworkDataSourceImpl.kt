package com.soldig.kawanmart.datasource.network.impl

import com.soldig.kawanmart.datasource.network.abstraction.PaymentNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.PaymentCategoryNetworkMapper
import com.soldig.kawanmart.datasource.network.service.PaymentService
import com.soldig.kawanmart.model.PaymentCategory
import javax.inject.Inject

class PaymentNetworkDataSourceImpl
@Inject constructor(
    private val paymentService: PaymentService,
    private val paymentCategoryNetworkMapper: PaymentCategoryNetworkMapper
): PaymentNetworkDataSource {

    override suspend fun getAllPaymentMethod(token: String): List<PaymentCategory> {
        return paymentService.getAllPaymentMethode(token).data.map {
            paymentCategoryNetworkMapper.mapToDomain(it) }
    }
}