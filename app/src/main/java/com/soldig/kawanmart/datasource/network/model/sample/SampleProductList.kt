package com.soldig.kawanmart.datasource.network.model.sample

import com.soldig.kawanmart.datasource.network.model.ProductDto

data class SampleProductList(
    val Data: ProductDto,
    val Message: String,
    val Response: Response
)