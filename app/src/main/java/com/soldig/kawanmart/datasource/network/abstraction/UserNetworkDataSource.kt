package com.soldig.kawanmart.datasource.network.abstraction

import com.soldig.kawanmart.model.ResponseGetSaldo
import com.soldig.kawanmart.model.ResponseRiwayatTopup
import com.soldig.kawanmart.model.POS
import com.soldig.kawanmart.model.ResponseTopup
import com.soldig.kawanmart.model.User
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface UserNetworkDataSource {

    suspend fun login(user : User) : POS
    suspend fun setStartingBalance(startTingBalance : Double, token: String) : Boolean

    suspend fun setDeliveryStatus(token: String, isDeliveryOpen: Boolean) : String

    suspend fun closePOS(token: String) : String

    suspend fun updateFCM(token: String) : String
    suspend fun topupVirtual(
        token: String,
        param: HashMap<String, @JvmSuppressWildcards RequestBody>,
        partFile: MultipartBody.Part?
    ): ResponseTopup

    suspend fun getRiwayatSaldo(token: String,tgl1: String,tgl2: String,status: String) : ResponseRiwayatTopup

    suspend fun getSaldo(token: String) : ResponseGetSaldo


}