package com.soldig.kawanmart.datasource.cache.mapper

import com.soldig.kawanmart.datasource.cache.model.PaymentMethodEntity
import com.soldig.kawanmart.model.PaymentMethod
import com.soldig.kawanmart.datasource.DomainMapper
import javax.inject.Inject

class PaymentMethodEntityMapper
@Inject constructor() : DomainMapper<PaymentMethodEntity, PaymentMethod> {
    override fun mapFromDomain(domain: PaymentMethod): PaymentMethodEntity {
        return PaymentMethodEntity(
            name = domain.name,
            paymentMethodId = domain.id,
            categoryId = domain.categoryId
        )
    }

    override fun mapToDomain(t: PaymentMethodEntity): PaymentMethod {
        return PaymentMethod(
            name = t.name,
            categoryId = t.categoryId,
            id = t.paymentMethodId
        )
    }
}