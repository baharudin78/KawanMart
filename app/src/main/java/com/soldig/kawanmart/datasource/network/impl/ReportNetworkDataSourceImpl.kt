package com.soldig.kawanmart.datasource.network.impl

import android.annotation.SuppressLint
import com.soldig.kawanmart.datasource.network.abstraction.ReportNetworkDataSource
import com.soldig.kawanmart.datasource.network.mapper.ReportNetworkMapper
import com.soldig.kawanmart.datasource.network.service.ReportService
import com.soldig.kawanmart.model.Report
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ReportNetworkDataSourceImpl
@Inject constructor(
    private val reportService: ReportService,
    private val reportNetworkMapper: ReportNetworkMapper
) : ReportNetworkDataSource {
    override suspend fun getReport(token: String, startDate: Date?, endDate: Date?): Report {
        val startDateInString = if(startDate != null) dateQueryString(startDate) else null
        val endDateInString = if(endDate != null) dateQueryString(endDate) else null
        return reportNetworkMapper.mapToDomain(reportService.getReport(token,startDateInString,endDateInString).data)
    }
    override suspend fun getCloseReport(token: String): Report {
        return reportNetworkMapper.mapToDomain(reportService.getCloseReport(token).data);
    }
    /*
    Required format for date in api : 2021-06-11 00:00:00
 */
    @SuppressLint("SimpleDateFormat")
    private fun dateQueryString(date: Date): String{
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return simpleDateFormat.format(date)
    }
}