package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ProductCategoryDto(
    @SerializedName("Created")
    val created: String,
    @SerializedName("CreatedBy")
    val createdBy: String,
    @SerializedName("Description")
    val description: Any,
    @SerializedName("Id")
    val id: String,
    @SerializedName("Modified")
    val modified: String,
    @SerializedName("ModifiedBy")
    val modifiedBy: String,
    @SerializedName("Name")
    val name: String,
    @SerializedName("Picture")
    val picture: String
)