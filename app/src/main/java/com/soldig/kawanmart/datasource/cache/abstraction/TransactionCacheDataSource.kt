package com.soldig.kawanmart.datasource.cache.abstraction

import com.soldig.kawanmart.model.Transaction
import com.soldig.kawanmart.model.TransactionDetail
import com.soldig.kawanmart.model.TransactionForSync
import com.soldig.kawanmart.model.TransactionHistory

interface TransactionCacheDataSource {

    suspend fun checkout(phoneNumber:String, transaction: Transaction) : TransactionDetail

    suspend fun getTransactionDetail(transactionId: String) : TransactionDetail

    suspend fun getAllTransactionHistory(phoneNumber: String) : List<TransactionHistory>

    suspend fun getAllTransactionForSync(phoneNumber: String) : List<TransactionForSync>

    suspend fun deleteAllPOSTransaction(phoneNumber: String)

//    suspend fun updateTransactionStatus(token: String, id: String, transactionStatus: TransactionStatus) : String

}