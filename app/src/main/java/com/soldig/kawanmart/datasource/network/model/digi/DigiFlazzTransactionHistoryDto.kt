package com.soldig.kawanmart.datasource.network.model.digi


import com.google.gson.annotations.SerializedName

data class DigiFlazzTransactionHistoryDto(
    @SerializedName("FeeApp")
    val feeApp: Double = 0.0,
    @SerializedName("FeeShop")
    val feeShop: Double = 0.0 ,
    @SerializedName("FeeTotal")
    val feeTotal: Double = 0.0 ,
    @SerializedName("Id")
    val id: String,
    @SerializedName("Latitude")
    val latitude: String,
    @SerializedName("Longitude")
    val longitude: String,
    @SerializedName("RefId")
    val refId: String,
    @SerializedName("ShopName")
    val shopName: String,
    @SerializedName("Status")
    val status: String,
    @SerializedName("SubTotal")
    val subTotal: Double = 0.0
)