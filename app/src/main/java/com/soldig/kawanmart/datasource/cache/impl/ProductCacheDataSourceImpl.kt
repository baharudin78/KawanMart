package com.soldig.kawanmart.datasource.cache.impl

import com.soldig.kawanmart.datasource.cache.abstraction.ProductCacheDataSource
import com.soldig.kawanmart.datasource.cache.dao.POSDao
import com.soldig.kawanmart.datasource.cache.dao.POSProductDao
import com.soldig.kawanmart.datasource.cache.dao.ProductDao
import com.soldig.kawanmart.datasource.cache.mapper.POSProductEntityMapper
import com.soldig.kawanmart.datasource.cache.model.POSProductCrossRef
import com.soldig.kawanmart.datasource.cache.model.ProductEntity
import com.soldig.kawanmart.model.Product
import javax.inject.Inject


class ProductCacheDataSourceImpl
@Inject constructor(
    private val productDao: ProductDao,
    private val posProductDao: POSProductDao,
    private val productEntityMapper: POSProductEntityMapper,
    private val posDao: POSDao
) : ProductCacheDataSource {

    override suspend fun upsertOne(product: Product) {
        val mapped = productEntityMapper.mapFromDomain(product)
        productDao.update(mapped.productEntity)
        posProductDao.update(mapped.posProductEntity)
    }

    override suspend fun insertAll(product: List<Product>) {
        val productList = ArrayList<ProductEntity>()
        val posProductList = ArrayList<POSProductCrossRef>()
        product.forEach {
            val mapped = productEntityMapper.mapFromDomain(it)
            productList.add(mapped.productEntity)
            posProductList.add(mapped.posProductEntity)
        }
        productDao.upsert(productList)
        posProductDao.upsert(posProductList)
    }

    override suspend fun getAll(phoneNumber: String): List<Product> {
        return posProductDao.getPOSProduct(phoneNumber).map {  productEntityMapper.mapToDomain(it) }
    }

    override suspend fun getProductByName(nameKey: String, phoneNumber: String): List<Product> {
        return posProductDao.getPosProductByName(phoneNumber,"%$nameKey%").map { productEntityMapper.mapToDomain(it) }
    }

    override suspend fun getLastProductUpdate(phoneNumber: String) : String {
        return posDao.getPOS(phoneNumber).lastUpdateProductDate
    }

    override suspend fun updateLastProductUpdate(phoneNumber: String, lastProductUpdate: String) {
        return posDao.update(posDao.getPOS(phoneNumber).copy(lastUpdateProductDate = lastProductUpdate))
    }



}