package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ProductUpdateResultDto(
    @SerializedName("Created")
    val created: Any,
    @SerializedName("CreatedBy")
    val createdBy: Any,
    @SerializedName("DiscountType")
    val discountType: Int?,
    @SerializedName("DiscountValue")
    val discountValue: Double,
    @SerializedName("Id")
    val id: String,
    @SerializedName("IsActive")
    val isActive: Int,
    @SerializedName("IsFeatured")
    val isFeatured: Int,
    @SerializedName("M_Product_Id")
    val mProductId: String,
    @SerializedName("M_Shop_Id")
    val mShopId: String,
    @SerializedName("Modified")
    val modified: Any,
    @SerializedName("ModifiedBy")
    val modifiedBy: Any,
    @SerializedName("Ordering")
    val ordering: Int,
    @SerializedName("PurchasePrice")
    val purchasePrice: Double,
    @SerializedName("SellPrice")
    val sellPrice: Double,
    @SerializedName("Stock")
    val stock: Int,
    @SerializedName("CurrentPrice")
    val currentPrice: Double,
)