package com.soldig.kawanmart.datasource


object DataSourceConst {

    const val NETWORK_TIMEOUT = 5000L
    const val CACHE_TIMEOUT = 7000L
//    const val CONNECTION_CHECK_BASE_URL =  "http://174.138.30.49" //IP Version
//    const val CONNECTION_CHECK_BASE_URL ="https://ayogrosir.id"
//    const val BASE_IMAGE_URL ="$CONNECTION_CHECK_BASE_URL/"
//    const val BASE_URL ="$CONNECTION_CHECK_BASE_URL/api/"

    const val CONNECTION_CHECK_BASE_URL =  "https://ayogrosir.id"
    const val BASE_IMAGE_URL = "$CONNECTION_CHECK_BASE_URL/"
    const val BASE_URL = "$CONNECTION_CHECK_BASE_URL/api/"

}
