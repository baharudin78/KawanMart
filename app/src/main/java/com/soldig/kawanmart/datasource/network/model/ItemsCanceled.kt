package com.soldig.kawanmart.datasource.network.model


import com.google.gson.annotations.SerializedName

data class ItemsCanceled(
    @SerializedName("M_Shopproduct_Id")
    val mShopproductId: String,
    @SerializedName("Name")
    val name: String,
    @SerializedName("Quantity")
    val quantity: Int
)